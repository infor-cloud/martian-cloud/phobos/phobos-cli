# Phobos CLI Commands

## Available Commands

Currently, the following commands are available:

```
configure                    Create or update a profile.
documentation                Perform command documentation operations.
fmt                          Reformat HCL configuration files.
organization                 Perform operations on organizations.
pipeline                     Perform operations on pipelines.
pipeline-template            Perform operations on pipeline templates.
plugin                       Interact with plugins.
project                      Perform operations on projects.
release                      Create, manage releases and deployments.
release-lifecycle            Perform operations on release lifecycles.
sso                          Log in to the OAuth2 provider and return an authentication token.
version                      Get the CLI's version.
```

***

### Command: configure

##### Create or update a profile.

**Subcommands:**

```
delete                       Remove a profile.
list                         Show all profiles.
```

```
Usage: phobos configure [options]
```

   The configure command creates or updates a profile. If no
   options are specified, the command prompts for values.

<details>
<summary>Expand options</summary>

- `--http-endpoint`: The Phobos HTTP API endpoint (in URL format).

- `--insecure-tls-skip-verify`: Allow TLS but disable verification of the gRPC server's certificate chain and hostname. This should ONLY be true for testing as it could allow the CLI to connect to an impersonated server.

- `--profile`: The name of the profile to set.

</details>

##### Example:

```
phobos configure \
  --http-endpoint https://api.phobos.example.com \
  --profile prod-example
```

---

#### configure delete

##### Remove a profile.

```
Usage: phobos configure delete <name>
```

   The configure delete command removes a profile and its
   credentials with the given name.

##### Example:

```
phobos configure delete prod-example
```

---

#### configure list

##### Show all profiles.

```
Usage: phobos configure list
```

   The configure list command prints information about all profiles.

##### Example:

```
phobos configure list
```

***

### Command: documentation

##### Perform command documentation operations.

**Subcommands:**

```
generate                     Generate documentation of commands.
```

The documentation command(s) perform operations on the documentation.

---

#### documentation generate

##### Generate documentation of commands.

```
Usage: phobos [global options] documentation generate
```

  The documentation generate command generates markdown documentation
  for the entire CLI.

<details>
<summary>Expand options</summary>

- `--output`: The output filename.

</details>

##### Example:

```
phobos documentation generate
```

***

### Command: fmt

##### Reformat HCL configuration files.

```
Usage: phobos [global options] fmt [options] [file ...]
```

  The fmt command rewrites HCL configuration files to canonical format.
  All files with the (.hcl) extension in the current directory will be
  formatted if no file(s) are specified. Lists all files that were
  formatted to the standard output.

<details>
<summary>Expand options</summary>

- `--directory`: Directory to format files in. Defaults to the current directory.

- `--recursive`: Format files in the specified directory and all subdirectories.

- `--write`: Write the formatted result to the file(s).

</details>

##### Example:

```
phobos fmt \
  --directory test-pipelines \
  --recursive \
  --write=true
```

***

### Command: organization

##### Perform operations on organizations.

**Subcommands:**

```
create                       Create an organization.
delete                       Delete an organization.
get                          Retrieve an organization.
list                         Retrieve a paginated list of organizations.
update                       Update an organization.
```

The organization commands do operations on organizations.
Organizations are containers that house other Phobos
resources. Subcommands provide full CRUD support.

---

#### organization create

##### Create an organization.

```
Usage: phobos [global options] organization create [options] <name>
```

  The organization create command creates a new organization
  with the given name and description. -json flag optionally
  allows showing the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--description`: Description for the new organization.

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos organization create \
  --description \
  --json example-org
```

---

#### organization delete

##### Delete an organization.

```
Usage: phobos [global options] organization delete [options] <id>
```

  The organization delete command deletes an organization
  with the given ID.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos organization delete prn:organization:example-org
```

---

#### organization get

##### Retrieve an organization.

```
Usage: phobos [global options] organization get <name>
```

  The organization get command retrieves an organization by name.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos organization get \
  --json \
  example-org
```

---

#### organization list

##### Retrieve a paginated list of organizations.

```
Usage: phobos [global options] organization list [options]
```

  The organization list command prints information about (likely
  multiple) organizations. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: The cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--limit`: Maximum number of result elements to return. Defaults to 100.

- `--search`: Filter to only organizations with this name prefix. Helpful when searching for a specific result.

- `--sort-order`: Sort in this direction, ASC or DESC.

</details>

##### Example:

```
phobos organization list \
  --search unseen \
  --limit 5 \
  --json
```

---

#### organization update

##### Update an organization.

```
Usage: phobos [global options] organization update [options] <id>
```

  The organization update command update an organization with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--description`: Description for the new organization.

- `--json`: Show final output as JSON.

- `--version`: Metadata version of the resource to be updated. In most cases, this is not required.

</details>

##### Example:

```
phobos organization update \
  --description "updated description" \
  prn:organization:example-org
```

***

### Command: pipeline

##### Perform operations on pipelines.

**Subcommands:**

```
cancel                       Cancel a pipeline.
create                       Create a pipeline.
get                          Retrieve a pipeline.
list                         Retrieve a paginated list of pipelines.
validate                     Validates a pipeline template.
```

The pipeline commands do operations on pipelines.
Pipelines execute sequences of prescribed operations.
Subcommands provide create, list, get, and cancel support.

---

#### pipeline cancel

##### Cancel a pipeline.

```
Usage: phobos [global options] pipeline cancel <id>
```

  The pipeline cancel command cancels a pipeline by id.

##### Example:

```
phobos pipeline cancel \
  prn:pipeline:my-org/my-project/MM3DEOLGGVSTILLEGJSTOLJUG4ZTQLJYMYZWMLLFHA3TEZLFGFTDEYZUGJPVASI
```

---

#### pipeline create

##### Create a pipeline.

```
Usage: phobos [global options] pipeline create [options] project-name
```

  The pipeline create command creates a new pipeline under the
  specified organization and project. The -json flag optionally
  allows showing the final output as formatted JSON.

  HCL variables may be passed in via supported options or
  exported to the environment with a 'PB_VAR_' prefix.

  Variable parsing precedence:
  1. HCL variables from the environment.
  2. phobos.pbvars file from configuration's directory,
     if present.
  3. phobos.pbvars.json file from configuration's
     directory, if present.
  4. *.auto.pbvars, *.auto.pbvars.json files
     from the configuration's directory, if present.
  5. --pb-var-file option(s).
  6. --pb-var option(s).

  NOTE: If the same variable is assigned multiple values,
  the last value found will be used. A --pb-var option
  will override the values from a *.pbvars file which
  will override values from the environment (PB_VAR_).

<details>
<summary>Expand options</summary>

- `--annotation`: A key/value pair that will be associated with the pipeline. (This flag may be repeated).

- `--directory`: Path to directory that contains the HCL input file.

- `--env-var`: Define an environment variable. Environment variables are expected to be in the format of key=value. Non key=value pairs will be ignored. (This flag may be repeated).

- `--env-var-file`: Path to environment variable file. Environment variable files are expected to be in the format of key=value per line. (This flag may be repeated).

- `--environment-name`: The name of the environment to which this pipeline will deploy resources to (only required for deployment pipelines)

- `--filename`: Name of HCL input file.

- `--follow`: Show live pipeline output

- `--json`: Show final output as JSON.

- `--org-name`: Name of parent organization for the new pipeline. (This flag is required).

- `--pb-var`: Define a HCL variable as a key=value. To supply a string, do --pb-var 'sample=this is a value'. Non key=value pairs will be ignored. (This flag may be repeated).

- `--pb-var-file`: Path to a HCL variable file (*.pbvars or *.pbvars.json). (This flag may be repeated).

- `--template-id`: ID of the pipeline template to use

- `--type`: The type of the pipeline. (runbook or deployment)

- `--variable-set-revision`: Project variable set revision or "latest" for latest variable set.

</details>

##### Example:

```
phobos pipeline create \
  --org-name my-organization \
  --type runbook \
  --filename my-pipeline-template.hcl \
  my-project
```

---

#### pipeline get

##### Retrieve a pipeline.

```
Usage: phobos [global options] pipeline get <id>
```

  The pipeline get command retrieves a pipeline by id.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos pipeline get \
  prn:pipeline:my-org/my-project/MM3DEOLGGVSTILLEGJSTOLJUG4ZTQLJYMYZWMLLFHA3TEZLFGFTDEYZUGJPVASI
```

---

#### pipeline list

##### Retrieve a paginated list of pipelines.

```
Usage: phobos [global options] pipeline list [options]
```

  The pipeline list command prints information about (likely
  multiple) pipelines. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: The cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--limit`: Maximum number of results to return. Defaults to 100.

- `--org-name`: The organization to limit search.  (This flag is required.)

- `--project-name`: The project to limit search.  (This flag is required.)

- `--sort-order`: Sort in this direction, ASC or DESC.

</details>

##### Example:

```
phobos pipeline list \
  --limit 5 \
  --org-name my-organization \
  --project-name my-project \
  --json
```

---

#### pipeline validate

##### Validates a pipeline template.

```
Usage: phobos [global options] pipeline validate
```

  The pipeline validate command validates a pipeline template.

  HCL variables are optional when using the validate command except
  for variables used in dynamic blocks. Variables may be passed in
  via supported options or exported to the environment with
  a 'PB_VAR_' prefix.

  Variable parsing precedence:
  1. HCL variables from the environment.
  2. phobos.pbvars file from configuration's directory,
     if present.
  3. phobos.pbvars.json file from configuration's
     directory, if present.
  4. *.auto.pbvars, *.auto.pbvars.json files
     from the configuration's directory, if present.
  5. --pb-var-file option(s).
  6. --pb-var option(s).

  NOTE: If the same variable is assigned multiple values,
  the last value found will be used. A --pb-var option
  will override the values from a *.pbvars file which
  will override values from the environment (PB_VAR_).

<details>
<summary>Expand options</summary>

- `--directory`: Path to directory that contains the HCL input file.

- `--filename`: Name of HCL input file.

- `--pb-var`: Define a HCL variable as a key=value. To supply a string, do --pb-var 'sample=this is a value'. Non key=value pairs will be ignored. (This flag may be repeated).

- `--pb-var-file`: Path to a HCL variable file (*.pbvars or *.pbvars.json). (This flag may be repeated).

</details>

##### Example:

```
phobos pipeline validate \
  --filename my-pipeline-template.hcl
```

***

### Command: pipeline-template

##### Perform operations on pipeline templates.

**Subcommands:**

```
create                       Create a pipeline template.
delete                       Delete a pipeline template.
get                          Retrieve a pipeline template.
list                         Retrieve a paginated list of pipeline templates.
```

The pipeline-template commands do operations on pipeline templates.
A pipeline template is a reusable pipeline definition.
Subcommands provide create, list, get, and delete support.

---

#### pipeline-template create

##### Create a pipeline template.

```
Usage: phobos [global options] pipeline-template create [options]
```

  The pipeline-template create command creates a new pipeline template
  under the specified organization and project.  The -json flag optionally
  allows showing the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--directory`: Path to directory that contains the HCL input file.

- `--filename`: Name of HCL input file.

- `--json`: Show final output as JSON.

- `--name`: Specify a name for a versioned pipeline template.  (Required if -versioned is true.)

- `--org-name`: Name of organization for the new pipeline.  (This flag is required.)

- `--project-name`: Name of project for the new pipeline.  (This flag is required.)

- `--semantic-version`: Specify a semantic version for a versioned pipeline template.  (Required if -versioned is true.)

- `--versioned`: Create a versioned pipeline template.

</details>

##### Example:

```
phobos pipeline-template create \
  --filename my-pipeline-template.hcl \
  --name my-pipeline-template \
  --versioned=true \
  --semantic-version 0.0.1 \
  --org-name my-org \
  --project-name my-project
```

---

#### pipeline-template delete

##### Delete a pipeline template.

```
Usage: phobos [global options] pipeline-template delete [options] <id>
```

  The pipeline-template delete command deletes a pipeline template
  with the given ID.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos pipeline-template delete \
  prn:pipeline_template:my-org/my-project/MI3DKNBYGNQTCLJXMVSTKLJUGY4DSLLBMNRDKLJSHA4TOZRQGY3TKZBZMNPVAVA
```

---

#### pipeline-template get

##### Retrieve a pipeline template.

```
Usage: phobos [global options] pipeline-template get <id>
```

  The pipeline-template get command retrieves a pipeline template by id.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos pipeline-template get \
  --json \
  prn:pipeline_template:my-org/my-project/MI3DKNBYGNQTCLJXMVSTKLJUGY4DSLLBMNRDKLJSHA4TOZRQGY3TKZBZMNPVAVA
```

---

#### pipeline-template list

##### Retrieve a paginated list of pipeline templates.

```
Usage: phobos [global options] pipeline-template list [options]
```

  The pipeline-template list command prints information about (likely
  multiple) pipeline templates. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: The cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--latest`: If supplied, filter by latest pipeline templates.

- `--limit`: Maximum number of result elements to return. Defaults to 100.

- `--org-name`: Name of parent organization for the new pipeline.  (This flag is required.)

- `--project-name`: The project to limit search.  (This flag is required.)

- `--search`: Filter to only pipeline templates with this name prefix.

- `--sort-order`: Sort in this direction, ASC or DESC.

- `--versioned`: If supplied, filter by versioned or non-versioned pipeline templates.

</details>

##### Example:

```
phobos pipeline-template list \
  --limit 5 \
  --project-name my-project \
  --json
```

***

### Command: plugin

##### Interact with plugins.

**Subcommands:**

```
create                       Create a new plugin in the plugin registry.
delete                       Delete a plugin from the plugin registry.
delete-platform              Delete a plugin platform from the plugin registry.
delete-version               Delete a plugin version from the plugin registry.
get                          Retrieve a plugin from the plugin registry.
get-platform                 Retrieve a plugin platform from the plugin registry.
get-version                  Retrieve a plugin version from the plugin registry.
install                      Install a plugin from a remote registry.
launch                       Launch a built-in plugin.
list                         List available plugins.
list-platforms               Lists the available plugin platforms from the registry.
list-versions                List versions for a plugin from the registry.
update                       Update a plugin in the plugin registry.
upload-version               Upload a new version of a plugin to the plugin registry.
```

The plugin commands do operations on plugins.
Plugins are used to extend the functionality of Phobos.
Subcommands provide support for listing, launching and
managing plugins via the plugin registry.

---

#### plugin create

##### Create a new plugin in the plugin registry.

```
Usage: phobos [global options] plugin create [options] <name>
```

  The plugin create command creates a new plugin
  at the specified organization. The plugin will
  be private by default. -json flag optionally
  allows showing the final output as formatted
  JSON.

<details>
<summary>Expand options</summary>

- `--if-not-exists`: Create the plugin only if it does not exist.

- `--json`: Show final output as JSON.

- `--org-name`: Name of parent organization for the new plugin. (This flag is required).

- `--private`: Keep the plugin private i.e. not visible to other organizations.

- `--repository-url`: URL of the repository containing the plugin source code.

</details>

##### Example:

```
phobos plugin create \
  --org-name my-org \
  --private=true \
  --json \
  my-plugin
```

---

#### plugin delete

##### Delete a plugin from the plugin registry.

```
Usage: phobos [global options] plugin delete [options] <id>
```

  The plugin delete command deletes a plugin from the plugin registry.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos plugin delete \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
```

---

#### plugin delete-platform

##### Delete a plugin platform from the plugin registry.

```
Usage: phobos [global options] plugin delete-platform [options] <id>
```

  The plugin delete platform command deletes a plugin platform
  from the plugin registry.

<details>
<summary>Expand options</summary>

- `--platform`: Metadata platform of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos plugin delete-platform \
  prn:plugin_platform:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9/windows/amd64
```

---

#### plugin delete-version

##### Delete a plugin version from the plugin registry.

```
Usage: phobos [global options] plugin delete-version [options] <id>
```

  The plugin delete version command deletes a plugin version
  from the plugin registry.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos plugin delete-version \
  prn:plugin_version:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9
```

---

#### plugin get

##### Retrieve a plugin from the plugin registry.

```
Usage: phobos [global options] plugin get [options] <id>
```

  The plugin get command retrieves a plugin from the
  plugin registry. -json flag can be used to get the
  final output as JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos plugin get \
  --json \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
```

---

#### plugin get-platform

##### Retrieve a plugin platform from the plugin registry.

```
Usage: phobos [global options] plugin get-platform [options] <id>
```

  The plugin get platform command retrieves a plugin platform
  from the plugin registry. -json flag can be used to get
  the final output as JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos plugin get-platform \
  prn:plugin_platform:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9/windows/amd64
```

---

#### plugin get-version

##### Retrieve a plugin version from the plugin registry.

```
Usage: phobos [global options] plugin get-version [options] <id>
```

  The plugin get version command retrieves a plugin version
  from the plugin registry. -json flag can be used to get
  the final output as JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos plugin get-version \
  prn:plugin_version:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9
```

---

#### plugin install

##### Install a plugin from a remote registry.

```
Usage: phobos [global options] plugin install <plugin-source>
```

  The plugin install command installs a plugin from a remote registry.

  Plugin will be installed to the user's home directory:

   '~/.phobos.d/plugins/'

  The plugin source format must be:

   '\<registry-hostname\>/\<organization-name\>/\<plugin-name\>'

  The registry hostname may be omitted when using the Phobos instance
  associated with the current profile. An authentication token
  (if exists) from the current profile will be automatically
  supplied in the request.

  Version constraints can optionally be used to limit the Plugin's
  versions. The latest version of the plugin (matching the
  constraints, if supplied) will be used.

<details>
<summary>Expand options</summary>

- `--token`: An authentication token to use when downloading plugin from the remote registry. If nothing is supplied, the current profile's token is used when connecting to its endpoint.

- `--version`: Version constraints to adhere to. Installs the latest version of the plugin (matching the constraints, if supplied).

</details>

##### Example:

```
phobos plugin install \
  my-org/my-plugin
```

---

#### plugin launch

##### Launch a built-in plugin.

```
Usage: phobos [global options] plugin launch <plugin-name>
```

  The plugin launch command launches built-in plugins.
  Please note, these plugins are not meant to be invoked
  directly by the user but only by programs which consume
  the plugin.

---

#### plugin list

##### List available plugins.

```
Usage: phobos [global options] plugin list [options]
```

  The plugin list command lists the available plugins.
  -json can be used to output the list in JSON format.
  This command can either be used to list built-in
  plugins or plugins from the plugin registry.

<details>
<summary>Expand options</summary>

- `--built-in`: List only built-in plugins.

- `--cursor`: Cursor string for manual pagination. Not applicable for built-in plugins.

- `--json`: Show final output as JSON.

- `--limit`: Limit the number of results. Defaults to 100. Not applicable for built-in plugins.

- `--org-name`: Organization name to filter by. Not applicable for built-in plugins.

- `--search`: Filter to only plugins with this name prefix. Not applicable for built-in plugins.

- `--sort-order`: Sort in this direction, ASC or DESC. Not applicable for built-in plugins.

</details>

##### Example:

```
phobos plugin list \
  --limit 10 \
  --sort DESC \
  --org-name "tomcat" \
  --json
```

---

#### plugin list-platforms

##### Lists the available plugin platforms from the registry.

```
Usage: phobos [global options] plugin list-platforms [options]
```

  The plugin list-platforms command lists the available platforms
  for a plugin version. Advanced filtering can be done using the
  provided flags.

<details>
<summary>Expand options</summary>

- `--arch`: Architecture of the platform.

- `--cursor`: Cursor string for manual pagination.

- `--has-binary`: Filter platforms with binary uploaded.

- `--json`: Show final output as JSON.

- `--limit`: Limit the number of results.

- `--os`: Operating system of the platform.

- `--plugin-id`: ID of the plugin. (Required if version-id is provided).

- `--sort`: Sort in this direction, ASC or DESC.

- `--version-id`: ID of the plugin version. (Required if plugin-id is provided).

</details>

##### Example:

```
phobos plugin list-platforms \
  --limit 10 \
  --sort DESC \
  --json \
  --version-id prn:plugin:tomcat/gitlab/1.0.0
```

---

#### plugin list-versions

##### List versions for a plugin from the registry.

```
Usage: phobos [global options] plugin list-versions [options] <plugin-id>
```

  The plugin list-versions command lists the available versions
  for a plugin. -json can be used to output the list in JSON
  format. Advanced filtering can be done using the flags.

<details>
<summary>Expand options</summary>

- `--cursor`: Cursor string for manual pagination.

- `--has-checksums`: Filter by whether the sha sums are uploaded.

- `--json`: Show final output as JSON.

- `--latest`: Filter by whether the version is the latest.

- `--limit`: Limit the number of results.

- `--sort`: Sort in this direction, ASC or DESC.

- `--version`: Semantic version of the plugin.

</details>

##### Example:

```
phobos plugin list-versions \
  --limit 10 \
  --sort DESC \
  --json \
  prn:plugin:tomcat/gitlab
```

---

#### plugin update

##### Update a plugin in the plugin registry.

```
Usage: phobos [global options] plugin update [options] <id>
```

  The plugin update command updates a plugin in the
  plugin registry. -json flag can be used to get
  the output in JSON format.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

- `--private`: Keep the plugin private i.e. not visible to other organizations.

- `--repository-url`: URL of the repository containing the plugin source code.

- `--version`: Metadata version of the resource to be updated. In most cases, this is not required.

</details>

##### Example:

```
phobos plugin update \
  --private=false \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
```

---

#### plugin upload-version

##### Upload a new version of a plugin to the plugin registry.

```
Usage: phobos [global options] plugin upload-version <plugin-id>
```

  The plugin upload-version command uploads a new version
  of a plugin to the plugin registry.

<details>
<summary>Expand options</summary>

- `--directory`: Directory containing the plugin to upload. Defaults to the current directory.

</details>

##### Example:

```
phobos plugin upload-version \
  --directory my-directory \
  prn:plugin:my-org/my-plugin
```

***

### Command: project

##### Perform operations on projects.

**Subcommands:**

```
create                       Create a project.
delete                       Delete a project.
get                          Retrieve a project.
list                         Retrieve a paginated list of projects.
update                       Update a project.
```

The project commands do operations on projects.
Projects are also containers that house other Phobos
resources. Subcommands provide full CRUD support.

---

#### project create

##### Create a project.

```
Usage: phobos [global options] project create [options] <name>
```

  The project create command creates a new project
  with the given name and description under the specified
  parent organization.  The -json flag optionally
  allows showing the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--description`: Description for the new project.

- `--json`: Show final output as JSON.

- `--org-name`: Name of parent organization for the new project.

</details>

##### Example:

```
phobos project create \
  --json \
  --description "this is my project" \
  --org-name my-org \
  example-proj
```

---

#### project delete

##### Delete a project.

```
Usage: phobos [global options] project delete [options] <id>
```

  The project delete command deletes a project
  with the given ID.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos project delete \
  prn:project:my-org/example-proj
```

---

#### project get

##### Retrieve a project.

```
Usage: phobos [global options] project get <id>
```

  The project get command retrieves a project by id.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos project get \
  prn:project:my-org/example-proj
```

---

#### project list

##### Retrieve a paginated list of projects.

```
Usage: phobos [global options] project list [options]
```

  The project list command prints information about (likely
  multiple) projects. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: The cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--limit`: Maximum number of result elements to return. Defaults to 100.

- `--search`: Filter to only projects with this name prefix. Helpful when searching for a specific result.

- `--sort-order`: Sort in this direction, ASC or DESC.

</details>

##### Example:

```
phobos project list \
  --search unseen \
  --limit 5 \
  --json
```

---

#### project update

##### Update a project.

```
Usage: phobos [global options] project update [options] <id>
```

  The project update command updates a project with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--description`: Description for the new project.

- `--json`: Show final output as JSON.

- `--version`: Metadata version of the resource to be updated. In most cases, this is not required.

</details>

##### Example:

```
phobos project update \
  --description "this is an updated description" \
  prn:project:my-org/example-proj
```

***

### Command: release

##### Create, manage releases and deployments.

**Subcommands:**

```
add-participant              Add a participant to a release.
create                       Create a release.
delete                       Delete a release.
get                          Get a release.
list                         Retrieve a paginated list of releases.
remove-participant           Remove a participant from a release.
update                       Update a release.
update-deployment            Update a release deployment.
```

The release commands perform operations on project releases and deployments.
Subcommands provide create, update, list, get functionality and more.

---

#### release add-participant

##### Add a participant to a release.

```
Usage: phobos [global options] release add-participant [options] <release-id>
```

  The release add-participant command adds one or more participants
  to a release. The -json flag optionally allows showing the final
  output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

- `--team-id`: ID of the team to add as a participant to the release.

- `--user-id`: ID of the user to add as a participant to the release.

</details>

##### Example:

```
phobos release add-participant \
  --user-id prn:user:random.person \
  prn:release:my-org/my-project/0.0.17
```

---

#### release create

##### Create a release.

```
Usage: phobos [global options] release create [options]
```

  The release create command creates a new release for
  the specified project. The -json flag optionally
  allows showing the final output as formatted JSON.

  ###### Schema for the release JSON input file:
```json
{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"type": "object",
	"properties": {
		"project_id": {
			"description": "The project ID or PRN",
			"type": "string"
		},
		"lifecycle_id": {
			"description": "The release lifecycle ID or PRN",
			"type": "string"
		},
		"semantic_version": {
			"description": "The semantic version of the release",
			"type": "string"
		},
		"deployment_templates": {
			"description": "The deployment templates to be used for the release",
			"type": "array",
			"minItems": 1,
			"items": {
				"type": "object",
				"properties": {
					"environment_name": {
						"description": "The name of the environment",
						"type": "string"
					},
					"pipeline_template_id": {
						"description": "The ID or PRN of the pipeline template",
						"type": "string"
					}
				},
				"required": \[
					"environment_name",
					"pipeline_template_id"
				\]
			}
		},
		"hcl_variables": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"key": {
						"description": "The key of the variable",
						"type": "string"
					},
					"value": {
						"description": "The value of the variable",
						"type": "string"
					}
				},
				"required": \[
					"key",
					"value"
				\]
			}
		},
		"variable_set_revision": {
			"description": "The variable set revision ID or latest",
			"type": "string"
		},
		"due_date": {
			"description": "The due date of the release",
			"type": "string"
		},
		"user_participant_ids": {
			"description": "The user participant IDs or PRNs",
			"type": "array",
			"items": {
				"type": "string"
			}
		},
		"team_participant_ids": {
			"description": "The team participant IDs or PRNs",
			"type": "array",
			"items": {
				"type": "string"
			}
		},
		"notes": {
			"description": "The notes for the release",
			"type": "string"
		}
	},
	"required": \[
		"project_id",
		"lifecycle_id",
		"semantic_version",
		"deployment_templates",
		"hcl_variables",
		"user_participant_ids",
		"team_participant_ids",
		"notes"
	\]
}
```

<details>
<summary>Expand options</summary>

- `--directory`: Path to the directory that contains the release JSON input file.

- `--filename`: Name of the JSON input file.

- `--json`: Show final output as JSON.

</details>

##### Example:

```
cat <<-EOF > release.json
{
    "project_id": "prn:project:example-org/example-project",
    "lifecycle_id": "prn:release_lifecycle:example-org/standard",
    "semantic_version": "1.0.1",
    "deployment_templates": [
        {
            "environment_name": "dev",
            "pipeline_template_id": "prn:pipeline_template:example-org/example-project/deploy/1.0.0"
        },
        {
            "environment_name": "prod",
            "pipeline_template_id": "prn:pipeline_template:example-org/example-project/deploy/1.0.0"
        }
    ],
    "hcl_variables": [
        {
            "key": "region",
            "value": "us-east-1"
        }
    ],
    "variable_set_revision": "latest",
    "due_date": "2025-01-01T12:00:00.00Z",
    "user_participant_ids": [
        "prn:user:john.smith"
    ],
    "team_participant_ids": [],
    "notes": "This is an example release"
}
EOF

phobos release create \
  --filename release.json
```

---

#### release delete

##### Delete a release.

```
Usage: phobos [global options] release delete [options] <id>
```

  The release delete command deletes a release.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos release delete \
  prn:release:my-org/my-project/0.0.17
```

---

#### release get

##### Get a release.

```
Usage: phobos [global options] release get [options] <id>
```

  The release get command retrieves a release for
  the specified project. The -json flag optionally
  allows showing the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos release get \
  prn:release:my-org/my-project/0.0.17
```

---

#### release list

##### Retrieve a paginated list of releases.

```
Usage: phobos [global options] release list [options]
```

  The release list command prints information about (likely
  multiple) releases. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: Cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--limit`: Maximum number of results to return. Defaults to 100.

- `--org-name`: The organization to limit search (This flag is required).

- `--project-name`: The project to limit search (This flag is required).

- `--sort-order`: Sort in this direction, ASC or DESC.

</details>

##### Example:

```
phobos release list \
  --limit 5 \
  --org-name my-organization \
  --project-name my-project \
  --json
```

---

#### release remove-participant

##### Remove a participant from a release.

```
Usage: phobos [global options] release remove-participant [options] <release-id>
```

  The release remove-participant command removes one or more participants
  from a release. The -json flag optionally allows showing the final
  output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

- `--team-id`: ID of the team participant to remove from the release.

- `--user-id`: ID of the user participant to remove from the release.

</details>

##### Example:

```
phobos release remove-participant \
  --user-id prn:user:random.person \
  prn:release:my-org/my-project/0.0.17
```

---

#### release update

##### Update a release.

```
Usage: phobos [global options] release update [options] <id>
```

  The release update command updates a release. The -json
  flag optionally allows showing the final output as
  formatted JSON.

<details>
<summary>Expand options</summary>

- `--due-date`: Due date for the release. Format must be '2006-01-02T15:04:05Z07:00'. As a special case, supplying 'none' will remove the due date.

- `--json`: Show final output as JSON.

- `--notes`: Notes associated with the release.

</details>

##### Example:

```
phobos release update \
  --due-date none \
  --notes "this is a note" \
  prn:release:my-org/my-project/0.0.17
```

---

#### release update-deployment

##### Update a release deployment.

```
Usage: phobos [global options] release update-deployment [options] <release-id>
```

  The release update-deployment command updates a release
  deployment. The -json flag optionally allows showing
  the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--directory`: Path to the directory that contains the deployment JSON input file.

- `--filename`: Name of the JSON input file.

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos release update-deployment \
  --filename my-deployment.json \
  prn:release:my-org/my-project/0.0.17
```

***

### Command: release-lifecycle

##### Perform operations on release lifecycles.

**Subcommands:**

```
create                       Create a release lifecycle.
delete                       Delete a release lifecycle.
get                          Retrieve a release lifecycle.
list                         Retrieve a paginated list of release lifecycles.
update                       Update a release lifecycle with a new lifecycle template.
validate                     Validate a release lifecycle template.
```

The release-lifecycle commands do operations on release lifecycles.
Subcommands provide create, validate, list, get, update and delete support.

---

#### release-lifecycle create

##### Create a release lifecycle.

```
Usage: phobos [global options] release-lifecycle create [options] name
```

  The release-lifecycle create command creates a new release lifecycle
  under the specified organization.  The -json flag optionally
  allows showing the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--directory`: Path to directory that contains the HCL input file.

- `--filename`: Name of HCL input file.

- `--json`: Show final output as JSON.

- `--org-name`: Name of the organization under which the release lifecycle will be created. (This flag is required.)

- `--project-name`: Name of the project under which the release lifecycle will be created.

</details>

##### Example:

```
phobos release-lifecycle create \
  --filename lifecycle-two-stages.hcl \
  --org-name my-org \
  --project-name my-project \
  example-release-lifecycle
```

---

#### release-lifecycle delete

##### Delete a release lifecycle.

```
Usage: phobos [global options] release-lifecycle delete [options] <id>
```

  The release-lifecycle delete command deletes a release lifecycle
  with the given ID.

<details>
<summary>Expand options</summary>

- `--version`: Metadata version of the resource to be deleted. In most cases, this is not required.

</details>

##### Example:

```
phobos release-lifecycle delete \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
```

---

#### release-lifecycle get

##### Retrieve a release lifecycle.

```
Usage: phobos [global options] release-lifecycle get <id>
```

  The release-lifecycle get command retrieves a release lifecycle by id.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos release-lifecycle get \
  --json \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
```

---

#### release-lifecycle list

##### Retrieve a paginated list of release lifecycles.

```
Usage: phobos [global options] release-lifecycle list [options]
```

  The release-lifecycle list command prints information about (likely
  multiple) release lifecycles. Supports pagination, filtering and
  sorting the output.

<details>
<summary>Expand options</summary>

- `--cursor`: The cursor string for manual pagination.

- `--json`: Show final output as JSON.

- `--limit`: Maximum number of result elements to return. Defaults to 100.

- `--org-name`: The organization to limit search. Required.

- `--project-name`: Name of the project under which the release lifecycle will be created.

- `--scope`: Scope to filter release lifecycles. (This flag may be repeated).

- `--search`: Filter to only release lifecycles with this name prefix.

- `--sort-order`: Sort in this direction, ASC or DESC.

</details>

##### Example:

```
phobos release-lifecycle list \
  --limit 5 \
  --org-name my-organization \
  --json
```

---

#### release-lifecycle update

##### Update a release lifecycle with a new lifecycle template.

```
Usage: phobos [global options] release-lifecycle update [options] <id>
```

  The release-lifecycle update command updates a release lifecycle with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.

<details>
<summary>Expand options</summary>

- `--directory`: Path to directory that contains the HCL input file.

- `--filename`: Name of HCL input file.

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos release-lifecycle update \
  --filename my-release-lifecycle.hcl \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
```

---

#### release-lifecycle validate

##### Validate a release lifecycle template.

```
Usage: phobos [global options] release-lifecycle validate
```

  The release-lifecycle validate command validates a
  release lifecycle template.

<details>
<summary>Expand options</summary>

- `--directory`: Path to directory that contains the HCL input file.

- `--filename`: Name of HCL input file.

</details>

##### Example:

```
phobos release-lifecycle validate \
  --filename my-release-lifecycle.hcl
```

***

### Command: sso

##### Log in to the OAuth2 provider and return an authentication token.

**Subcommands:**

```
login                        Log in to the OAuth2 provider and return an authentication token.
```

The sso command authenticates the CLI with the OAuth2 provider,
and allows making authenticated calls to Phobos backend.

---

#### sso login

##### Log in to the OAuth2 provider and return an authentication token.

```
Usage: phobos [global options] sso login
```

   The login command starts an embedded web server and opens
   a web browser page or tab pointed at said web server.
   That redirects to the OAuth2 provider's login page, where
   the user can sign in. If there is an SSO scheme active,
   that will sign in the user. The login command captures
   the authentication token for use in subsequent commands.

##### Example:

```
phobos sso login
```

***

### Command: version

##### Get the CLI's version.

```
Usage: phobos [global options] version
```

  The phobos version command returns the CLI's version.

<details>
<summary>Expand options</summary>

- `--json`: Show final output as JSON.

</details>

##### Example:

```
phobos version --json
```

