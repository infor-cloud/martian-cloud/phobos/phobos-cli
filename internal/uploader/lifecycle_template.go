package uploader

import (
	"context"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

type lifecycleTemplateWriter struct {
	client pb.LifecycleTemplates_UploadLifecycleTemplateClient
}

func (w *lifecycleTemplateWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadLifecycleTemplateRequest{
		Data: &pb.UploadLifecycleTemplateRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *lifecycleTemplateWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

// LifecycleTemplateUploader is an interface for uploading lifecycle templates.
type LifecycleTemplateUploader struct {
	ui     terminal.UI
	logger hclog.Logger
	client *client.Client
}

// NewLifecycleTemplateUploader returns a LifecycleTemplateUploader struct.
func NewLifecycleTemplateUploader(ui terminal.UI, logger hclog.Logger, client *client.Client) *LifecycleTemplateUploader {
	return &LifecycleTemplateUploader{
		ui:     ui,
		logger: logger,
		client: client,
	}
}

// Upload uploads a lifecycle template.
func (u *LifecycleTemplateUploader) Upload(ctx context.Context, lifecycleTemplateID string, reader io.Reader) error {
	input := &pb.UploadLifecycleTemplateRequest{
		Data: &pb.UploadLifecycleTemplateRequest_Info{
			Info: &pb.UploadLifecycleTemplateMetadata{
				Id: lifecycleTemplateID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("lifecycle-template upload input: %#v", input))

	uploadClient, err := u.client.LifecycleTemplatesClient.UploadLifecycleTemplate(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload lifecycle template client: %w", err)
	}

	err = uploadClient.Send(input)
	if err != nil {
		return fmt.Errorf("failed to send upload lifecycle template metadata: %w", err)
	}

	return upload(reader, u.ui, &lifecycleTemplateWriter{client: uploadClient}, u.logger)
}
