package uploader

import (
	"context"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

type pipelineTemplateWriter struct {
	client pb.PipelineTemplates_UploadPipelineTemplateClient
}

func (w *pipelineTemplateWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPipelineTemplateRequest{
		Data: &pb.UploadPipelineTemplateRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pipelineTemplateWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

// PipelineTemplateUploader is an interface for uploading pipeline templates.
type PipelineTemplateUploader struct {
	ui     terminal.UI
	logger hclog.Logger
	client *client.Client
}

// NewPipelineTemplateUploader returns a PipelineTemplateUploader struct.
func NewPipelineTemplateUploader(ui terminal.UI, logger hclog.Logger, client *client.Client) *PipelineTemplateUploader {
	return &PipelineTemplateUploader{
		ui:     ui,
		logger: logger,
		client: client,
	}
}

// Upload uploads a pipeline template.
func (u *PipelineTemplateUploader) Upload(ctx context.Context, pipelineTemplateID string, reader io.Reader) error {
	input := &pb.UploadPipelineTemplateRequest{
		Data: &pb.UploadPipelineTemplateRequest_Info{
			Info: &pb.UploadPipelineTemplateMetadata{
				Id: pipelineTemplateID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("pipeline-template upload input: %#v", input))

	uploadClient, err := u.client.PipelineTemplatesClient.UploadPipelineTemplate(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload pipeline template client: %w", err)
	}

	err = uploadClient.Send(input)
	if err != nil {
		return fmt.Errorf("failed to send upload pipeline template metadata: %w", err)
	}

	return upload(reader, u.ui, &pipelineTemplateWriter{client: uploadClient}, u.logger)
}
