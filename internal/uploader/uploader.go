// Package uploader provides various uploaders for uploading data to Phobos.
package uploader

import (
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
)

const chunkSize = 1024 * 1024

// Writer is an interface for writing chunks of data.
type Writer interface {
	Write(chunk []byte) error
	Close() error
}

func upload(reader io.Reader, ui terminal.UI, w Writer, logger hclog.Logger) error {
	// Prepare to provide live status while uploading.
	liveStatus := ui.Status()
	counter := 0
	chunkData := make([]byte, chunkSize)
	for {

		// Show live status to the user, because this might take a while.
		liveStatus.Update(fmt.Sprintf("uploading chunk %d", counter))
		counter++

		n, cErr := reader.Read(chunkData)
		if cErr != nil {
			if cErr == io.EOF {
				break
			}
			return fmt.Errorf("%s: %w", "failed to read from HCL input file", cErr)
		}

		cErr = w.Write(chunkData[:n])
		if cErr != nil {
			// cErr is most likely EOF, which doesn't tell what really happened, so discard cErr.
			break
		}
	}

	liveStatus.Close()
	logger.Debug("last chunk uploaded")

	err := w.Close()

	if err != nil && err != io.EOF {
		return err
	}

	return nil
}
