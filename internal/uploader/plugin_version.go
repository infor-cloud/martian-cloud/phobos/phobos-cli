package uploader

import (
	"context"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

// pluginPlatformBinaryWriter is used for writing plugin platform binary.
type pluginPlatformBinaryWriter struct {
	client pb.PluginRegistry_UploadPluginPlatformBinaryClient
}

func (w *pluginPlatformBinaryWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPluginPlatformBinaryRequest{
		Data: &pb.UploadPluginPlatformBinaryRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pluginPlatformBinaryWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

type pluginVersionDocFileWriter struct {
	client pb.PluginRegistry_UploadPluginVersionDocFileClient
}

func (w *pluginVersionDocFileWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPluginVersionDocFileRequest{
		Data: &pb.UploadPluginVersionDocFileRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pluginVersionDocFileWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

// pluginVersionReadmeWriter is used for writing plugin version README.
type pluginVersionReadmeWriter struct {
	client pb.PluginRegistry_UploadPluginVersionReadmeClient
}

func (w *pluginVersionReadmeWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPluginVersionReadmeRequest{
		Data: &pb.UploadPluginVersionReadmeRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pluginVersionReadmeWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

type pluginVersionSchemaWriter struct {
	client pb.PluginRegistry_UploadPluginVersionSchemaClient
}

func (w *pluginVersionSchemaWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPluginVersionSchemaRequest{
		Data: &pb.UploadPluginVersionSchemaRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pluginVersionSchemaWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

// pluginVersionShaSumsWriter is used for writing plugin version checksums.
type pluginVersionShaSumsWriter struct {
	client pb.PluginRegistry_UploadPluginVersionShaSumsClient
}

func (w *pluginVersionShaSumsWriter) Write(chunk []byte) error {
	chunkInput := &pb.UploadPluginVersionShaSumsRequest{
		Data: &pb.UploadPluginVersionShaSumsRequest_ChunkData{
			ChunkData: chunk,
		},
	}

	return w.client.Send(chunkInput)
}

func (w *pluginVersionShaSumsWriter) Close() error {
	_, err := w.client.CloseAndRecv()
	return err
}

// PluginVersionUploader is used for uploading plugin versions.
type PluginVersionUploader struct {
	ui     terminal.UI
	logger hclog.Logger
	client *client.Client
}

// NewPluginVersionUploader returns a PluginVersionUploader struct.
func NewPluginVersionUploader(
	ui terminal.UI,
	logger hclog.Logger,
	client *client.Client,
) *PluginVersionUploader {
	return &PluginVersionUploader{
		ui:     ui,
		logger: logger,
		client: client,
	}
}

// UploadPlatformBinary uploads a plugin version platform binary.
func (u *PluginVersionUploader) UploadPlatformBinary(ctx context.Context, pluginPlatformID string, reader io.Reader) error {
	input := &pb.UploadPluginPlatformBinaryRequest{
		Data: &pb.UploadPluginPlatformBinaryRequest_Info{
			Info: &pb.UploadPluginPlatformBinaryMetadata{
				PluginPlatformId: pluginPlatformID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("plugin version upload platform binary input: %#v", input))

	uploadClient, err := u.client.PluginRegistryClient.UploadPluginPlatformBinary(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload plugin platform binary client: %w", err)
	}

	if err = uploadClient.Send(input); err != nil {
		return fmt.Errorf("failed to send upload plugin platform binary metadata: %w", err)
	}

	return upload(reader, u.ui, &pluginPlatformBinaryWriter{client: uploadClient}, u.logger)
}

// UploadDocFile uploads a plugin version doc file.
func (u *PluginVersionUploader) UploadDocFile(
	ctx context.Context,
	pluginVersionID string,
	category string,
	subcategory string,
	title string,
	name string,
	reader io.Reader,
) error {
	input := &pb.UploadPluginVersionDocFileRequest{
		Data: &pb.UploadPluginVersionDocFileRequest_Info{
			Info: &pb.UploadPluginVersionDocFileMetadata{
				PluginVersionId: pluginVersionID,
				Category:        pb.PluginVersionDocFileCategory(pb.PluginVersionDocFileCategory_value[category]),
				Subcategory:     subcategory,
				Title:           title,
				Name:            name,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("plugin version upload doc file input: %#v", input))

	uploadClient, err := u.client.PluginRegistryClient.UploadPluginVersionDocFile(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload plugin version doc file client: %w", err)
	}

	if err = uploadClient.Send(input); err != nil {
		return fmt.Errorf("failed to send upload plugin version doc file metadata: %w", err)
	}

	return upload(reader, u.ui, &pluginVersionDocFileWriter{client: uploadClient}, u.logger)
}

// UploadReadme uploads a plugin version README.
func (u *PluginVersionUploader) UploadReadme(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	input := &pb.UploadPluginVersionReadmeRequest{
		Data: &pb.UploadPluginVersionReadmeRequest_Info{
			Info: &pb.UploadPluginVersionReadmeMetadata{
				PluginVersionId: pluginVersionID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("plugin version upload readme input: %#v", input))

	uploadClient, err := u.client.PluginRegistryClient.UploadPluginVersionReadme(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload plugin version readme client: %w", err)
	}

	if err = uploadClient.Send(input); err != nil {
		return fmt.Errorf("failed to send upload plugin version readme metadata: %w", err)
	}

	return upload(reader, u.ui, &pluginVersionReadmeWriter{client: uploadClient}, u.logger)
}

// UploadSchema uploads a plugin version schema.
func (u *PluginVersionUploader) UploadSchema(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	input := &pb.UploadPluginVersionSchemaRequest{
		Data: &pb.UploadPluginVersionSchemaRequest_Info{
			Info: &pb.UploadPluginVersionReadmeMetadata{
				PluginVersionId: pluginVersionID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("plugin version upload schema input: %#v", input))

	uploadClient, err := u.client.PluginRegistryClient.UploadPluginVersionSchema(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload plugin version schema client: %w", err)
	}

	if err = uploadClient.Send(input); err != nil {
		return fmt.Errorf("failed to send upload plugin version schema metadata: %w", err)
	}

	return upload(reader, u.ui, &pluginVersionSchemaWriter{client: uploadClient}, u.logger)
}

// UploadShaSums uploads a plugin version checksums.
func (u *PluginVersionUploader) UploadShaSums(ctx context.Context, pluginVersionID string, reader io.Reader) error {
	input := &pb.UploadPluginVersionShaSumsRequest{
		Data: &pb.UploadPluginVersionShaSumsRequest_Info{
			Info: &pb.UploadPluginVersionShaSumsMetadata{
				PluginVersionId: pluginVersionID,
			},
		},
	}

	u.logger.Debug(fmt.Sprintf("plugin version upload sha sums input: %#v", input))

	uploadClient, err := u.client.PluginRegistryClient.UploadPluginVersionShaSums(ctx)
	if err != nil {
		return fmt.Errorf("failed to get upload plugin version sha sums client: %w", err)
	}

	if err = uploadClient.Send(input); err != nil {
		return fmt.Errorf("failed to send upload plugin version sha sums metadata: %w", err)
	}

	return upload(reader, u.ui, &pluginVersionShaSumsWriter{client: uploadClient}, u.logger)
}
