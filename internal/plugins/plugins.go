// Package plugins provides built-in plugins.
package plugins

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/plugins/exec"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
)

// PluginMetadata is the structure for plugin metadata.
type PluginMetadata struct {
	Plugin      pipeline.Plugin `json:"-"`
	Description string          `json:"description"`
}

// builtinPlugins is a map of the built-in plugins.
var builtinPlugins = map[string]*PluginMetadata{
	"exec": {
		Description: "Execute a command.",
		Plugin:      &exec.Plugin{},
	},
}

// ListPlugins returns a map of the built-in plugins and their metadata.
func ListPlugins() map[string]*PluginMetadata {
	return builtinPlugins
}
