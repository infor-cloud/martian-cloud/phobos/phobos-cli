// Package exec contains the exec plugin.
package exec

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"runtime"

	"github.com/armon/circbuf"
	"github.com/hashicorp/go-hclog"
	"github.com/joho/godotenv"
	"github.com/mitchellh/go-linereader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

var (
	_ pipeline.ConfigurableAction = (*cmdAction)(nil)
	_ pipeline.ActionInput        = (*CmdActionInputData)(nil)
)

const maxBufSize = 8 * 1024

// CmdActionResponse is the response struct returned from the action.
// These values will be exposed as action outputs so they can be consumed by
// subsequent actions in the pipeline
type CmdActionResponse struct {
	DotEnv   map[string]string
	ExitCode int
}

// CmdActionInputData is the input data for this action
type CmdActionInputData struct {
	DotEnvFilename *string `hcl:"dot_env_filename,optional"`
	Command        string  `hcl:"command,attr"`
}

// IsActionInput is a marker function to indicate that this struct is used for action input
func (e *CmdActionInputData) IsActionInput() {}

// cmdAction is the action implementation
type cmdAction struct{}

// NewCmdAction returns a new action instance
func NewCmdAction() pipeline.Action {
	return &cmdAction{}
}

func (g *cmdAction) Configure(_ interface{}, _ hclog.Logger) error {
	return nil
}

func (g *cmdAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "command",
		Description: "Executes a command string in a shell",
	}
}

// ExecuteFunc returns the function which will execute the action.
func (g *cmdAction) ExecuteFunc() interface{} {
	return g.execute
}

// This function contains modified code from https://github.com/hashicorp/terraform/blob/main/internal/builtin/provisioners/local-exec/resource_provisioner.go
// Copied code is licensed under Mozilla Public License, version 2.0
func (g *cmdAction) execute(
	ctx context.Context,
	ui terminal.UI,
	logger hclog.Logger,
	input *CmdActionInputData,
) (*CmdActionResponse, error) {
	var cmdargs []string
	if runtime.GOOS == "windows" {
		cmdargs = []string{"cmd", "/C"}
	} else {
		cmdargs = []string{"/bin/sh", "-c"}
	}

	cmdargs = append(cmdargs, input.Command)

	workingdir := ""

	// Set up the reader that will read the output from the command.
	// We use an os.Pipe so that the *os.File can be passed directly to the
	// process, and not rely on goroutines copying the data which may block.
	// See golang.org/issue/18874
	pr, pw, err := os.Pipe()
	if err != nil {
		return nil, fmt.Errorf("failed to initialize pipe for output: %s", err)
	}

	cmdEnv := os.Environ()

	// Set up the command
	cmd := exec.CommandContext(ctx, cmdargs[0], cmdargs[1:]...) // nosemgrep: gosec.G204-1
	cmd.Stderr = pw
	cmd.Stdout = pw
	// Dir specifies the working directory of the command.
	// If Dir is the empty string (this is default), runs the command
	// in the calling process's current directory.
	cmd.Dir = workingdir
	// Env specifies the environment of the command.
	// By default will use the calling process's environment
	cmd.Env = cmdEnv

	output, _ := circbuf.NewBuffer(maxBufSize)

	// Write everything we read from the pipe to the output buffer too
	tee := io.TeeReader(pr, output)

	// copy the teed output to the UI output
	copyDoneCh := make(chan struct{})
	go copyUIOutput(ui, tee, copyDoneCh)

	// Output what we're about to run
	logger.Debug(fmt.Sprintf("Executing: %q", cmdargs))

	// Start the command
	err = cmd.Start()
	if err == nil {
		err = cmd.Wait()
	}

	// Close the write-end of the pipe so that the goroutine mirroring output
	// ends properly.
	pw.Close()

	// Cancelling the command may block the pipe reader if the file descriptor
	// was passed to a child process which hasn't closed it. In this case the
	// copyOutput goroutine will just hang out until exit.
	select {
	case <-copyDoneCh:
	case <-ctx.Done():
	}

	exitCode := 0
	if err != nil {
		if exiterr, ok := err.(*exec.ExitError); ok {
			exitCode = exiterr.ExitCode()
		} else {
			return nil, fmt.Errorf("error running command '%s': %v. Output: %s", input.Command, err, output.Bytes())
		}
	}

	// If the optional dotenv input filename was specified, parse the file and add the parsed outputs here.
	dotEnvOutput := map[string]string{}
	if input.DotEnvFilename != nil {
		dotEnvOutput, err = godotenv.Read(*input.DotEnvFilename)
		if err != nil {
			return nil, fmt.Errorf("failed to read and parse action_outputs dotenv file: %s", *input.DotEnvFilename)
		}
	}

	return &CmdActionResponse{
		ExitCode: exitCode,
		DotEnv:   dotEnvOutput,
	}, nil
}

func copyUIOutput(o terminal.UI, r io.Reader, doneCh chan<- struct{}) {
	defer close(doneCh)
	lr := linereader.New(r)
	for line := range lr.Ch {
		o.Output(line)
	}
}
