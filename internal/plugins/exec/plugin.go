package exec

import (
	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
)

var _ pipeline.Plugin = (*Plugin)(nil)

// PluginConfig defines the required config fields for this provider.
// HCL tags are used for defining the accepted fields https://pkg.go.dev/github.com/hashicorp/hcl/v2/gohcl#pkg-overview
type PluginConfig struct {
}

// Plugin is the provider implementation
type Plugin struct {
	config PluginConfig
}

// Config returns the config struct for this provider
func (b *Plugin) Config() (interface{}, error) {
	// Config values will be populated automatically
	return &b.config, nil
}

// ValidateConfig validates the config struct for this provider
func (b *Plugin) ValidateConfig(_ interface{}) error {
	// The config is already validated based on the hcl tags in the config
	// struct but extra validation logic can go here if needed
	return nil
}

// PluginData returns data that can be consumed by actions
func (b *Plugin) PluginData(_ hclog.Logger) (interface{}, error) {
	// This provider does not have any data
	return nil, nil
}

// GetActions returns the actions that this provider supports
func (b *Plugin) GetActions() []func() pipeline.Action {
	return []func() pipeline.Action{NewCmdAction}
}
