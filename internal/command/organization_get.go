package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// organizationGetCommand is the structure for organization get command.
type organizationGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*organizationGetCommand)(nil)

func (c *organizationGetCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewOrganizationGetCommandFactory returns an organizationGetCommand struct.
func NewOrganizationGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &organizationGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *organizationGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("organization get"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.GetOrganizationByIdRequest{
		Id: newResourcePRN("organization", c.arguments[0]),
	}

	c.Logger.Debug(fmt.Sprintf("organization get input: %#v", input))

	org, err := c.client.OrganizationsClient.GetOrganizationByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get organization"))
		return 1
	}

	return outputOrganization(c.UI, c.toJSON, org)
}

func (c *organizationGetCommand) Synopsis() string {
	return "Retrieve an organization."
}

func (c *organizationGetCommand) Usage() string {
	return "phobos [global options] organization get <name>"
}

func (c *organizationGetCommand) Description() string {
	return `
  The organization get command retrieves an organization by name.
`
}

func (c *organizationGetCommand) Example() string {
	return `
phobos organization get \
  --json \
  example-org
`
}

func (c *organizationGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputOrganization is the output for most organization operations.
func outputOrganization(ui terminal.UI, toJSON bool, org *pb.Organization) int {
	if toJSON {
		buf, err := objectToJSON(org)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "description")
		t.Rich([]string{
			org.GetMetadata().Id,
			org.Name,
			org.Description,
		}, nil)

		ui.Table(t)
	}

	return 0
}
