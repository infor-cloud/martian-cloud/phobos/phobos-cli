package command

import (
	"flag"
	"fmt"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginGetCommand is the structure for plugin get command.
type pluginGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*pluginGetCommand)(nil)

func (c *pluginGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginGetCommandFactory returns a pluginGetCommand struct.
func NewPluginGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin get"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.GetPluginByIdRequest{Id: c.arguments[0]}

	c.Logger.Debug(fmt.Sprintf("plugin get input: %#v", input))

	plugin, err := c.client.PluginRegistryClient.GetPluginByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get plugin"))
		return 1
	}

	return outputPlugin(c.UI, c.toJSON, plugin)
}

func (*pluginGetCommand) Synopsis() string {
	return "Retrieve a plugin from the plugin registry."
}

func (c *pluginGetCommand) Usage() string {
	return "phobos [global options] plugin get [options] <id>"
}

func (c *pluginGetCommand) Description() string {
	return `
  The plugin get command retrieves a plugin from the
  plugin registry. -json flag can be used to get the
  final output as JSON.
`
}

func (c *pluginGetCommand) Example() string {
	return `
phobos plugin get \
  --json \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
`
}

func (c *pluginGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputPlugin is the output for most plugin operations.
func outputPlugin(ui terminal.UI, toJSON bool, plugin *pb.Plugin) int {
	if toJSON {
		buf, err := objectToJSON(plugin)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "organization", "repository_url", "private")
		t.Rich([]string{
			plugin.Metadata.Id,
			plugin.Name,
			plugin.OrganizationId,
			plugin.RepositoryUrl,
			strconv.FormatBool(plugin.Private),
		}, nil)
		ui.Table(t)
	}

	return 0
}
