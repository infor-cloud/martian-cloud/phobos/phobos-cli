package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginGetPlatformCommand is the structure for plugin get-platform command.
type pluginGetPlatformCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*pluginGetPlatformCommand)(nil)

func (c *pluginGetPlatformCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginGetPlatformCommandFactory returns a pluginGetPlatformCommand struct.
func NewPluginGetPlatformCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginGetPlatformCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginGetPlatformCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin get-platform"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.GetPluginPlatformByIdRequest{Id: c.arguments[0]}

	c.Logger.Debug(fmt.Sprintf("plugin get-platform input: %#v", input))

	pluginPlatform, err := c.client.PluginRegistryClient.GetPluginPlatformByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get plugin platform"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(pluginPlatform)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "plugin version id", "operating system", "architecture")
		t.Rich([]string{
			pluginPlatform.Metadata.Id,
			pluginPlatform.PluginVersionId,
			pluginPlatform.OperatingSystem,
			pluginPlatform.Architecture,
		}, nil)
		c.UI.Table(t)
	}

	return 0
}

func (*pluginGetPlatformCommand) Synopsis() string {
	return "Retrieve a plugin platform from the plugin registry."
}

func (c *pluginGetPlatformCommand) Usage() string {
	return "phobos [global options] plugin get-platform [options] <id>"
}

func (c *pluginGetPlatformCommand) Description() string {
	return `
  The plugin get platform command retrieves a plugin platform
  from the plugin registry. -json flag can be used to get
  the final output as JSON.
`
}

func (c *pluginGetPlatformCommand) Example() string {
	return `
phobos plugin get-platform \
  prn:plugin_platform:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9/windows/amd64
`
}

func (c *pluginGetPlatformCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
