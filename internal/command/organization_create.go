package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// organizationCreateCommand is the structure for organization create command.
type organizationCreateCommand struct {
	*BaseCommand

	description string
	toJSON      bool
}

var _ Command = (*organizationCreateCommand)(nil)

func (c *organizationCreateCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewOrganizationCreateCommandFactory returns an organizationCreateCommand struct.
func NewOrganizationCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &organizationCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *organizationCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("organization create"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.CreateOrganizationRequest{
		Name:        c.arguments[0],
		Description: c.description,
	}

	c.Logger.Debug(fmt.Sprintf("organization create input: %#v", input))

	createdOrg, err := c.client.OrganizationsClient.CreateOrganization(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create organization"))
		return 1
	}

	return outputOrganization(c.UI, c.toJSON, createdOrg)
}

func (c *organizationCreateCommand) Synopsis() string {
	return "Create an organization."
}

func (c *organizationCreateCommand) Usage() string {
	return "phobos [global options] organization create [options] <name>"
}

func (c *organizationCreateCommand) Description() string {
	return `
  The organization create command creates a new organization
  with the given name and description. -json flag optionally
  allows showing the final output as formatted JSON.
`
}

func (c *organizationCreateCommand) Example() string {
	return `
phobos organization create \
  --description \
  --json example-org
`
}

func (c *organizationCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.description,
		"description",
		"",
		"Description for the new organization.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
