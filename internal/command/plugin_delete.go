package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginDeleteCommand is the structure for plugin delete command.
type pluginDeleteCommand struct {
	*BaseCommand

	version *string
}

var _ Command = (*pluginDeleteCommand)(nil)

func (c *pluginDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int, validation.When(c.version != nil)),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginDeleteCommandFactory returns a pluginDeleteCommand struct.
func NewPluginDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin delete"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.DeletePluginRequest{
		Id:      c.arguments[0],
		Version: c.version,
	}

	c.Logger.Debug(fmt.Sprintf("plugin delete input: %#v", input))

	if _, err := c.client.PluginRegistryClient.DeletePlugin(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete plugin"))
		return 1
	}

	c.UI.Output("Plugin deleted successfully.", terminal.WithSuccessStyle())
	return 0
}

func (*pluginDeleteCommand) Synopsis() string {
	return "Delete a plugin from the plugin registry."
}

func (c *pluginDeleteCommand) Usage() string {
	return "phobos [global options] plugin delete [options] <id>"
}

func (c *pluginDeleteCommand) Description() string {
	return `
  The plugin delete command deletes a plugin from the plugin registry.
`
}

func (c *pluginDeleteCommand) Example() string {
	return `
phobos plugin delete \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
`
}

func (c *pluginDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"version",
		"Metadata version of the resource to be deleted. In most cases, this is not required.",
		func(s string) error {
			c.version = &s
			return nil
		},
	)

	return f
}
