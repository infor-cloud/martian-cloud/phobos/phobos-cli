package command

import (
	"flag"
	"fmt"
	"os"
	"path"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/uploader"
)

// releaseLifecycleUpdateCommand is the structure for release lifecycle update command.
type releaseLifecycleUpdateCommand struct {
	*BaseCommand

	directory string
	filename  string
	toJSON    bool
}

var _ Command = (*releaseLifecycleUpdateCommand)(nil)

func (c *releaseLifecycleUpdateCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseLifecycleUpdateCommandFactory returns a releaseLifecycleUpdateCommand struct.
func NewReleaseLifecycleUpdateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseLifecycleUpdateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseLifecycleUpdateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release-lifecycle update"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	// Get existing release lifecycle
	releaseLifecycle, err := c.client.ReleaseLifecyclesClient.GetReleaseLifecycleByID(c.Context, &pb.GetReleaseLifecycleByIdRequest{
		Id: c.arguments[0],
	})
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get existing release lifecycle"))
		return 1
	}

	// Open the input file to make sure that succeeds.
	fh, err := os.Open(path.Join(c.directory, c.filename)) // nosemgrep: gosec.G304-1
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to open HCL input file"))
		return 1
	}
	defer fh.Close()

	lifecycleTemplateToCreate := &pb.CreateLifecycleTemplateRequest{}

	switch releaseLifecycle.Scope {
	case pb.ScopeType_ORGANIZATION.String():
		lifecycleTemplateToCreate.OrgId = &releaseLifecycle.OrgId
		lifecycleTemplateToCreate.Scope = pb.ScopeType_ORGANIZATION
	case pb.ScopeType_PROJECT.String():
		lifecycleTemplateToCreate.ProjectId = releaseLifecycle.ProjectId
		lifecycleTemplateToCreate.Scope = pb.ScopeType_PROJECT
	}

	c.Logger.Debug(fmt.Sprintf("lifecycle-template create input: %#v", lifecycleTemplateToCreate))

	lifecycleTemplate, err := c.client.LifecycleTemplatesClient.CreateLifecycleTemplate(c.Context, lifecycleTemplateToCreate)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create release lifecycle template"))
		return 1
	}

	// Upload the template.
	err = uploader.NewLifecycleTemplateUploader(c.UI, c.Logger, c.client).Upload(c.Context, lifecycleTemplate.Metadata.Id, fh)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload release lifecycle template"))
		return 1
	}

	input := &pb.UpdateReleaseLifecycleRequest{
		Id:                  c.arguments[0],
		LifecycleTemplateId: lifecycleTemplate.Metadata.Id,
	}

	c.Logger.Debug(fmt.Sprintf("release lifecycle update input: %#v", input))

	updatedReleaseLifecycle, err := c.client.ReleaseLifecyclesClient.UpdateReleaseLifecycle(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update release lifecycle"))
		return 1
	}

	return outputReleaseLifecycle(c.UI, c.toJSON, updatedReleaseLifecycle)
}

func (c *releaseLifecycleUpdateCommand) Usage() string {
	return "phobos [global options] release-lifecycle update [options] <id>"
}

func (c *releaseLifecycleUpdateCommand) Description() string {
	return `
  The release-lifecycle update command updates a release lifecycle with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.
`
}

func (c *releaseLifecycleUpdateCommand) Synopsis() string {
	return "Update a release lifecycle with a new lifecycle template."
}

func (c *releaseLifecycleUpdateCommand) Example() string {
	return `
phobos release-lifecycle update \
  --filename my-release-lifecycle.hcl \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
`
}

func (c *releaseLifecycleUpdateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to directory that contains the HCL input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultReleaseLifecycleFilename,
		"Name of HCL input file.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
