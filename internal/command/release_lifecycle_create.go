package command

import (
	"flag"
	"fmt"
	"os"
	"path"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/uploader"
)

const (
	defaultReleaseLifecycleFilename = "lifecycle.hcl"
)

// releaseLifecycleCreateCommand is the structure for release lifecycle create command.
type releaseLifecycleCreateCommand struct {
	*BaseCommand

	projectName      *string
	organizationName string
	directory        string
	filename         string
	toJSON           bool
}

var _ Command = (*releaseLifecycleCreateCommand)(nil)

func (c *releaseLifecycleCreateCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
		validation.Field(&c.organizationName, validation.Required),
	)
}

// NewReleaseLifecycleCreateCommandFactory returns a releaseLifecycleCreateCommand struct.
func NewReleaseLifecycleCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseLifecycleCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseLifecycleCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release-lifecycle create"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	// Open the input file to make sure that succeeds.
	fh, err := os.Open(path.Join(c.directory, c.filename)) // nosemgrep: gosec.G304-1
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to open HCL input file"))
		return 1
	}
	defer fh.Close()

	var (
		orgPRN                    = newResourcePRN("organization", c.organizationName)
		lifecycleTemplateToCreate *pb.CreateLifecycleTemplateRequest
		projectPRN                *string
	)

	// Cannot specify both project and organization since the API does not support it.
	if c.projectName != nil {
		projectPRN = ptr.String(newResourcePRN("project", c.organizationName, *c.projectName))
		lifecycleTemplateToCreate = &pb.CreateLifecycleTemplateRequest{
			Scope:     pb.ScopeType_PROJECT,
			ProjectId: projectPRN,
		}
	} else {
		lifecycleTemplateToCreate = &pb.CreateLifecycleTemplateRequest{
			Scope: pb.ScopeType_ORGANIZATION,
			OrgId: &orgPRN,
		}
	}

	c.Logger.Debug(fmt.Sprintf("lifecycle-template create input: %#v", lifecycleTemplateToCreate))

	lifecycleTemplate, err := c.client.LifecycleTemplatesClient.CreateLifecycleTemplate(c.Context, lifecycleTemplateToCreate)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create release lifecycle template"))
		return 1
	}

	// Upload the template.
	err = uploader.NewLifecycleTemplateUploader(c.UI, c.Logger, c.client).Upload(c.Context, lifecycleTemplate.Metadata.Id, fh)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload release lifecycle template"))
		return 1
	}

	var createReleaseLifecycleInput *pb.CreateReleaseLifecycleRequest

	if c.projectName != nil {
		createReleaseLifecycleInput = &pb.CreateReleaseLifecycleRequest{
			Scope:               pb.ScopeType_PROJECT,
			ProjectId:           projectPRN,
			LifecycleTemplateId: lifecycleTemplate.Metadata.Id,
			Name:                c.arguments[0],
		}
	} else {
		createReleaseLifecycleInput = &pb.CreateReleaseLifecycleRequest{
			Scope:               pb.ScopeType_ORGANIZATION,
			OrgId:               &orgPRN,
			LifecycleTemplateId: lifecycleTemplate.Metadata.Id,
			Name:                c.arguments[0],
		}
	}

	c.Logger.Debug(fmt.Sprintf("release lifecycle create input: %#v", createReleaseLifecycleInput))

	createdReleaseLifecycle, err := c.client.ReleaseLifecyclesClient.CreateReleaseLifecycle(c.Context, createReleaseLifecycleInput)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create release lifecycle"))
		return 1
	}

	return outputReleaseLifecycle(c.UI, c.toJSON, createdReleaseLifecycle)
}

func (c *releaseLifecycleCreateCommand) Synopsis() string {
	return "Create a release lifecycle."
}

func (c *releaseLifecycleCreateCommand) Usage() string {
	return "phobos [global options] release-lifecycle create [options] name"
}

func (c *releaseLifecycleCreateCommand) Description() string {
	return `
  The release-lifecycle create command creates a new release lifecycle
  under the specified organization.  The -json flag optionally
  allows showing the final output as formatted JSON.
`
}

func (c *releaseLifecycleCreateCommand) Example() string {
	return `
phobos release-lifecycle create \
  --filename lifecycle-two-stages.hcl \
  --org-name my-org \
  --project-name my-project \
  example-release-lifecycle
`
}

func (c *releaseLifecycleCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"Name of the organization under which the release lifecycle will be created. (This flag is required.)",
	)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to directory that contains the HCL input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultReleaseLifecycleFilename,
		"Name of HCL input file.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	f.Func(
		"project-name",
		"Name of the project under which the release lifecycle will be created.",
		func(s string) error {
			c.projectName = &s
			return nil
		},
	)

	return f
}
