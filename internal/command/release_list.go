package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseListCommand is the structure for release list command.
type releaseListCommand struct {
	*BaseCommand

	cursor      *string
	sortOrder   *string
	limit       *int32
	orgName     string
	projectName string
	toJSON      bool
}

var _ Command = (*releaseListCommand)(nil)

func (c *releaseListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.limit, validation.Min(0), validation.Max(100)),
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.orgName, validation.Required),
		validation.Field(&c.projectName, validation.Required),
	)
}

// NewReleaseListCommand returns a new releaseListCommand struct.
func NewReleaseListCommand(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release list"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	sortOrder := pb.ReleaseSortableField_CREATED_AT_ASC.Enum()
	if c.sortOrder != nil && strings.ToLower(*c.sortOrder) == "desc" {
		sortOrder = pb.ReleaseSortableField_CREATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetReleasesRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		ProjectId: newResourcePRN("project", c.orgName, c.projectName),
	}

	c.Logger.Debug(fmt.Sprintf("release list input: %#v", input))

	result, err := c.client.ReleasesClient.GetReleases(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of releases"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "semantic version", "due date", "pre release")

		for _, r := range result.Releases {
			dueDate := "none"
			if r.DueDate != nil {
				// Make due date more human friendly.
				dueDate = r.DueDate.AsTime().Local().Format(time.RFC1123)
			}

			t.Rich([]string{
				r.Metadata.Id,
				r.SemanticVersion,
				dueDate,
				strconv.FormatBool(r.PreRelease),
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *releaseListCommand) Synopsis() string {
	return "Retrieve a paginated list of releases."
}

func (c *releaseListCommand) Usage() string {
	return "phobos [global options] release list [options]"
}

func (c *releaseListCommand) Description() string {
	return `
  The release list command prints information about (likely
  multiple) releases. Supports pagination, filtering and
  sorting the output.
`
}

func (c *releaseListCommand) Example() string {
	return `
phobos release list \
  --limit 5 \
  --org-name my-organization \
  --project-name my-project \
  --json
`
}

func (c *releaseListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"cursor",
		"Cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"limit",
		"Maximum number of results to return. Defaults to 100.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.Func(
		"sort-order",
		"Sort in this direction, ASC or DESC.",
		func(s string) error {
			c.sortOrder = &s
			return nil
		},
	)
	f.StringVar(
		&c.orgName,
		"org-name",
		"",
		"The organization to limit search (This flag is required).",
	)
	f.StringVar(
		&c.projectName,
		"project-name",
		"",
		"The project to limit search (This flag is required).",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
