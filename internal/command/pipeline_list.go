package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineListCommand is the structure for pipeline list command.
type pipelineListCommand struct {
	*BaseCommand

	limit            *int32
	cursor           *string
	organizationName string
	projectName      string
	sortOrder        string
	toJSON           bool
}

var _ Command = (*pipelineListCommand)(nil)

func (c *pipelineListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.limit, validation.Min(0), validation.Max(100), validation.When(c.limit != nil)),
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.organizationName, validation.Required),
		validation.Field(&c.projectName, validation.Required),
	)
}

// NewPipelineListCommandFactory returns a pipelineListCommand struct.
func NewPipelineListCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline list"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	sortOrder := pb.PipelineSortableField_UPDATED_AT_ASC.Enum()
	if strings.ToLower(c.sortOrder) == "desc" {
		sortOrder = pb.PipelineSortableField_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetPipelinesRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		ProjectId: newResourcePRN("project", c.organizationName, c.projectName),
	}

	c.Logger.Debug(fmt.Sprintf("pipeline list input: %#v", input))

	result, err := c.client.PipelinesClient.GetPipelines(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of pipelines"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "type", "pipeline-template")

		for _, p := range result.Pipelines {
			t.Rich([]string{
				p.GetMetadata().Id,
				p.Type,
				p.PipelineTemplateId,
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *pipelineListCommand) Synopsis() string {
	return "Retrieve a paginated list of pipelines."
}

func (c *pipelineListCommand) Usage() string {
	return "phobos [global options] pipeline list [options]"
}

func (c *pipelineListCommand) Description() string {
	return `
  The pipeline list command prints information about (likely
  multiple) pipelines. Supports pagination, filtering and
  sorting the output.
`
}

func (c *pipelineListCommand) Example() string {
	return `
phobos pipeline list \
  --limit 5 \
  --org-name my-organization \
  --project-name my-project \
  --json
`
}

func (c *pipelineListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"cursor",
		"The cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"limit",
		"Maximum number of results to return. Defaults to 100.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.StringVar(
		&c.sortOrder,
		"sort-order",
		"ASC",
		"Sort in this direction, ASC or DESC.",
	)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"The organization to limit search.  (This flag is required.)",
	)
	f.StringVar(
		&c.projectName,
		"project-name",
		"",
		"The project to limit search.  (This flag is required.)",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
