package command

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	// defaultDeploymentDocFilename is the default filename used to find the deployment JSON doc.
	defaultDeploymentDocFilename = "deployment.json"
)

// releaseUpdateDeploymentCommand is the structure for the release update-deployment command.
type releaseUpdateDeploymentCommand struct {
	*BaseCommand

	directory string
	filename  string
	toJSON    bool
}

var _ Command = (*releaseUpdateDeploymentCommand)(nil)

func (c *releaseUpdateDeploymentCommand) validate() error {
	const message = "release-id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseUpdateDeploymentCommandFactory returns a releaseUpdateDeploymentCommand struct.
func NewReleaseUpdateDeploymentCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseUpdateDeploymentCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseUpdateDeploymentCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release update-deployment"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	// Open the input file to make sure that succeeds.
	deploymentDoc, err := os.ReadFile(filepath.Join(c.directory, c.filename)) // nosemgrep: gosec.G304-1
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read deployment JSON doc"))
		return 1
	}

	var input pb.UpdateReleaseDeploymentPipelineRequest
	if err = protojson.Unmarshal(deploymentDoc, &input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to un-marshal deployment JSON doc"))
		return 1
	}

	// Add the release ID.
	input.ReleaseId = c.arguments[0]

	c.Logger.Debug(fmt.Sprintf("release update-deployment input: %#v", &input))

	pipeline, err := c.client.ReleasesClient.UpdateReleaseDeploymentPipeline(c.Context, &input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update release deployment"))
		return 1
	}

	return outputPipeline(c.UI, c.toJSON, pipeline)
}

func (*releaseUpdateDeploymentCommand) Synopsis() string {
	return "Update a release deployment."
}

func (c *releaseUpdateDeploymentCommand) Usage() string {
	return "phobos [global options] release update-deployment [options] <release-id>"
}

func (c *releaseUpdateDeploymentCommand) Description() string {
	return `
  The release update-deployment command updates a release
  deployment. The -json flag optionally allows showing
  the final output as formatted JSON.
`
}

func (c *releaseUpdateDeploymentCommand) Example() string {
	return `
phobos release update-deployment \
  --filename my-deployment.json \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseUpdateDeploymentCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to the directory that contains the deployment JSON input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultDeploymentDocFilename,
		"Name of the JSON input file.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	return f
}
