package command

import (
	"flag"
	"fmt"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseLifecycleGetCommand is the structure for release-lifecycle get command.
type releaseLifecycleGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*releaseLifecycleGetCommand)(nil)

func (c *releaseLifecycleGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewReleaseLifecycleGetCommandFactory returns a releaseLifecycleGetCommand struct.
func NewReleaseLifecycleGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseLifecycleGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseLifecycleGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release-lifecycle get"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.GetReleaseLifecycleByIdRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("release-lifecycle get input: %#v", input))

	p, err := c.client.ReleaseLifecyclesClient.GetReleaseLifecycleByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get release lifecycle"))
		return 1
	}

	return outputReleaseLifecycle(c.UI, c.toJSON, p)
}

func (c *releaseLifecycleGetCommand) Synopsis() string {
	return "Retrieve a release lifecycle."
}

func (c *releaseLifecycleGetCommand) Usage() string {
	return "phobos [global options] release-lifecycle get <id>"
}

func (c *releaseLifecycleGetCommand) Description() string {
	return `
  The release-lifecycle get command retrieves a release lifecycle by id.
`
}

func (c *releaseLifecycleGetCommand) Example() string {
	return `
phobos release-lifecycle get \
  --json \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
`
}

func (c *releaseLifecycleGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputReleaseLifecycle is the output for most release lifecycle operations.
func outputReleaseLifecycle(ui terminal.UI, toJSON bool, p *pb.ReleaseLifecycle) int {
	path := resourcePathFromPRN(p.Metadata.Prn)
	parts := strings.Split(path, "/")

	if toJSON {
		buf, err := objectToJSON(p)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "organization", "lifecycle-template")
		t.Rich([]string{
			p.GetMetadata().Id,
			parts[0],
			p.LifecycleTemplateId,
		}, nil)

		ui.Table(t)
	}

	return 0
}
