package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineTemplateDeleteCommand is the structure for pipeline template delete command.
type pipelineTemplateDeleteCommand struct {
	*BaseCommand

	version string
}

var _ Command = (*pipelineTemplateDeleteCommand)(nil)

func (c *pipelineTemplateDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPipelineTemplateDeleteCommandFactory returns a pipelineTemplateDeleteCommand struct.
func NewPipelineTemplateDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineTemplateDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineTemplateDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline-template delete"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.DeletePipelineTemplateRequest{
		Id: c.arguments[0],
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("pipeline template delete input: %#v", input))

	// Returned empty response is not needed.
	if _, err := c.client.PipelineTemplatesClient.DeletePipelineTemplate(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete pipeline template"))
		return 1
	}

	c.UI.Output("PipelineTemplate deleted successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *pipelineTemplateDeleteCommand) Synopsis() string {
	return "Delete a pipeline template."
}

func (c *pipelineTemplateDeleteCommand) Usage() string {
	return "phobos [global options] pipeline-template delete [options] <id>"
}

func (c *pipelineTemplateDeleteCommand) Description() string {
	return `
  The pipeline-template delete command deletes a pipeline template
  with the given ID.
`
}

func (c *pipelineTemplateDeleteCommand) Example() string {
	return `
phobos pipeline-template delete \
  prn:pipeline_template:my-org/my-project/MI3DKNBYGNQTCLJXMVSTKLJUGY4DSLLBMNRDKLJSHA4TOZRQGY3TKZBZMNPVAVA
`
}

func (c *pipelineTemplateDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be deleted. "+
			"In most cases, this is not required.",
	)

	return f
}
