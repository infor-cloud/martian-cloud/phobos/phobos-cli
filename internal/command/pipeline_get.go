package command

import (
	"flag"
	"fmt"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineGetCommand is the structure for pipeline get command.
type pipelineGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*pipelineGetCommand)(nil)

func (c *pipelineGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewPipelineGetCommandFactory returns a pipelineGetCommand struct.
func NewPipelineGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline get"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.GetPipelineByIdRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("pipeline get input: %#v", input))

	p, err := c.client.PipelinesClient.GetPipelineByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get pipeline"))
		return 1
	}

	return outputPipeline(c.UI, c.toJSON, p)
}

func (c *pipelineGetCommand) Synopsis() string {
	return "Retrieve a pipeline."
}

func (c *pipelineGetCommand) Usage() string {
	return "phobos [global options] pipeline get <id>"
}

func (c *pipelineGetCommand) Description() string {
	return `
  The pipeline get command retrieves a pipeline by id.
`
}

func (c *pipelineGetCommand) Example() string {
	return `
phobos pipeline get \
  prn:pipeline:my-org/my-project/MM3DEOLGGVSTILLEGJSTOLJUG4ZTQLJYMYZWMLLFHA3TEZLFGFTDEYZUGJPVASI
`
}

func (c *pipelineGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputPipeline is the output for most pipeline operations.
func outputPipeline(ui terminal.UI, toJSON bool, p *pb.Pipeline) int {
	resourcePath := resourcePathFromPRN(p.Metadata.Prn)
	parts := strings.Split(resourcePath, "/")

	if toJSON {
		buf, err := objectToJSON(p)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "organization", "project", "pipeline-template")
		t.Rich([]string{
			p.GetMetadata().Id,
			parts[0],
			parts[1],
			p.PipelineTemplateId,
		}, nil)

		ui.Table(t)
	}

	return 0
}
