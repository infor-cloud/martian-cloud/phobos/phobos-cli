package command

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/uploader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/varparser"
)

const (
	defaultPipelineFilename = "pipeline.hcl"
)

// pipelineCreateCommand is the structure for pipeline create command.
type pipelineCreateCommand struct {
	environmentName *string
	*BaseCommand
	filename            string
	templateID          string
	organizationName    string
	directory           string
	pipelineType        string
	variableSetRevision string
	hclVarFiles         arrayFlags
	hclVars             arrayFlags
	envVarFiles         arrayFlags
	envVars             arrayFlags
	annotations         arrayFlags
	toJSON              bool
	follow              bool
}

var _ Command = (*pipelineCreateCommand)(nil)

func (c *pipelineCreateCommand) validate() error {
	if c.templateID != "" && c.filename != "" {
		return validation.Errors{
			"template-id": errors.New("template-id and filename flags are mutually exclusive"),
			"filename":    errors.New("template-id and filename flags are mutually exclusive"),
		}
	}

	const message = "project-name is required"

	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
		validation.Field(&c.organizationName, validation.Required),
		validation.Field(&c.pipelineType, validation.Required, validation.In("runbook", "deployment")),
		validation.Field(&c.toJSON, validation.When(c.follow, validation.In(false).Error("json and follow flags are mutually exclusive"))),
		validation.Field(&c.follow, validation.When(c.toJSON, validation.In(false).Error("json and follow flags are mutually exclusive"))),
	)
}

// NewPipelineCreateCommandFactory returns a pipelineCreateCommand struct.
func NewPipelineCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline create"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	templateID := c.templateID
	if templateID != "" && strings.HasSuffix(templateID, "/latest") {
		// Split resource path
		resourcePathParts := strings.Split(resourcePathFromPRN(templateID), "/")
		if len(resourcePathParts) != 4 {
			c.UI.Output(output.FormatError(nil, "invalid template ID"))
			return 1
		}
		templateOrg := resourcePathParts[0]
		templateProj := resourcePathParts[1]
		templateName := resourcePathParts[2]

		templates, err := c.client.PipelineTemplatesClient.GetPipelineTemplates(c.Context, &pb.GetPipelineTemplatesRequest{
			ProjectId: newResourcePRN("project", templateOrg, templateProj),
			Name:      &templateName,
			Latest:    ptr.Bool(true),
		})
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get pipeline templates for project %s in org %s", templateProj, templateOrg))
			return 1
		}
		if len(templates.PipelineTemplates) == 0 {
			c.UI.Output(output.FormatError(nil, "no pipeline templates found for template with name %s", templateName))
			return 1
		}
		templateID = templates.PipelineTemplates[0].Metadata.Id
	}

	if templateID == "" {
		filename := c.filename
		if filename == "" {
			filename = defaultPipelineFilename
		}

		// Open the input file to make sure that succeeds.
		fh, err := os.Open(path.Join(c.directory, filename)) // nosemgrep: gosec.G304-1
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to open HCL input file"))
			return 1
		}
		defer fh.Close()

		pipelineTemplateInput := &pb.CreatePipelineTemplateRequest{
			ProjId: newResourcePRN("project", c.organizationName, c.arguments[0]),
		}

		c.Logger.Debug(fmt.Sprintf("pipeline-template create input: %#v", pipelineTemplateInput))

		pipelineTemplate, err := c.client.PipelineTemplatesClient.CreatePipelineTemplate(c.Context, pipelineTemplateInput)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to create pipeline template"))
			return 1
		}

		// Upload the pipeline template.
		err = uploader.NewPipelineTemplateUploader(c.UI, c.Logger, c.client).Upload(c.Context, pipelineTemplate.Metadata.Id, fh)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to upload pipeline template"))
			return 1
		}

		templateID = pipelineTemplate.Metadata.Id
	}

	hclVarsInput := &varparser.ParseHCLVariablesInput{
		HCLVarFilePaths: c.hclVarFiles,
		HCLVariables:    c.hclVars,
	}

	configDirectory := path.Dir(c.directory)
	parser := varparser.NewVariableParser(&configDirectory, true)

	hclVars, err := parser.ParseHCLVariables(hclVarsInput)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse HCL variables"))
		return 1
	}

	envVarsInput := &varparser.ParseEnvironmentVariablesInput{
		EnvVarFilePaths: c.envVarFiles,
		EnvVariables:    c.envVars,
	}

	envVars, err := parser.ParseEnvironmentVariables(envVarsInput)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse environment variables"))
		return 1
	}

	pbHCLVars := make([]*pb.PipelineVariable, len(hclVars))
	for ix, v := range hclVars {
		pbHCLVars[ix] = &pb.PipelineVariable{
			Key:   v.Key,
			Value: v.Value,
		}
	}

	pbEnvironmentVariables := make([]*pb.PipelineVariable, len(envVars))
	for ix, v := range envVars {
		pbEnvironmentVariables[ix] = &pb.PipelineVariable{
			Key:   v.Key,
			Value: v.Value,
		}
	}

	annotations := make([]*pb.PipelineAnnotation, len(c.annotations))
	for ix, strLabel := range c.annotations {
		var annotation pb.PipelineAnnotation
		if jErr := json.Unmarshal([]byte(strLabel), &annotation); jErr != nil {
			c.UI.Output(output.FormatError(jErr, "failed to unmarshal pipeline annotation"))
			return 1
		}
		annotations[ix] = &annotation
	}

	var pipelineType pb.PipelineType
	switch c.pipelineType {
	case "runbook":
		pipelineType = pb.PipelineType_RUNBOOK
	case "deployment":
		pipelineType = pb.PipelineType_DEPLOYMENT
	}

	input := &pb.CreatePipelineRequest{
		Type:                 pipelineType,
		PipelineTemplateId:   templateID,
		HclVariables:         pbHCLVars,
		EnvironmentVariables: pbEnvironmentVariables,
		Annotations:          annotations,
		EnvironmentName:      c.environmentName,
	}

	if c.variableSetRevision != "" {
		input.VariableSetRevision = &c.variableSetRevision
	}

	c.Logger.Debug(fmt.Sprintf("pipeline create input: %#v", input))

	createdPipeline, err := c.client.PipelinesClient.CreatePipeline(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create pipeline"))
		return 1
	}

	if c.follow {
		if err := output.NewPipelineRenderer(c.UI, c.client).Render(c.Context, createdPipeline.Metadata.Id); err != nil {
			c.UI.Output(output.FormatError(err, "failed to show pipeline"))
			return 1
		}
		return 0
	}

	return outputPipeline(c.UI, c.toJSON, createdPipeline)
}

func (c *pipelineCreateCommand) Synopsis() string {
	return "Create a pipeline."
}

func (c *pipelineCreateCommand) Usage() string {
	return "phobos [global options] pipeline create [options] project-name"
}

func (c *pipelineCreateCommand) Description() string {
	return `
  The pipeline create command creates a new pipeline under the
  specified organization and project. The -json flag optionally
  allows showing the final output as formatted JSON.

  HCL variables may be passed in via supported options or
  exported to the environment with a 'PB_VAR_' prefix.

  Variable parsing precedence:
  1. HCL variables from the environment.
  2. phobos.pbvars file from configuration's directory,
     if present.
  3. phobos.pbvars.json file from configuration's
     directory, if present.
  4. *.auto.pbvars, *.auto.pbvars.json files
     from the configuration's directory, if present.
  5. --pb-var-file option(s).
  6. --pb-var option(s).

  NOTE: If the same variable is assigned multiple values,
  the last value found will be used. A --pb-var option
  will override the values from a *.pbvars file which
  will override values from the environment (PB_VAR_).
`
}

func (c *pipelineCreateCommand) Example() string {
	return `
phobos pipeline create \
  --org-name my-organization \
  --type runbook \
  --filename my-pipeline-template.hcl \
  my-project
`
}

func (c *pipelineCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"Name of parent organization for the new pipeline. (This flag is required).",
	)
	f.StringVar(
		&c.pipelineType,
		"type",
		"",
		"The type of the pipeline. (runbook or deployment)",
	)
	f.StringVar(
		&c.templateID,
		"template-id",
		"",
		"ID of the pipeline template to use",
	)
	f.Func(
		"environment-name",
		"The name of the environment to which this pipeline will deploy resources to (only required for deployment pipelines)",
		func(s string) error {
			c.environmentName = &s
			return nil
		},
	)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to directory that contains the HCL input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		"",
		"Name of HCL input file.",
	)
	f.StringVar(
		&c.variableSetRevision,
		"variable-set-revision",
		"",
		"Project variable set revision or \"latest\" for latest variable set.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	f.BoolVar(
		&c.follow,
		"follow",
		false,
		"Show live pipeline output",
	)
	f.Var(&c.annotations,
		"annotation",
		"A key/value pair that will be associated with the pipeline. (This flag may be repeated).",
	)
	f.Var(&c.hclVarFiles,
		"pb-var-file",
		"Path to a HCL variable file (*.pbvars or *.pbvars.json). (This flag may be repeated).",
	)
	f.Var(&c.hclVars,
		"pb-var",
		"Define a HCL variable as a key=value. To supply a string, do --pb-var 'sample=this is a value'. "+
			"Non key=value pairs will be ignored. (This flag may be repeated).",
	)
	f.Var(&c.envVarFiles,
		"env-var-file",
		"Path to environment variable file. Environment variable files are "+
			"expected to be in the format of key=value per line. (This flag may be repeated).",
	)
	f.Var(&c.envVars,
		"env-var",
		"Define an environment variable. Environment variables are "+
			"expected to be in the format of key=value. Non key=value pairs will be ignored. "+
			"(This flag may be repeated).",
	)

	return f
}
