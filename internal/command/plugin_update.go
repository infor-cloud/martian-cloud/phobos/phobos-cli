package command

import (
	"flag"
	"fmt"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginUpdateCommand is the structure for plugin update command.
type pluginUpdateCommand struct {
	*BaseCommand

	repositoryURL *string
	version       *string
	private       *bool
	toJSON        bool
}

var _ Command = (*pluginUpdateCommand)(nil)

func (c *pluginUpdateCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int, validation.When(c.version != nil)),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginUpdateCommandFactory returns a pluginUpdateCommand struct.
func NewPluginUpdateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginUpdateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginUpdateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin update"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.UpdatePluginRequest{
		Id:            c.arguments[0],
		Version:       c.version,
		RepositoryUrl: c.repositoryURL,
		Private:       c.private,
	}

	c.Logger.Debug(fmt.Sprintf("plugin update input: %#v", input))

	plugin, err := c.client.PluginRegistryClient.UpdatePlugin(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update plugin"))
		return 1
	}

	return outputPlugin(c.UI, c.toJSON, plugin)
}

func (c *pluginUpdateCommand) Synopsis() string {
	return "Update a plugin in the plugin registry."
}

func (c *pluginUpdateCommand) Usage() string {
	return "phobos [global options] plugin update [options] <id>"
}

func (c *pluginUpdateCommand) Description() string {
	return `
  The plugin update command updates a plugin in the
  plugin registry. -json flag can be used to get
  the output in JSON format.
`
}

func (c *pluginUpdateCommand) Example() string {
	return `
phobos plugin update \
  --private=false \
  GZSDINBVGQ4TELJZMZSTQLJUMQ4TALJZG44DQLLCGJQTEMRSGA2GCYRVMZPVATA
`
}

func (c *pluginUpdateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"repository-url",
		"URL of the repository containing the plugin source code.",
		func(s string) error {
			c.repositoryURL = &s
			return nil
		},
	)
	f.BoolFunc(
		"private",
		"Keep the plugin private i.e. not visible to other organizations.",
		func(s string) error {
			b, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}
			c.private = &b
			return nil
		},
	)
	f.Func(
		"version",
		"Metadata version of the resource to be updated. In most cases, this is not required.",
		func(s string) error {
			c.version = &s
			return nil
		},
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
