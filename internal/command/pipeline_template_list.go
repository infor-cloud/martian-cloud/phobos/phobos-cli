package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineTemplateListCommand is the structure for pipeline template list command.
type pipelineTemplateListCommand struct {
	*BaseCommand

	versioned        *bool
	latest           *bool
	cursor           *string
	limit            *int32
	organizationName string
	projectName      string
	sortOrder        string
	search           string
	toJSON           bool
}

var _ Command = (*pipelineTemplateListCommand)(nil)

func (c *pipelineTemplateListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.limit, validation.Min(0), validation.Max(100), validation.When(c.limit != nil)),
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.organizationName, validation.Required),
		validation.Field(&c.projectName, validation.Required),
	)
}

// NewPipelineTemplateListCommandFactory returns a pipelineTemplateListCommand struct.
func NewPipelineTemplateListCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineTemplateListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineTemplateListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline-template list"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	sortOrder := pb.PipelineTemplateSortableField_UPDATED_AT_ASC.Enum()
	if strings.ToLower(c.sortOrder) == "desc" {
		sortOrder = pb.PipelineTemplateSortableField_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetPipelineTemplatesRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		ProjectId: newResourcePRN("project", c.organizationName, c.projectName),
		Versioned: c.versioned,
		Latest:    c.latest,
		Search:    &c.search,
	}

	c.Logger.Debug(fmt.Sprintf("pipeline-template list input: %#v", input))

	result, err := c.client.PipelineTemplatesClient.GetPipelineTemplates(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of pipeline templates"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "status", "versioned", "name", "semantic-version", "latest")

		for _, p := range result.PipelineTemplates {
			t.Rich([]string{
				p.Metadata.Id,
				p.Status,
				strconv.FormatBool(p.Versioned),
				p.GetName(),
				p.GetSemanticVersion(),
				strconv.FormatBool(p.GetLatest()),
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *pipelineTemplateListCommand) Synopsis() string {
	return "Retrieve a paginated list of pipeline templates."
}

func (c *pipelineTemplateListCommand) Usage() string {
	return "phobos [global options] pipeline-template list [options]"
}

func (c *pipelineTemplateListCommand) Description() string {
	return `
  The pipeline-template list command prints information about (likely
  multiple) pipeline templates. Supports pagination, filtering and
  sorting the output.
`
}

func (c *pipelineTemplateListCommand) Example() string {
	return `
phobos pipeline-template list \
  --limit 5 \
  --project-name my-project \
  --json
`
}

func (c *pipelineTemplateListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"cursor",
		"The cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"limit",
		"Maximum number of result elements to return. Defaults to 100.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.StringVar(
		&c.sortOrder,
		"sort-order",
		"ASC",
		"Sort in this direction, ASC or DESC.",
	)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"Name of parent organization for the new pipeline.  (This flag is required.)",
	)
	f.StringVar(
		&c.projectName,
		"project-name",
		"",
		"The project to limit search.  (This flag is required.)",
	)
	f.BoolFunc(
		"versioned",
		"If supplied, filter by versioned or non-versioned pipeline templates.",
		func(s string) error {
			val, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}

			c.versioned = &val
			return nil
		},
	)
	f.BoolFunc(
		"latest",
		"If supplied, filter by latest pipeline templates.",
		func(s string) error {
			val, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}

			c.latest = &val
			return nil
		},
	)
	f.StringVar(
		&c.search,
		"search",
		"",
		"Filter to only pipeline templates with this name prefix.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
