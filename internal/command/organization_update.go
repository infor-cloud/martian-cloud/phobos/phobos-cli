package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// organizationUpdateCommand is the structure for organization update command.
type organizationUpdateCommand struct {
	*BaseCommand

	description string
	version     string
	toJSON      bool
}

var _ Command = (*organizationUpdateCommand)(nil)

func (c *organizationUpdateCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewOrganizationUpdateCommandFactory returns an organizationUpdateCommand struct.
func NewOrganizationUpdateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &organizationUpdateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *organizationUpdateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("organization update"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.UpdateOrganizationRequest{
		Id:          c.arguments[0],
		Description: &c.description,
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("organization update input: %#v", input))

	updatedOrg, err := c.client.OrganizationsClient.UpdateOrganization(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update organization"))
		return 1
	}

	return outputOrganization(c.UI, c.toJSON, updatedOrg)
}

func (c *organizationUpdateCommand) Synopsis() string {
	return "Update an organization."
}

func (c *organizationUpdateCommand) Usage() string {
	return "phobos [global options] organization update [options] <id>"
}

func (c *organizationUpdateCommand) Description() string {
	return `
  The organization update command update an organization with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.
`
}

func (c *organizationUpdateCommand) Example() string {
	return `
phobos organization update \
  --description "updated description" \
  prn:organization:example-org
`
}

func (c *organizationUpdateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.description,
		"description",
		"",
		"Description for the new organization.",
	)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be updated. "+
			"In most cases, this is not required.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
