package command

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/adrg/frontmatter"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/uploader"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/internal-shared/schema"
)

type pluginDocFileMetadata struct {
	Title       string `yaml:"page_title"`
	Subcategory string `yaml:"subcategory"`
}

// pluginVersionMetadataType is the structure for the plugin version metadata.
type pluginVersionMetadataType struct {
	Version string `json:"version"`
}

// artifactType is the structure for the plugin version artifacts.
type artifactType struct {
	Name            string `json:"name"`
	Path            string `json:"path"`
	OperatingSystem string `json:"goos"`
	Architecture    string `json:"goarch"`
	Type            string `json:"type"`
}

// pluginUploadVersionCommand is the structure for plugin upload-version command.
type pluginUploadVersionCommand struct {
	*BaseCommand

	// sg is the step group to display the progress of the upload process.
	sg        terminal.StepGroup
	directory string
}

var _ Command = (*pluginUploadVersionCommand)(nil)

func (c *pluginUploadVersionCommand) validate() error {
	const message = "plugin-id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginUploadVersionCommandFactory returns a pluginUploadVersionCommand struct.
func NewPluginUploadVersionCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginUploadVersionCommand{
			BaseCommand: baseCommand,
			sg:          baseCommand.UI.StepGroup(),
		}, nil
	}
}

func (c *pluginUploadVersionCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin upload-version"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	if c.directory == "" {
		// Default to the current directory
		c.directory = "."
	}

	// Check if the directory exists
	if err := c.checkDirectory(); err != nil {
		c.UI.Output(output.FormatError(err, "failed to check directory"))
		return 1
	}

	pluginSchema, err := c.readPluginSchema()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read plugin manifest"))
		return 1
	}

	versionMetadata, err := c.readPluginVersionMetadata()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read plugin version metadata"))
		return 1
	}

	artifacts, err := c.readPluginVersionArtifacts()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read plugin version artifacts"))
		return 1
	}

	step := c.sg.Add("Create plugin version %q", versionMetadata.Version)

	versionToCreate := &pb.CreatePluginVersionRequest{
		PluginId:        c.arguments[0],
		SemanticVersion: versionMetadata.Version,
		Protocols:       pluginSchema.ProtocolVersions,
	}

	c.Logger.Debug(fmt.Sprintf("create plugin version input: %#v", versionToCreate))

	// Create the plugin version
	createdVersion, err := c.client.PluginRegistryClient.CreatePluginVersion(c.Context, versionToCreate)
	if err != nil {
		step.Abort() // Error occurred, abort the step.
		c.UI.Output(output.FormatError(err, "failed to create plugin version"))
		return 1
	}

	step.Update("Created plugin version %q", createdVersion.Metadata.Id)
	step.Done()

	if err = c.uploadReadme(createdVersion.Metadata.Id); err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload README"))
		return 1
	}

	if err = c.uploadSchema(createdVersion.Metadata.Id); err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload schema"))
		return 1
	}

	if err = c.uploadDocs(createdVersion.Metadata.Id); err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload plugin docs"))
		return 1
	}

	var checksums map[string]string
	for _, artifact := range artifacts {
		if artifact.Type == "Checksum" {
			checksums, err = c.uploadChecksums(createdVersion.Metadata.Id, artifact.Path)
			if err != nil {
				c.UI.Output(output.FormatError(err, "failed to upload checksums"))
				return 1
			}
		}
	}

	for _, artifact := range artifacts {
		if artifact.Type == "Archive" {
			artifactCopy := artifact
			if err := c.uploadPlatformArchive(createdVersion.Metadata.Id, &artifactCopy, checksums); err != nil {
				c.UI.Output(output.FormatError(err, "failed to upload platform archive"))
				return 1
			}
		}
	}

	c.UI.Output("\nPlugin version uploaded successfully!", terminal.WithSuccessStyle())
	return 0
}

func (*pluginUploadVersionCommand) Synopsis() string {
	return "Upload a new version of a plugin to the plugin registry."
}

func (c *pluginUploadVersionCommand) Usage() string {
	return "phobos [global options] plugin upload-version <plugin-id>"
}

func (c *pluginUploadVersionCommand) Description() string {
	return `
  The plugin upload-version command uploads a new version
  of a plugin to the plugin registry.
`
}

func (c *pluginUploadVersionCommand) Example() string {
	return `
phobos plugin upload-version \
  --directory my-directory \
  prn:plugin:my-org/my-plugin
`
}

func (c *pluginUploadVersionCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"directory",
		"Directory containing the plugin to upload. Defaults to the current directory.",
		func(s string) error {
			c.directory = filepath.Clean(s)
			return nil
		},
	)

	return f
}

// checkDirectory checks if the directory exists and is a directory.
func (c *pluginUploadVersionCommand) checkDirectory() error {
	dirStat, err := os.Stat(c.directory)
	if err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("directory does not exist: %s", c.directory)
		}
		return fmt.Errorf("failed to stat directory %s: %s", c.directory, err)
	}

	if !dirStat.IsDir() {
		return fmt.Errorf("path is not a directory: %s", c.directory)
	}

	return nil
}

// readPluginSchema reads the plugin schema json file
func (c *pluginUploadVersionCommand) readPluginSchema() (pluginSchema *schema.PluginSchema, err error) {
	step := c.sg.Add("Read schema.json")
	defer c.finalizeUIStep(step, err)

	// Load the plugin.json file to get the protocol versions
	data, err := os.ReadFile(filepath.Join(c.directory, "schema.json")) // nosemgrep: gosec.G304-1
	if err != nil {
		return nil, fmt.Errorf("failed to find schema.json file: %w", err)
	}

	if err = json.Unmarshal(data, &pluginSchema); err != nil {
		return nil, fmt.Errorf("failed to unmarshal phobos-registry-manifest.json file: %w", err)
	}

	return pluginSchema, nil
}

// readPluginVersionMetadata reads the metadata.json file in the dist directory.
func (c *pluginUploadVersionCommand) readPluginVersionMetadata() (pluginVersionMetadata *pluginVersionMetadataType, err error) {
	step := c.sg.Add("Read metadata.json")
	defer c.finalizeUIStep(step, err)

	// Load version string from the metadata.json file in the dist directory
	data, err := os.ReadFile(filepath.Join(c.directory, "dist", "metadata.json")) // nosemgrep: gosec.G304-1
	if err != nil {
		return nil, fmt.Errorf("failed to find metadata.json file: %w", err)
	}

	if err = json.Unmarshal(data, &pluginVersionMetadata); err != nil {
		return nil, fmt.Errorf("failed to unmarshal metadata.json file: %w", err)
	}

	return pluginVersionMetadata, nil
}

// readPluginVersionArtifacts reads the artifacts.json file in the dist directory.
func (c *pluginUploadVersionCommand) readPluginVersionArtifacts() (artifacts []artifactType, err error) {
	step := c.sg.Add("Read artifacts.json")
	defer c.finalizeUIStep(step, err)

	// Load the artifacts.json file to get the list of files/platforms that will be uploaded
	data, err := os.ReadFile(filepath.Join(c.directory, "dist", "artifacts.json")) // nosemgrep: gosec.G304-1
	if err != nil {
		return nil, fmt.Errorf("failed to find artifacts.json file: %w", err)
	}

	if err = json.Unmarshal(data, &artifacts); err != nil {
		return nil, fmt.Errorf("failed to unmarshal artifacts.json file: %w", err)
	}

	return artifacts, nil
}

func (c *pluginUploadVersionCommand) uploadDocs(pluginVersionID string) (err error) {
	docsDir := filepath.Join(c.directory, "docs")
	if err := filepath.WalkDir(docsDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Check if this is not a directory and if the file end with a markdown extension
		if !d.IsDir() && !strings.HasPrefix(d.Name(), ".") && filepath.Ext(d.Name()) == ".md" {
			step := c.sg.Add("Parsing doc file %q", path)
			defer step.Abort()

			// Open file
			file, err := os.Open(path)
			if err != nil {
				return fmt.Errorf("failed to open file %s: %w", path, err)
			}

			// Parse front matter
			var fileMetadata pluginDocFileMetadata
			remaining, err := frontmatter.Parse(file, &fileMetadata)
			if err != nil {
				return fmt.Errorf("failed to parse plugin doc markdown front matter for file %s: %w", path, err)
			}

			var category string

			dir := filepath.Dir(path)
			if dir == docsDir {
				category = "OVERVIEW"
			} else if dir == filepath.Join(docsDir, "actions") {
				category = "ACTION"
			} else if dir == filepath.Join(docsDir, "guides") {
				category = "GUIDE"
			} else {
				return fmt.Errorf("unknown doc category for file %s", path)
			}

			rawFilename := d.Name()
			// Remove file extension from filename
			filename := rawFilename[:strings.Index(rawFilename, ".")]

			step.Update("Uploading doc file %q", path)

			// Uploading
			err = uploader.NewPluginVersionUploader(c.UI, c.Logger, c.client).UploadDocFile(
				c.Context,
				pluginVersionID,
				category,
				fileMetadata.Subcategory,
				fileMetadata.Title,
				filename,
				bytes.NewReader(remaining),
			)
			if err != nil {
				return fmt.Errorf("failed to upload doc file %s: %w", path, err)
			}

			step.Update("Uploaded doc file %q", path)
			step.Done()
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

// uploadReadme uploads the README file to the plugin registry.
func (c *pluginUploadVersionCommand) uploadReadme(pluginVersionID string) (err error) {
	step := c.sg.Add("Upload README file")
	defer c.finalizeUIStep(step, err)

	matches, err := filepath.Glob(filepath.Join(c.directory, "README*"))
	if err != nil {
		return fmt.Errorf("error occurred while checking for README: %w", err)
	}

	if len(matches) == 0 {
		step.Update("README file upload not needed")
		return nil
	}

	step.Update("Upload README file %q", matches[0])

	reader, err := os.Open(matches[0]) // nosemgrep: gosec.G304-1
	if err != nil {
		return fmt.Errorf("failed to read README file: %w", err)
	}
	defer reader.Close()

	// upload readme file
	err = uploader.NewPluginVersionUploader(c.UI, c.Logger, c.client).UploadReadme(c.Context, pluginVersionID, reader)
	if err != nil {
		return fmt.Errorf("failed to upload README: %w", err)
	}

	return nil
}

func (c *pluginUploadVersionCommand) uploadSchema(pluginVersionID string) (err error) {
	step := c.sg.Add("Upload schema file")
	defer c.finalizeUIStep(step, err)

	matches, err := filepath.Glob(filepath.Join(c.directory, "schema.json"))
	if err != nil {
		return fmt.Errorf("error occurred while checking for schema.json: %w", err)
	}

	if len(matches) == 0 {
		return errors.New("failed to find schema.json file")
	}

	step.Update("Upload schema.json file %q", matches[0])

	reader, err := os.Open(matches[0]) // nosemgrep: gosec.G304-1
	if err != nil {
		return fmt.Errorf("failed to read schema.json file: %w", err)
	}
	defer reader.Close()

	// upload readme file
	err = uploader.NewPluginVersionUploader(c.UI, c.Logger, c.client).UploadSchema(c.Context, pluginVersionID, reader)
	if err != nil {
		return fmt.Errorf("failed to upload schema: %w", err)
	}

	return nil
}

// uploadChecksums uploads the checksums to the plugin registry.
func (c *pluginUploadVersionCommand) uploadChecksums(pluginVersionID, artifactPath string) (checksumMap map[string]string, err error) {
	step := c.sg.Add("Upload checksums file %q", artifactPath)
	defer c.finalizeUIStep(step, err)

	checksumMap = map[string]string{}

	data, err := os.ReadFile(filepath.Join(c.directory, artifactPath)) // nosemgrep: gosec.G304-1
	if err != nil {
		return nil, fmt.Errorf("failed to find %s file: %w", artifactPath, err)
	}

	checksums := strings.Split(string(data), "\n")
	for _, checksum := range checksums {
		parsedChecksum := strings.Split(checksum, "  ")
		if len(parsedChecksum) == 2 {
			checksumMap[parsedChecksum[1]] = parsedChecksum[0]
		}
	}

	reader, err := os.Open(filepath.Join(c.directory, artifactPath)) // nosemgrep: gosec.G304-1
	if err != nil {
		return nil, fmt.Errorf("failed to read %s file: %w", artifactPath, err)
	}
	defer reader.Close()

	// upload sums file
	err = uploader.NewPluginVersionUploader(c.UI, c.Logger, c.client).UploadShaSums(c.Context, pluginVersionID, reader)
	if err != nil {
		return nil, fmt.Errorf("failed to upload checksums: %w", err)
	}

	return checksumMap, nil
}

// uploadPlatformArchive uploads the platform archive to the plugin registry.
func (c *pluginUploadVersionCommand) uploadPlatformArchive(pluginVersionID string, artifact *artifactType, checksums map[string]string) (err error) {
	step := c.sg.Add(fmt.Sprintf("Upload platform archive %s_%s", artifact.OperatingSystem, artifact.Architecture))
	defer c.finalizeUIStep(step, err)

	checksum, ok := checksums[artifact.Name]
	if !ok {
		return fmt.Errorf("failed to find checksum for file %s", artifact.Path)
	}

	platform, err := c.client.PluginRegistryClient.CreatePluginPlatform(c.Context,
		&pb.CreatePluginPlatformRequest{
			PluginVersionId: pluginVersionID,
			OperatingSystem: artifact.OperatingSystem,
			Architecture:    artifact.Architecture,
			ShaSum:          checksum,
			Filename:        artifact.Name,
		},
	)
	if err != nil {
		return fmt.Errorf("failed to create platform: %v", err)
	}

	// upload binary
	reader, err := os.Open(filepath.Join(c.directory, artifact.Path)) // nosemgrep: gosec.G304-1
	if err != nil {
		return fmt.Errorf("failed to read %s file: %v", artifact.Path, err)
	}
	defer reader.Close()

	// upload binary file
	err = uploader.NewPluginVersionUploader(c.UI, c.Logger, c.client).UploadPlatformBinary(c.Context, platform.Metadata.Id, reader)
	if err != nil {
		return fmt.Errorf("failed to upload platform: %v", err)
	}

	return nil
}

// finalizeUIStep updates the status of the step based on the error.
func (*pluginUploadVersionCommand) finalizeUIStep(step terminal.Step, err error) {
	if err != nil {
		step.Abort()
		return
	}

	step.Done()
}
