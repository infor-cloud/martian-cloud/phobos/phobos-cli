package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseAddParticipantCommand is the structure for the release add-participant command.
type releaseAddParticipantCommand struct {
	*BaseCommand

	userID *string
	teamID *string
	toJSON bool
}

var _ Command = (*releaseAddParticipantCommand)(nil)

func (c *releaseAddParticipantCommand) validate() error {
	const message = "release-id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseAddParticipantCommandFactory returns a releaseAddParticipantCommand struct.
func NewReleaseAddParticipantCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseAddParticipantCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseAddParticipantCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release add-participant"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.AddParticipantToReleaseRequest{
		ReleaseId: c.arguments[0],
		UserId:    c.userID,
		TeamId:    c.teamID,
	}

	c.Logger.Debug(fmt.Sprintf("release add-participant input: %#v", input))

	release, err := c.client.ReleasesClient.AddParticipantToRelease(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to add participant to release"))
		return 1
	}

	return outputRelease(c.UI, c.toJSON, release)
}

func (*releaseAddParticipantCommand) Synopsis() string {
	return "Add a participant to a release."
}

func (c *releaseAddParticipantCommand) Usage() string {
	return "phobos [global options] release add-participant [options] <release-id>"
}

func (c *releaseAddParticipantCommand) Description() string {
	return `
  The release add-participant command adds one or more participants
  to a release. The -json flag optionally allows showing the final
  output as formatted JSON.
`
}

func (c *releaseAddParticipantCommand) Example() string {
	return `
phobos release add-participant \
  --user-id prn:user:random.person \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseAddParticipantCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	f.Func(
		"user-id",
		"ID of the user to add as a participant to the release.",
		func(s string) error {
			c.userID = &s
			return nil
		},
	)
	f.Func(
		"team-id",
		"ID of the team to add as a participant to the release.",
		func(s string) error {
			c.teamID = &s
			return nil
		},
	)
	return f
}
