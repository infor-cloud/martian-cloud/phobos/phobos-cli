package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/plugins"
	sdk "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk"
)

// pluginLaunchCommand is the structure for plugin launch command.
type pluginLaunchCommand struct {
	*BaseCommand
}

var _ Command = (*pluginLaunchCommand)(nil)

func (c *pluginLaunchCommand) validate() error {
	const message = "plugin-name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginLaunchCommandFactory returns a pluginLaunchCommand struct.
func NewPluginLaunchCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginLaunchCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginLaunchCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithCommandName("plugin launch"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	pluginName := c.arguments[0]

	meta, ok := plugins.ListPlugins()[pluginName]
	if !ok {
		c.UI.Output(output.FormatError(nil, "plugin %s not found", pluginName))
		return 1
	}

	c.Logger.Debug(fmt.Sprintf("Launching provider plugin %s", pluginName))

	// Launch the plugin
	sdk.Main(
		sdk.WithPluginDescription(meta.Description),
		sdk.WithPipelinePlugin(meta.Plugin),
	)

	return 0
}

func (c *pluginLaunchCommand) Synopsis() string {
	return "Launch a built-in plugin."
}

func (c *pluginLaunchCommand) Usage() string {
	return "phobos [global options] plugin launch <plugin-name>"
}

func (c *pluginLaunchCommand) Description() string {
	return `
  The plugin launch command launches built-in plugins.
  Please note, these plugins are not meant to be invoked
  directly by the user but only by programs which consume
  the plugin.
`
}

func (c *pluginLaunchCommand) Example() string {
	return ""
}

func (c *pluginLaunchCommand) Flags() *flag.FlagSet {
	return nil
}
