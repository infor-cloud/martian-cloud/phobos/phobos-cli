package command

import (
	"flag"
	"net/url"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginInstallCommand is the structure for the plugin install command
type pluginInstallCommand struct {
	*BaseCommand

	version *string
	token   *string
}

var _ Command = (*pluginInstallCommand)(nil)

func (c *pluginInstallCommand) validate() error {
	const message = "plugin-source is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewPluginInstallCommandFactory returns a pluginInstallCommand struct.
func NewPluginInstallCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginInstallCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginInstallCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("plugin install"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	pluginLogger := c.Logger.Named("phobos-plugin")
	pluginLogger.SetLevel(hclog.LevelFromString("ERROR"))

	settings, err := c.getCurrentSettings()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get current settings"))
		return 1
	}

	rawSource := c.arguments[0]

	source, err := plugin.ParseSource(rawSource, &settings.CurrentProfile.Endpoint)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse plugin source %q", rawSource))
		return 1
	}

	pluginInstaller, err := plugin.NewInstaller(pluginLogger, settings.CurrentProfile.Endpoint)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create plugin installer"))
		return 1
	}

	// Parse the default endpoint for accurate hostname comparison.
	defaultEndpointURL, err := url.Parse(settings.CurrentProfile.Endpoint)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse default endpoint"))
		return 1
	}

	token := c.token
	if token == nil && source.Hostname.String() == defaultEndpointURL.Hostname() {
		// Use the token getter when connecting to default endpoint hostname.
		tokenGetter, tErr := settings.CurrentProfile.NewTokenGetter(c.Context)
		if tErr != nil {
			c.UI.Output(output.FormatError(tErr, "failed to get token to download plugin %s", source.Name))
			return 1
		}

		// Get token to download the plugin.
		t, gErr := tokenGetter.Token(c.Context)
		if gErr != nil {
			c.UI.Output(output.FormatError(gErr, "failed to get token to download plugin %s", source.Name))
			return 1
		}

		token = &t
	}

	// Ensure the latest version of the plugin meets the requirements.
	version, err := pluginInstaller.EnsureLatestVersion(
		c.Context,
		source.String(),
		plugin.WithConstraints(c.version),
		plugin.WithToken(token),
	)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to ensure a version of plugin %q", source.Name))
		return 1
	}

	c.UI.Output("Installing %s@%s...", source.Name, version)

	// Install the plugin if it's not already installed.
	if err := pluginInstaller.InstallPlugin(
		c.Context,
		source.String(),
		version,
		plugin.WithToken(token),
	); err != nil {
		c.UI.Output(output.FormatError(err, "failed to install plugin %s@%s", source.Name, version))
		return 1
	}

	c.UI.Output("Plugin installed successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *pluginInstallCommand) Synopsis() string {
	return "Install a plugin from a remote registry."
}

func (c *pluginInstallCommand) Usage() string {
	return "phobos [global options] plugin install <plugin-source>"
}

func (c *pluginInstallCommand) Description() string {
	return `
  The plugin install command installs a plugin from a remote registry.

  Plugin will be installed to the user's home directory:

   '~/.phobos.d/plugins/'

  The plugin source format must be:

   '<registry-hostname>/<organization-name>/<plugin-name>'

  The registry hostname may be omitted when using the Phobos instance
  associated with the current profile. An authentication token
  (if exists) from the current profile will be automatically
  supplied in the request.

  Version constraints can optionally be used to limit the Plugin's
  versions. The latest version of the plugin (matching the
  constraints, if supplied) will be used.
`
}

func (c *pluginInstallCommand) Example() string {
	return `
phobos plugin install \
  my-org/my-plugin
`
}

func (c *pluginInstallCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"version",
		"Version constraints to adhere to. Installs the latest version of the plugin (matching the constraints, if supplied).",
		func(s string) error {
			c.version = &s
			return nil
		},
	)
	f.Func(
		"token",
		"An authentication token to use when downloading plugin from the remote registry. "+
			"If nothing is supplied, the current profile's token is used when connecting to its endpoint.",
		func(s string) error {
			c.token = &s
			return nil
		},
	)

	return f
}
