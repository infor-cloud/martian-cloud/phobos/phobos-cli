package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginDeletePlatformCommand is the structure for plugin delete-platform command.
type pluginDeletePlatformCommand struct {
	*BaseCommand

	version *string
}

var _ Command = (*pluginDeletePlatformCommand)(nil)

func (c *pluginDeletePlatformCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int, validation.When(c.version != nil)),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginDeletePlatformCommandFactory returns a pluginDeletePlatformCommand struct.
func NewPluginDeletePlatformCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginDeletePlatformCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginDeletePlatformCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin delete-platform"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.DeletePluginPlatformRequest{
		Id:      c.arguments[0],
		Version: c.version,
	}

	c.Logger.Debug(fmt.Sprintf("plugin delete-platform input: %#v", input))

	if _, err := c.client.PluginRegistryClient.DeletePluginPlatform(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete plugin platform"))
		return 1
	}

	c.UI.Output("Plugin platform deleted successfully.", terminal.WithSuccessStyle())
	return 0
}

func (*pluginDeletePlatformCommand) Synopsis() string {
	return "Delete a plugin platform from the plugin registry."
}

func (c *pluginDeletePlatformCommand) Usage() string {
	return "phobos [global options] plugin delete-platform [options] <id>"
}

func (c *pluginDeletePlatformCommand) Description() string {
	return `
  The plugin delete platform command deletes a plugin platform
  from the plugin registry.
`
}

func (c *pluginDeletePlatformCommand) Example() string {
	return `
phobos plugin delete-platform \
  prn:plugin_platform:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9/windows/amd64
`
}

func (c *pluginDeletePlatformCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"platform",
		"Metadata platform of the resource to be deleted. In most cases, this is not required.",
		func(s string) error {
			c.version = &s
			return nil
		},
	)

	return f
}
