package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginListPlatformsCommand is the structure for the plugin list-platforms command.
type pluginListPlatformsCommand struct {
	*BaseCommand

	pluginID        *string
	pluginVersionID *string
	operatingSystem *string
	architecture    *string
	cursor          *string
	sortOrder       *string
	limit           *int32
	binaryUploaded  *bool
	toJSON          bool
}

var _ Command = (*pluginListPlatformsCommand)(nil)

func (c *pluginListPlatformsCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.limit, validation.Min(0), validation.Max(100)),
		validation.Field(&c.sortOrder, validation.In("ASC", "DESC")),
	)
}

// NewPluginListPlatformsCommandFactory returns a pluginListPlatformsCommand struct.
func NewPluginListPlatformsCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginListPlatformsCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginListPlatformsCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("plugin list-platforms"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	sortOrder := pb.PluginPlatformSortableField_PluginPlatform_UPDATED_AT_ASC.Enum()
	if c.sortOrder != nil && strings.ToLower(*c.sortOrder) == "desc" {
		sortOrder = pb.PluginPlatformSortableField_PluginPlatform_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetPluginPlatformsRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		PluginId:        c.pluginID,
		PluginVersionId: c.pluginVersionID,
		OperatingSystem: c.operatingSystem,
		Architecture:    c.architecture,
		BinaryUploaded:  c.binaryUploaded,
	}

	c.Logger.Debug(fmt.Sprintf("plugin list-platforms input: %#v", input))

	result, err := c.client.PluginRegistryClient.GetPluginPlatforms(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get plugin platforms"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "os", "architecture", "version")

		for _, p := range result.PluginPlatforms {
			t.Rich([]string{
				p.Metadata.Id,
				p.OperatingSystem,
				p.Architecture,
				p.PluginVersionId,
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (*pluginListPlatformsCommand) Synopsis() string {
	return "Lists the available plugin platforms from the registry."
}

func (c *pluginListPlatformsCommand) Usage() string {
	return "phobos [global options] plugin list-platforms [options]"
}

func (c *pluginListPlatformsCommand) Description() string {
	return `
  The plugin list-platforms command lists the available platforms
  for a plugin version. Advanced filtering can be done using the
  provided flags.
`
}

func (c *pluginListPlatformsCommand) Example() string {
	return `
phobos plugin list-platforms \
  --limit 10 \
  --sort DESC \
  --json \
  --version-id prn:plugin:tomcat/gitlab/1.0.0
`
}

func (c *pluginListPlatformsCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"limit",
		"Limit the number of results.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.Func(
		"sort",
		"Sort in this direction, ASC or DESC.",
		func(s string) error {
			c.sortOrder = &s
			return nil
		},
	)
	f.Func(
		"cursor",
		"Cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"version-id",
		"ID of the plugin version. (Required if plugin-id is provided).",
		func(s string) error {
			c.pluginVersionID = &s
			return nil
		},
	)
	f.Func(
		"plugin-id",
		"ID of the plugin. (Required if version-id is provided).",
		func(s string) error {
			c.pluginID = &s
			return nil
		},
	)
	f.Func(
		"os",
		"Operating system of the platform.",
		func(s string) error {
			c.operatingSystem = &s
			return nil
		},
	)
	f.Func(
		"arch",
		"Architecture of the platform.",
		func(s string) error {
			c.architecture = &s
			return nil
		},
	)
	f.BoolFunc(
		"has-binary",
		"Filter platforms with binary uploaded.",
		func(s string) error {
			b, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}
			c.binaryUploaded = &b
			return nil
		},
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
