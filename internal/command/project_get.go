package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// projectGetCommand is the structure for project get command.
type projectGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*projectGetCommand)(nil)

func (c *projectGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewProjectGetCommandFactory returns a projectGetCommand struct.
func NewProjectGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &projectGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *projectGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("project get"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.GetProjectByIdRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("project get input: %#v", input))

	proj, err := c.client.ProjectsClient.GetProjectByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get project"))
		return 1
	}

	return outputProject(c.UI, c.toJSON, proj)
}

func (c *projectGetCommand) Synopsis() string {
	return "Retrieve a project."
}

func (c *projectGetCommand) Usage() string {
	return "phobos [global options] project get <id>"
}

func (c *projectGetCommand) Description() string {
	return `
  The project get command retrieves a project by id.
`
}

func (c *projectGetCommand) Example() string {
	return `
phobos project get \
  prn:project:my-org/example-proj
`
}

func (c *projectGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputProject is the output for most project operations.
func outputProject(ui terminal.UI, toJSON bool, proj *pb.Project) int {
	if toJSON {
		buf, err := objectToJSON(proj)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "description", "organization")
		t.Rich([]string{
			proj.GetMetadata().Id,
			proj.Name,
			proj.Description,
			proj.OrgId,
		}, nil)

		ui.Table(t)
	}

	return 0
}
