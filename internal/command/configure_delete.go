package command

import (
	"flag"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/settings"
)

// configureDeleteCommand is the structure for the configure delete command.
type configureDeleteCommand struct {
	*BaseCommand
}

var _ Command = (*configureDeleteCommand)(nil)

func (c *configureDeleteCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // 1 and only 1 argument.
	)
}

// NewConfigureDeleteCommandFactory returns a configureDeleteCommand struct.
func NewConfigureDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &configureDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *configureDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithCommandName("configure delete"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	profileName := c.arguments[0]

	gotSettings, err := settings.ReadSettings(nil)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read settings"))
		return 1
	}

	// Helpful errors.

	if len(gotSettings.Profiles) == 0 {
		c.UI.Output(output.FormatError(nil, "no profiles currently exist. Please use the 'phobos configure' command to create one"))
		return 1
	}

	if _, ok := gotSettings.Profiles[profileName]; !ok {
		c.UI.Output(output.FormatError(nil, "profile %s not found", profileName))
		return 1
	}

	delete(gotSettings.Profiles, profileName)

	if err := gotSettings.WriteSettingsFile(nil); err != nil {
		c.UI.Output(output.FormatError(err, "failed to write settings file"))
		return 1
	}

	c.UI.Output("Profile %s and associated credentials deleted successfully!", profileName, terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *configureDeleteCommand) Synopsis() string {
	return "Remove a profile."
}

func (c *configureDeleteCommand) Usage() string {
	return "phobos configure delete <name>"
}

func (c *configureDeleteCommand) Description() string {
	return `
   The configure delete command removes a profile and its
   credentials with the given name.
`
}

func (c *configureDeleteCommand) Example() string {
	return `
phobos configure delete prod-example
`
}

func (c *configureDeleteCommand) Flags() *flag.FlagSet {
	return nil
}
