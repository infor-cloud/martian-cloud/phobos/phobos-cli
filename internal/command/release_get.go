package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseGetCommand is the structure for the release get command.
type releaseGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*releaseGetCommand)(nil)

func (c *releaseGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseGetCommandFactory returns a releaseGetCommand struct.
func NewReleaseGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release get"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.GetReleaseByIdRequest{Id: c.arguments[0]}

	c.Logger.Debug(fmt.Sprintf("release get input: %#v", input))

	release, err := c.client.ReleasesClient.GetReleaseByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get release"))
		return 1
	}

	return outputRelease(c.UI, c.toJSON, release)
}

func (*releaseGetCommand) Synopsis() string {
	return "Get a release."
}

func (c *releaseGetCommand) Usage() string {
	return "phobos [global options] release get [options] <id>"
}

func (c *releaseGetCommand) Description() string {
	return `
  The release get command retrieves a release for
  the specified project. The -json flag optionally
  allows showing the final output as formatted JSON.
`
}

func (c *releaseGetCommand) Example() string {
	return `
phobos release get \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	return f
}

func outputRelease(ui terminal.UI, toJSON bool, release *pb.Release) int {
	path := resourcePathFromPRN(release.Metadata.Prn)
	projectPath := path[:strings.LastIndex(path, "/")]

	dueDate := "none"
	if release.DueDate != nil {
		// Make due date more human friendly.
		dueDate = release.DueDate.AsTime().Local().Format(time.RFC1123)
	}

	if toJSON {
		buf, err := objectToJSON(release)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "semantic version", "project", "due date", "pre release")
		t.Rich([]string{
			release.Metadata.Id,
			release.SemanticVersion,
			projectPath,
			dueDate,
			strconv.FormatBool(release.PreRelease),
		}, nil)

		ui.Table(t)
	}

	return 0
}
