package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginDeleteVersionCommand is the structure for plugin delete-version command.
type pluginDeleteVersionCommand struct {
	*BaseCommand

	version *string
}

var _ Command = (*pluginDeleteVersionCommand)(nil)

func (c *pluginDeleteVersionCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int, validation.When(c.version != nil)),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginDeleteVersionCommandFactory returns a pluginDeleteVersionCommand struct.
func NewPluginDeleteVersionCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginDeleteVersionCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginDeleteVersionCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin delete-version"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.DeletePluginVersionRequest{
		Id:      c.arguments[0],
		Version: c.version,
	}

	c.Logger.Debug(fmt.Sprintf("plugin delete-version input: %#v", input))

	if _, err := c.client.PluginRegistryClient.DeletePluginVersion(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete plugin version"))
		return 1
	}

	c.UI.Output("Plugin version deleted successfully.", terminal.WithSuccessStyle())
	return 0
}

func (*pluginDeleteVersionCommand) Synopsis() string {
	return "Delete a plugin version from the plugin registry."
}

func (c *pluginDeleteVersionCommand) Usage() string {
	return "phobos [global options] plugin delete-version [options] <id>"
}

func (c *pluginDeleteVersionCommand) Description() string {
	return `
  The plugin delete version command deletes a plugin version
  from the plugin registry.
`
}

func (c *pluginDeleteVersionCommand) Example() string {
	return `
phobos plugin delete-version \
  prn:plugin_version:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9
`
}

func (c *pluginDeleteVersionCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"version",
		"Metadata version of the resource to be deleted. In most cases, this is not required.",
		func(s string) error {
			c.version = &s
			return nil
		},
	)

	return f
}
