package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginGetVersionCommand is the structure for plugin get-version command.
type pluginGetVersionCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*pluginGetVersionCommand)(nil)

func (c *pluginGetVersionCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewPluginGetVersionCommandFactory returns a pluginGetVersionCommand struct.
func NewPluginGetVersionCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginGetVersionCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginGetVersionCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin get-version"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.GetPluginVersionByIdRequest{Id: c.arguments[0]}

	c.Logger.Debug(fmt.Sprintf("plugin get-version input: %#v", input))

	pluginVersion, err := c.client.PluginRegistryClient.GetPluginVersionByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get plugin version"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(pluginVersion)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "plugin id", "protocols", "latest")
		t.Rich([]string{
			pluginVersion.Metadata.Id,
			pluginVersion.PluginId,
			strings.Join(pluginVersion.Protocols, ","),
			strconv.FormatBool(pluginVersion.Latest),
		}, nil)
		c.UI.Table(t)
	}

	return 0
}

func (*pluginGetVersionCommand) Synopsis() string {
	return "Retrieve a plugin version from the plugin registry."
}

func (c *pluginGetVersionCommand) Usage() string {
	return "phobos [global options] plugin get-version [options] <id>"
}

func (c *pluginGetVersionCommand) Description() string {
	return `
  The plugin get version command retrieves a plugin version
  from the plugin registry. -json flag can be used to get
  the final output as JSON.
`
}

func (c *pluginGetVersionCommand) Example() string {
	return `
phobos plugin get-version \
  prn:plugin_version:my-org/my-plugin/0.0.0-SNAPSHOT-4ffb2d9
`
}

func (c *pluginGetVersionCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
