package command

import (
	"context"
	"flag"
	"fmt"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineTemplateGetCommand is the structure for pipeline-template get command.
type pipelineTemplateGetCommand struct {
	*BaseCommand

	toJSON bool
}

var _ Command = (*pipelineTemplateGetCommand)(nil)

func (c *pipelineTemplateGetCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewPipelineTemplateGetCommandFactory returns a pipelineTemplateGetCommand struct.
func NewPipelineTemplateGetCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineTemplateGetCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineTemplateGetCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline-template get"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.GetPipelineTemplateByIdRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("pipeline-template get input: %#v", input))

	p, err := c.client.PipelineTemplatesClient.GetPipelineTemplateByID(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get pipeline template"))
		return 1
	}

	return outputPipelineTemplate(c.Context, c.client, c.UI, c.toJSON, p)
}

func (c *pipelineTemplateGetCommand) Synopsis() string {
	return "Retrieve a pipeline template."
}

func (c *pipelineTemplateGetCommand) Usage() string {
	return "phobos [global options] pipeline-template get <id>"
}

func (c *pipelineTemplateGetCommand) Description() string {
	return `
  The pipeline-template get command retrieves a pipeline template by id.
`
}

func (c *pipelineTemplateGetCommand) Example() string {
	return `
phobos pipeline-template get \
  --json \
  prn:pipeline_template:my-org/my-project/MI3DKNBYGNQTCLJXMVSTKLJUGY4DSLLBMNRDKLJSHA4TOZRQGY3TKZBZMNPVAVA
`
}

func (c *pipelineTemplateGetCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}

// outputPipelineTemplate is the output for most pipeline template operations.
func outputPipelineTemplate(ctx context.Context, client *client.Client, ui terminal.UI, toJSON bool, p *pb.PipelineTemplate) int {

	// Must get name of the project.
	proj, err := client.ProjectsClient.GetProjectByID(ctx, &pb.GetProjectByIdRequest{
		Id: p.ProjId,
	})
	if err != nil {
		ui.Output(output.FormatError(err, "failed to get project"))
		return 1
	}

	if toJSON {
		buf, err := objectToJSON(p)
		if err != nil {
			ui.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		ui.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "project", "status", "versioned", "name", "semantic-version", "latest")
		t.Rich([]string{
			p.GetMetadata().Id,
			proj.Name,
			p.GetStatus(),
			strconv.FormatBool(p.GetVersioned()),
			p.GetName(),
			p.GetSemanticVersion(),
			strconv.FormatBool(p.GetLatest()),
		}, nil)

		ui.Table(t)
	}

	return 0
}
