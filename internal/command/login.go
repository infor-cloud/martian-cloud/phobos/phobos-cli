// Copyright (c) HashiCorp, Inc.
// SPDX-License-Identifier: MPL-2.0

// Portions of this file are from
// https://github.com/hashicorp/terraform/blob/6b290cf163f6816686bf0a5ae85ff4cb37b3beed/internal/command/login.go.

package command

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"math/big"
	"net"
	"net/http"
	"net/url"
	"runtime"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/hashicorp/go-uuid"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"github.com/pkg/browser"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/settings"
	"golang.org/x/oauth2"
	"google.golang.org/protobuf/types/known/emptypb"
)

const (
	// Port range for temporary web server.
	tempServerMinPort = 21000
	tempServerMaxPort = 21200

	// Header reading timeout for temporary web server.
	readHeaderTimeout = 30 * time.Second

	// originHeader is added to the request during the token exchange.
	originHeader = "http://localhost"

	// Path for "well-known" URL.
	wellKnownURLPath = "/.well-known/openid-configuration"
)

// customHeaderTransport is used to set custom header on the
// token exchange requests with the IDP.
type customHeaderTransport struct {
	rt http.RoundTripper
}

// RoundTrip adds a custom 'Origin' header to the request for PKCE flow.
func (t *customHeaderTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("Origin", originHeader)
	return t.rt.RoundTrip(req)
}

// loginCommand is the top-level structure for the login command.
type loginCommand struct {
	*BaseCommand
}

var _ Command = (*loginCommand)(nil)

func (c *loginCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Empty),
	)
}

// NewLoginCommandFactory returns a loginCommand struct.
func NewLoginCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &loginCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *loginCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithCommandName("sso login"),
		WithInputValidator(c.validate),
		WithClient(false), // No auth is required for this module.
	); code != 0 {
		return code
	}

	// Cannot delay reading settings past this point.
	currentSettings, err := c.getCurrentSettings()
	if err != nil {
		if err != settings.ErrNoSettings || c.DefaultHTTPEndpoint == "" {
			c.UI.Output(output.FormatError(err, "failed to read settings file"))
			return 1
		}

		// Build settings when they don't exist.
		profile := settings.Profile{
			Endpoint: c.DefaultHTTPEndpoint,
		}
		profiles := map[string]settings.Profile{"default": profile}
		currentSettings = &settings.Settings{Profiles: profiles, CurrentProfile: &profile}
	}

	// Use the 'well-known' endpoint to get the OAuth2 configuration.
	oauthCfg, err := c.fetchAuthConfig()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to fetch OAuth config"))
		return 1
	}

	// Build the request state UUID.
	requestState, err := buildRequestState()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to build request state"))
		return 1
	}

	// Build the proof key and challenge values.
	proofKey, proofKeyChallenge, err := buildProofKeyChallenge()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to build proof key challenge"))
		return 1
	}

	// Open the net.listener and build the callback URL.
	netListener, callbackURL, err := openNetListener()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create temporary web server"))
		return 1
	}

	// Launch the embedded web server.
	webServerChannel, server, err := c.launchWebServer(requestState, proofKey, netListener)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to launch temporary web server"))
		return 1
	}

	// Create the authCodeURL from the OAuth2 configuration.
	oauthCfg.RedirectURL = callbackURL
	authCodeURL := oauthCfg.AuthCodeURL(
		requestState,
		oauth2.SetAuthURLParam("code_challenge", proofKeyChallenge),
		oauth2.SetAuthURLParam("code_challenge_method", "S256"),
	)

	// Launch the web browser.
	err = c.launchBrowser(authCodeURL, currentSettings)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to launch web browser for OAuth redirect"))
		return 1
	}

	credsFilepath, err := settings.DefaultCredentialsFilepath()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get default settings file name"))
		return 1
	}
	c.UI.Output("Phobos has opened your default browser to complete the SSO login.")
	c.UI.Output("")
	c.UI.Output("If the login is successful, Phobos will store the authentication token in:")
	c.UI.Output(credsFilepath)

	// Capture the token. This also terminates the web server.
	token, err := c.captureToken(oauthCfg, proofKey, webServerChannel, server)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to capture token in temporary web server"))
		return 1
	}

	// Store the token.
	err = c.storeToken(token, currentSettings)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to store token in settings file"))
		return 1
	}

	c.UI.Output("\nPhobos SSO login was successful using the %s profile!\n", c.CurrentProfileName, terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *loginCommand) Synopsis() string {
	return "Log in to the OAuth2 provider and return an authentication token."
}

func (c *loginCommand) Usage() string {
	return "phobos [global options] sso login"
}

func (c *loginCommand) Description() string {
	return `
   The login command starts an embedded web server and opens
   a web browser page or tab pointed at said web server.
   That redirects to the OAuth2 provider's login page, where
   the user can sign in. If there is an SSO scheme active,
   that will sign in the user. The login command captures
   the authentication token for use in subsequent commands.
`
}

func (c *loginCommand) Example() string {
	return `
phobos sso login
`
}

func (c *loginCommand) Flags() *flag.FlagSet {
	return nil
}

// Fetch the '.well-known' URL of the IDP to get the auth URL.
func (c *loginCommand) fetchAuthConfig() (*oauth2.Config, error) {
	// Get the auth settings from the API.
	authSettings, err := c.client.AuthSettingsClient.GetAuthSettings(c.Context, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}

	if authSettings.OidcIssuerUrl == nil || authSettings.OidcClientId == nil {
		return nil, errors.New("no OIDC issuer URL or client ID present in the auth settings")
	}

	wellKnownURL, err := url.JoinPath(authSettings.GetOidcIssuerUrl(), wellKnownURLPath)
	if err != nil {
		return nil, err
	}

	c.Logger.Debug(fmt.Sprintf("will fetch well-known URL: %s", wellKnownURL))

	resp, err := http.Get(wellKnownURL) // nosemgrep: gosec.G107-1
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received status code %d from well-known URL", resp.StatusCode)
	}

	var data struct {
		AuthorizationEndpoint string   `json:"authorization_endpoint"`
		TokenEndpoint         string   `json:"token_endpoint"`
		ScopesSupported       []string `json:"scopes_supported"`
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	return &oauth2.Config{
		ClientID: authSettings.GetOidcClientId(),
		Endpoint: oauth2.Endpoint{
			AuthURL:  data.AuthorizationEndpoint,
			TokenURL: data.TokenEndpoint,
		},
		Scopes: data.ScopesSupported,
	}, nil
}

// buildRequestState generates the request state UUID.
func buildRequestState() (string, error) {
	result, err := uuid.GenerateUUID()
	if err != nil {
		return "", fmt.Errorf("failed to generate UUID, likely not enough pseudo-random entropy: %s", err)
	}
	return result, nil
}

// buildProofKeyChallenge generates the proof key and challenge values.
func buildProofKeyChallenge() (string, string, error) {
	firstUUID, err := uuid.GenerateUUID()
	if err != nil {
		return "", "", err
	}

	randomInt, err := rand.Int(rand.Reader, big.NewInt(999999999))
	if err != nil {
		return "", "", err
	}

	key := fmt.Sprintf("%s.%09d", firstUUID, randomInt)

	hasher := sha256.New()
	hasher.Write([]byte(key))
	challenge := base64.RawURLEncoding.EncodeToString(hasher.Sum(nil))

	return key, challenge, nil
}

// openNetListener builds a net.listener and callback URL.
func openNetListener() (net.Listener, string, error) {
	for port := tempServerMinPort; port < tempServerMaxPort; port++ {
		listener, err := net.Listen("tcp4", fmt.Sprintf("127.0.0.1:%d", port))
		if err == nil {
			// Lack of an error means we succeeded in opening the listener on the port.
			// No trailing slash.
			//
			// The callback URL must use "localhost", not "127.0.0.1".
			callbackURL := fmt.Sprintf("http://localhost:%d/login", port)
			return listener, callbackURL, nil
		}
	}

	// Getting here means no port was available.
	return nil, "", errors.New("no port could be opened for the temporary web server")
}

// launchWebServer launches the embedded web server and returns the termination channel.
func (c *loginCommand) launchWebServer(requestState, _ string,
	netListener net.Listener,
) (chan string, *http.Server, error) {
	c.Logger.Debug("callback server: creating server")
	codeChannel := make(chan string)
	httpServer := &http.Server{
		ReadHeaderTimeout: readHeaderTimeout,
		Handler: http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
			c.Logger.Debug("callback server: handler called")

			// Parse the form.
			err := req.ParseForm()
			if err != nil {
				c.Logger.Error(fmt.Sprintf("callback server: cannot parse form on callback request: %s", err))
				resp.WriteHeader(http.StatusBadRequest)
				return
			}

			// Check that the request state matches.
			gotState := req.Form.Get("state")
			c.Logger.Debug(fmt.Sprintf("callback server got state: %s", gotState))

			// Prevent timing attacks on state. Code 0 indicates a mismatch.
			if subtle.ConstantTimeCompare([]byte(requestState), []byte(gotState)) == 0 {
				c.Logger.Debug(fmt.Sprintf("request with incorrect state: %#v", req))
				c.Logger.Debug(fmt.Sprintf("URL of request with incorrect state: %#v", req.URL))
				c.Logger.Debug(fmt.Sprintf("header of request with incorrect state: %#v", req.Header))
				if gotState == "" {
					// Ignore spurious requests (such as for favicon.ico) without state.
					return
				}
				c.UI.Output("callback server: incorrect 'state' value", terminal.WithErrorStyle())
				resp.WriteHeader(http.StatusBadRequest)
				return
			}

			// Did the response return a code?
			gotCode := req.Form.Get("code")
			if gotCode == "" {
				c.UI.Output("callback server: no 'code' argument in callback request", terminal.WithErrorStyle())
				resp.WriteHeader(http.StatusBadRequest)
				return
			}

			// Send the code back to the main execution line.
			c.Logger.Debug(fmt.Sprintf("callback server: got an authorization code: %s", gotCode))
			codeChannel <- gotCode
			close(codeChannel)
			c.Logger.Debug("callback server: sent authorization code to channel; closed channel.")

			// Return an HTTP response.
			c.Logger.Debug("callback server: returning an HTTP response")
			resp.Header().Add("Content-Type", "text/html")
			resp.WriteHeader(http.StatusOK)
			_, _ = resp.Write([]byte(c.buildCallbackResponseBody()))
		}),
	}

	// After creating the server, launch it.
	go func() {
		// At this point, Terraform does
		// defer logging.PanicHandler()
		err := httpServer.Serve(netListener)
		if err != nil && err != http.ErrServerClosed {
			c.UI.Output("failed to start the temporary login server", terminal.WithErrorStyle())
			close(codeChannel)
		}
	}()

	return codeChannel, httpServer, nil
}

// buildCallbackResponseBody builds the response body to be returned by callback server.
func (c *loginCommand) buildCallbackResponseBody() string {
	return fmt.Sprintf(`
	<html>
	<head>
	<title>%s Login</title>
	<style type="text/css">
	body {
		font-family: monospace;
		color: #fff;
		background-color: #000;
	}
	</style>
	</head>
	<body>
	<p>The %s SSO login has successfully completed. This page can now be closed.</p>
	</body>
	</html>
	`, c.DisplayTitle, c.DisplayTitle)
}

func (c *loginCommand) launchBrowser(authCodeURL string, currentSettings *settings.Settings) error {
	// Assume it is possible to launch a browser from here.
	asURL, err := url.Parse(currentSettings.CurrentProfile.Endpoint)
	if err != nil {
		return err
	}
	c.UI.Output("\n%s must now open a web browser to the login page for host %s\n",
		c.DisplayTitle, asURL.Host)

	// Cover the bases in case the browser does not fly.
	c.UI.Output("If a browser does not open automatically, open the following URL:\n%s\n",
		authCodeURL)

	// Ask the user to wait.
	c.UI.Output("%s will now wait for the host to signal that login was successful.\n\n",
		c.DisplayTitle)

	// Either it will fly or it won't.
	return browser.OpenURL(authCodeURL)
}

// captureToken captures the token and terminates the temporary web server.
func (c *loginCommand) captureToken(oauthCfg *oauth2.Config, proofKey string,
	webServerChannel chan string, server *http.Server,
) (*oauth2.Token, error) {
	// Wait for a code (or signal that no code is coming).
	code, ok := <-webServerChannel
	if !ok {
		// No code came or ever will come.
		return nil, errors.New("it was not possible to capture a token")
	}

	// Immediately terminate the web server.
	err := c.terminateWebServer(server)
	if err != nil {
		return nil, err
	}

	dialer := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
		DualStack: true,
	}
	transport := http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		DialContext:           dialer.DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		ForceAttemptHTTP2:     true,
		MaxIdleConnsPerHost:   runtime.GOMAXPROCS(0) + 1,
	}

	httpClient := &http.Client{
		// Use a custom transport to add 'Origin' header to each request.
		Transport: &customHeaderTransport{
			rt: &transport,
		},
	}

	// Add the HTTP client to the context.
	ctx := context.WithValue(context.Background(), oauth2.HTTPClient, httpClient)
	token, err := oauthCfg.Exchange(ctx, code, oauth2.SetAuthURLParam("code_verifier", proofKey))
	if err != nil {
		return nil, errors.New("failed to obtain an authentication token")
	}

	return token, nil
}

// storeToken stores the token.
func (c *loginCommand) storeToken(token *oauth2.Token, currentSettings *settings.Settings) error {
	// Must use the returned id_token as access_token won't work.
	idToken, ok := token.Extra("id_token").(string)
	if !ok {
		return errors.New("no token found in response")
	}

	// Update the internal data structure.
	if currentSettings.CurrentProfile == nil {
		return errors.New("nil current profile, cannot store token")
	}

	// Must find the name of the current profile in order to store the token.
	var foundName string
	for name, candidate := range currentSettings.Profiles {
		if candidate == *currentSettings.CurrentProfile {
			// found it
			foundName = name
		}
	}
	if foundName == "" {
		return errors.New("failed to find name of current profile")
	}
	foundProfile := currentSettings.Profiles[foundName] // returns a copy of the struct
	foundProfile.SetToken(idToken)                      // writes to the copy of the struct
	currentSettings.Profiles[foundName] = foundProfile  // replaces the original struct

	// Write the updated file.
	err := currentSettings.WriteSettingsFile(nil)
	if err != nil {
		return fmt.Errorf("failed to write updated credentials: %s", err)
	}

	return nil
}

// terminateWebServer terminates the embedded web server.
func (c *loginCommand) terminateWebServer(server *http.Server) error {
	// Terraform calls server.Close(), but Shutdown() is recommended as being more graceful.
	err := server.Shutdown(context.Background())
	if err != nil {
		c.UI.Output("Warning: Unable to close the temporary callback HTTP server.", terminal.WithWarningStyle())
		return err
	}

	return nil
}
