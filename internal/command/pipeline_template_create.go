package command

import (
	"flag"
	"fmt"
	"os"
	"path"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/uploader"
)

// pipelineTemplateCreateCommand is the structure for pipeline template create command.
type pipelineTemplateCreateCommand struct {
	*BaseCommand

	directory        string
	filename         string
	organizationName string
	projectName      string
	name             string
	semanticVersion  string
	versioned        bool
	toJSON           bool
}

var _ Command = (*pipelineTemplateCreateCommand)(nil)

func (c *pipelineTemplateCreateCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.organizationName, validation.Required),
		validation.Field(&c.projectName, validation.Required),
		validation.Field(&c.versioned, validation.When(c.name != "" || c.semanticVersion != "", validation.In(true).
			Error("name and semanticVersion fields are prohibited for a non-versioned pipeline template"))),
		validation.Field(&c.name, validation.Required.When(c.versioned).
			Error("name field is required for a versioned pipeline template")),
		validation.Field(&c.semanticVersion, validation.Required.When(c.versioned).
			Error("semanticVersion field is required for a versioned pipeline template")),
	)
}

// NewPipelineTemplateCreateCommandFactory returns a pipelineTemplateCreateCommand struct.
func NewPipelineTemplateCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineTemplateCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineTemplateCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline-template create"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	name := &c.name
	if *name == "" {
		name = nil
	}
	semanticVersion := &c.semanticVersion
	if *semanticVersion == "" {
		semanticVersion = nil
	}

	// Open the input file to make sure that succeeds.
	fh, err := os.Open(path.Join(c.directory, c.filename)) // nosemgrep: gosec.G304-1
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to open HCL input file"))
		return 1
	}
	defer fh.Close()

	input := &pb.CreatePipelineTemplateRequest{
		ProjId:          newResourcePRN("project", c.organizationName, c.projectName),
		Versioned:       c.versioned,
		Name:            name,
		SemanticVersion: semanticVersion,
	}

	c.Logger.Debug(fmt.Sprintf("pipeline template create input: %#v", input))

	createdPipelineTemplate, err := c.client.PipelineTemplatesClient.CreatePipelineTemplate(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create pipeline template"))
		return 1
	}

	// Upload the pipeline template.
	err = uploader.NewPipelineTemplateUploader(c.UI, c.Logger, c.client).Upload(c.Context, createdPipelineTemplate.Metadata.Id, fh)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to upload pipeline template"))
		return 1
	}

	return outputPipelineTemplate(c.Context, c.client, c.UI, c.toJSON, createdPipelineTemplate)
}

func (c *pipelineTemplateCreateCommand) Synopsis() string {
	return "Create a pipeline template."
}

func (c *pipelineTemplateCreateCommand) Usage() string {
	return "phobos [global options] pipeline-template create [options]"
}

func (c *pipelineTemplateCreateCommand) Description() string {
	return `
  The pipeline-template create command creates a new pipeline template
  under the specified organization and project.  The -json flag optionally
  allows showing the final output as formatted JSON.
`
}

func (c *pipelineTemplateCreateCommand) Example() string {
	return `
phobos pipeline-template create \
  --filename my-pipeline-template.hcl \
  --name my-pipeline-template \
  --versioned=true \
  --semantic-version 0.0.1 \
  --org-name my-org \
  --project-name my-project
`
}

func (c *pipelineTemplateCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"Name of organization for the new pipeline.  (This flag is required.)",
	)
	f.StringVar(
		&c.projectName,
		"project-name",
		"",
		"Name of project for the new pipeline.  (This flag is required.)",
	)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to directory that contains the HCL input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultPipelineFilename,
		"Name of HCL input file.",
	)
	f.BoolVar(
		&c.versioned,
		"versioned",
		false,
		"Create a versioned pipeline template.",
	)
	f.StringVar(
		&c.name,
		"name",
		"",
		"Specify a name for a versioned pipeline template.  (Required if -versioned is true.)",
	)
	f.StringVar(
		&c.semanticVersion,
		"semantic-version",
		"",
		"Specify a semantic version for a versioned pipeline template.  (Required if -versioned is true.)",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
