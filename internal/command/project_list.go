package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// projectListCommand is the structure for project list command.
type projectListCommand struct {
	*BaseCommand

	limit     *int32
	cursor    *string
	search    string
	sortOrder string
	toJSON    bool
}

var _ Command = (*projectListCommand)(nil)

func (c *projectListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.limit, validation.Min(0), validation.Max(100), validation.When(c.limit != nil)),
		validation.Field(&c.arguments, validation.Empty),
	)
}

// NewProjectListCommandFactory returns a projectListCommand struct.
func NewProjectListCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &projectListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *projectListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("project list"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	sortOrder := pb.ProjectSortableField_UPDATED_AT_ASC.Enum()
	if strings.ToLower(c.sortOrder) == "desc" {
		sortOrder = pb.ProjectSortableField_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetProjectsRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		Search: &c.search,
	}

	c.Logger.Debug(fmt.Sprintf("project list input: %#v", input))

	result, err := c.client.ProjectsClient.GetProjects(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of projects"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "description", "organization")

		for _, proj := range result.Projects {
			t.Rich([]string{
				proj.GetMetadata().Id,
				proj.Name,
				proj.Description,
				proj.OrgId,
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *projectListCommand) Synopsis() string {
	return "Retrieve a paginated list of projects."
}

func (c *projectListCommand) Usage() string {
	return "phobos [global options] project list [options]"
}

func (c *projectListCommand) Description() string {
	return `
  The project list command prints information about (likely
  multiple) projects. Supports pagination, filtering and
  sorting the output.
`
}

func (c *projectListCommand) Example() string {
	return `
phobos project list \
  --search unseen \
  --limit 5 \
  --json
`
}

func (c *projectListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"cursor",
		"The cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"limit",
		"Maximum number of result elements to return. Defaults to 100.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.StringVar(
		&c.sortOrder,
		"sort-order",
		"ASC",
		"Sort in this direction, ASC or DESC.",
	)
	f.StringVar(
		&c.search,
		"search",
		"",
		"Filter to only projects with this name prefix. "+
			"Helpful when searching for a specific result.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
