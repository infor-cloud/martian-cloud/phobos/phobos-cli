package command

import (
	"bytes"
	"flag"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclwrite"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// fmtCommand is the structure for fmt command.
type fmtCommand struct {
	*BaseCommand

	directory string
	write     bool
	recursive bool
}

var _ Command = (*fmtCommand)(nil)

// NewFmtCommandFactory returns an fmtCommand struct.
func NewFmtCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &fmtCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *fmtCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("fmt"),
	); code != 0 {
		return code
	}

	filesNames := c.arguments

	// If no file(s) are specified, walk the directory and find all HCL files.
	if len(filesNames) == 0 {
		if err := filepath.WalkDir(c.directory, func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}

			// Skip child directories if the recursive flag is false.
			if !c.recursive && filepath.Dir(path) != c.directory {
				return filepath.SkipDir
			}

			// Add the file to the list if it is a HCL file.
			if !d.IsDir() && !strings.HasPrefix(d.Name(), ".") && filepath.Ext(d.Name()) == ".hcl" {
				filesNames = append(filesNames, path)
			}

			return nil
		}); err != nil {
			c.UI.Output(output.FormatError(err, "failed to walk directory"))
			return 1
		}

		c.Logger.Debug("fmt command", "fileNames", filesNames)
	}

	// Format the file(s).
	for _, file := range filesNames {
		src, err := os.ReadFile(file)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to read file"))
			return 1
		}

		f, diags := hclwrite.ParseConfig(src, file, hcl.InitialPos)
		if diags.HasErrors() {
			c.UI.Output(output.FormatError(diags, "failed to parse HCL file"))
			return 1
		}

		formatted := hclwrite.Format(f.Bytes())

		if bytes.Equal(src, formatted) {
			// No changes were made to the file.
			continue
		}

		// Print the file name to the standard output.
		c.UI.Output(file)

		// If the write flag is false, don't write the formatted file.
		if !c.write {
			continue
		}

		// Use the file info to preserve the file mode.
		fileInfo, err := os.Stat(file)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get file info"))
			return 1
		}

		// Write the formatted file.
		if err = os.WriteFile(file, formatted, fileInfo.Mode()); err != nil {
			c.UI.Output(output.FormatError(err, "failed to write file"))
			return 1
		}
	}

	return 0
}

func (*fmtCommand) Synopsis() string {
	return "Reformat HCL configuration files."
}

func (c *fmtCommand) Usage() string {
	return "phobos [global options] fmt [options] [file ...]"
}

func (c *fmtCommand) Description() string {
	return `
  The fmt command rewrites HCL configuration files to canonical format.
  All files with the (.hcl) extension in the current directory will be
  formatted if no file(s) are specified. Lists all files that were
  formatted to the standard output.
`
}

func (c *fmtCommand) Example() string {
	return `
phobos fmt \
  --directory test-pipelines \
  --recursive \
  --write=true
`
}

func (c *fmtCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Directory to format files in. Defaults to the current directory.",
	)
	f.BoolVar(
		&c.write,
		"write",
		true,
		"Write the formatted result to the file(s).",
	)
	f.BoolVar(
		&c.recursive,
		"recursive",
		false,
		"Format files in the specified directory and all subdirectories.",
	)

	return f
}
