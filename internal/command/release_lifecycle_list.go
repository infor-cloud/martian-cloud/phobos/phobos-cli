package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseLifecycleListCommand is the structure for release lifecycle list command.
type releaseLifecycleListCommand struct {
	*BaseCommand

	cursor           *string
	projectName      *string
	limit            *int32
	organizationName string
	sortOrder        string
	search           string
	scopes           arrayFlags
	toJSON           bool
}

var _ Command = (*releaseLifecycleListCommand)(nil)

func (c *releaseLifecycleListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.limit, validation.Min(0), validation.Max(100), validation.When(c.limit != nil)),
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.organizationName, validation.Required),
	)
}

// NewReleaseLifecycleListCommandFactory returns a releaseLifecycleListCommand struct.
func NewReleaseLifecycleListCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseLifecycleListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseLifecycleListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release-lifecycle list"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	sortOrder := pb.ReleaseLifecycleSortableField_UPDATED_AT_ASC.Enum()
	if strings.ToLower(c.sortOrder) == "desc" {
		sortOrder = pb.ReleaseLifecycleSortableField_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetReleaseLifecyclesRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		Search: &c.search,
	}

	for _, scope := range c.scopes {
		scope = strings.ToUpper(scope)
		input.Scopes = append(input.Scopes, pb.ScopeType(pb.ScopeType_value[scope]))
	}

	if c.projectName != nil {
		input.ProjectId = ptr.String(newResourcePRN("project", c.organizationName, *c.projectName))
	} else {
		input.OrgId = ptr.String(newResourcePRN("organization", c.organizationName))
	}

	c.Logger.Debug(fmt.Sprintf("release-lifecycle list input: %#v", input))

	result, err := c.client.ReleaseLifecyclesClient.GetReleaseLifecycles(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of release lifecycles"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "scope", "release-lifecycle-template")

		for _, p := range result.ReleaseLifecycles {
			t.Rich([]string{
				p.GetMetadata().Id,
				p.Name,
				p.Scope,
				p.GetLifecycleTemplateId(),
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *releaseLifecycleListCommand) Synopsis() string {
	return "Retrieve a paginated list of release lifecycles."
}

func (c *releaseLifecycleListCommand) Usage() string {
	return "phobos [global options] release-lifecycle list [options]"
}

func (c *releaseLifecycleListCommand) Description() string {
	return `
  The release-lifecycle list command prints information about (likely
  multiple) release lifecycles. Supports pagination, filtering and
  sorting the output.
`
}

func (c *releaseLifecycleListCommand) Example() string {
	return `
phobos release-lifecycle list \
  --limit 5 \
  --org-name my-organization \
  --json
`
}

func (c *releaseLifecycleListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"cursor",
		"The cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"limit",
		"Maximum number of result elements to return. Defaults to 100.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.StringVar(
		&c.sortOrder,
		"sort-order",
		"ASC",
		"Sort in this direction, ASC or DESC.",
	)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"The organization to limit search. Required.",
	)
	f.Func(
		"project-name",
		"Name of the project under which the release lifecycle will be created.",
		func(s string) error {
			c.projectName = &s
			return nil
		},
	)
	f.StringVar(
		&c.search,
		"search",
		"",
		"Filter to only release lifecycles with this name prefix.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	f.Var(
		&c.scopes,
		"scope",
		"Scope to filter release lifecycles. (This flag may be repeated).",
	)

	return f
}
