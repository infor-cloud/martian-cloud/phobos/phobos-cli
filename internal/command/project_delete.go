package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// projectDeleteCommand is the structure for project delete command.
type projectDeleteCommand struct {
	*BaseCommand

	version string
}

var _ Command = (*projectDeleteCommand)(nil)

func (c *projectDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewProjectDeleteCommandFactory returns a projectDeleteCommand struct.
func NewProjectDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &projectDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *projectDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("project delete"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.DeleteProjectRequest{
		Id: c.arguments[0],
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("project delete input: %#v", input))

	// Returned empty response is not needed.
	if _, err := c.client.ProjectsClient.DeleteProject(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete project"))
		return 1
	}

	c.UI.Output("Project deleted successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *projectDeleteCommand) Synopsis() string {
	return "Delete a project."
}

func (c *projectDeleteCommand) Usage() string {
	return "phobos [global options] project delete [options] <id>"
}

func (c *projectDeleteCommand) Description() string {
	return `
  The project delete command deletes a project
  with the given ID.
`
}

func (c *projectDeleteCommand) Example() string {
	return `
phobos project delete \
  prn:project:my-org/example-proj
`
}

func (c *projectDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be deleted. "+
			"In most cases, this is not required.",
	)

	return f
}
