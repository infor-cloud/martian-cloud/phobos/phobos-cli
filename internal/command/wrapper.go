package command

import (
	"flag"
	"strings"

	"github.com/mitchellh/cli"
)

// Command wraps a CLI command.
type Command interface {
	Usage() string
	Description() string
	Example() string
	Flags() *flag.FlagSet
	Run(args []string) int
	Synopsis() string
}

// Wrapper implements cli.Command and wraps a Command.
type Wrapper struct {
	command Command
}

var _ cli.Command = (*Wrapper)(nil)

// Factory is a function that returns a Command.
type Factory func() (Command, error)

// NewWrapper returns a Wrapper.
func NewWrapper(command Command) Wrapper {
	return Wrapper{command}
}

// Help formats and returns the help text.
// For consistency, trim off the space from the command returns and add it (back) here.
func (c Wrapper) Help() string {
	result := "\n" + strings.TrimSpace(c.command.Usage()) + "\n"

	if description := c.command.Description(); description != "" {
		result += "\n" + strings.TrimSpace(description) + "\n"
	}

	if arrayFlags := c.command.Flags(); arrayFlags != nil {
		result += "\n" + getUsageString(arrayFlags) + "\n"
	}

	if example := strings.TrimSuffix(strings.TrimPrefix(c.command.Example(), "\n"), "\n"); example != "" {
		result += "\nExample:\n\n" + example + "\n\n"
	}

	return formatHelp(result)
}

// Run executes the command.
func (c Wrapper) Run(args []string) int {
	return c.command.Run(args)
}

// Synopsis returns a short description of the command.
func (c Wrapper) Synopsis() string {
	return c.command.Synopsis()
}
