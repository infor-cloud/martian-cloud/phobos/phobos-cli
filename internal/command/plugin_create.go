package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// pluginCreateCommand is the structure for plugin create command.
type pluginCreateCommand struct {
	*BaseCommand

	organizationName string
	repositoryURL    string
	toJSON           bool
	private          bool
	ifNotExists      bool
}

var _ Command = (*pluginCreateCommand)(nil)

func (c *pluginCreateCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
		validation.Field(&c.organizationName, validation.Required),
	)
}

// NewPluginCreateCommandFactory returns a pluginCreateCommand struct.
func NewPluginCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithClient(true),
		WithCommandName("plugin create"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	pluginName := c.arguments[0]

	if c.ifNotExists {
		plugin, err := c.client.PluginRegistryClient.GetPluginByID(c.Context, &pb.GetPluginByIdRequest{
			Id: newResourcePRN("plugin", c.organizationName, pluginName),
		})
		if err != nil && status.Code(err) != codes.NotFound {
			c.UI.Output(output.FormatError(err, "failed to get plugin"))
			return 1
		}

		if plugin != nil {
			outputPlugin(c.UI, c.toJSON, plugin)
			return 0
		}
	}

	input := &pb.CreatePluginRequest{
		Name:           pluginName,
		OrganizationId: newResourcePRN("organization", c.organizationName),
		RepositoryUrl:  c.repositoryURL,
		Private:        c.private,
	}

	c.Logger.Debug(fmt.Sprintf("plugin create input: %#v", input))

	plugin, err := c.client.PluginRegistryClient.CreatePlugin(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create plugin"))
		return 1
	}

	return outputPlugin(c.UI, c.toJSON, plugin)
}

func (*pluginCreateCommand) Synopsis() string {
	return "Create a new plugin in the plugin registry."
}

func (c *pluginCreateCommand) Usage() string {
	return "phobos [global options] plugin create [options] <name>"
}

func (c *pluginCreateCommand) Description() string {
	return `
  The plugin create command creates a new plugin
  at the specified organization. The plugin will
  be private by default. -json flag optionally
  allows showing the final output as formatted
  JSON.
`
}

func (c *pluginCreateCommand) Example() string {
	return `
phobos plugin create \
  --org-name my-org \
  --private=true \
  --json \
  my-plugin
`
}

func (c *pluginCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.organizationName,
		"org-name",
		"",
		"Name of parent organization for the new plugin. (This flag is required).",
	)
	f.StringVar(
		&c.repositoryURL,
		"repository-url",
		"",
		"URL of the repository containing the plugin source code.",
	)
	f.BoolVar(
		&c.private,
		"private",
		true,
		"Keep the plugin private i.e. not visible to other organizations.",
	)
	f.BoolVar(
		&c.ifNotExists,
		"if-not-exists",
		false,
		"Create the plugin only if it does not exist.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
