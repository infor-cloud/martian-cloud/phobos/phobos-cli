package command

import (
	"bytes"
	"flag"
	"net/url"
	"os"
	"path/filepath"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/config"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/hcl/validate"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/plugin"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/varparser"
)

// pipelineValidateCommand is the structure for pipeline get command.
type pipelineValidateCommand struct {
	*BaseCommand
	directory   string
	filename    string
	hclVarFiles arrayFlags
	hclVars     arrayFlags
}

var _ Command = (*pipelineValidateCommand)(nil)

func (c *pipelineValidateCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Empty),
	)
}

// NewPipelineValidateCommandFactory returns a pipelineValidateCommand struct.
func NewPipelineValidateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineValidateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineValidateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("pipeline validate"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	fullPath := filepath.Join(c.directory, c.filename)

	buf, err := os.ReadFile(fullPath) // nosemgrep: gosec.G304-1
	if err != nil {
		if os.IsNotExist(err) {
			c.UI.Output(output.FormatError(nil, "the configuration file %s does not exist.", fullPath))
			return 1
		}
		c.UI.Output(output.FormatError(err, "cannot read %s", fullPath))
		return 1
	}

	hclVarsInput := &varparser.ParseHCLVariablesInput{
		HCLVarFilePaths: c.hclVarFiles,
		HCLVariables:    c.hclVars,
	}

	directory := filepath.Dir(c.directory)
	parser := varparser.NewVariableParser(&directory, true)

	hclVars, err := parser.ParseHCLVariables(hclVarsInput)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse HCL variables"))
		return 1
	}

	varMap := map[string]string{}
	for _, v := range hclVars {
		varMap[v.Key] = v.Value
	}

	hclConfig, _, err := config.NewPipelineTemplate(bytes.NewReader(buf), varMap, config.WithUnknownVars())
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to decode HCL input file: %s", fullPath))
		return 1
	}

	settings, err := c.getCurrentSettings()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get current settings"))
		return 1
	}

	// Parse the default endpoint for accurate hostname comparison.
	defaultEndpointURL, err := url.Parse(settings.CurrentProfile.Endpoint)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to parse default endpoint"))
		return 1
	}

	pluginLogger := c.Logger.Named("phobos-plugin")
	pluginLogger.SetLevel(hclog.LevelFromString("ERROR"))

	pluginManager := plugin.New(pluginLogger)
	defer pluginManager.Close()

	pluginInstaller, err := plugin.NewInstaller(pluginLogger, settings.CurrentProfile.Endpoint)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create plugin installer"))
		return 1
	}

	// Get the plugins requirements from the configuration file and launch the required plugins.
	requirements := map[string]config.PluginRequirement{}
	if hclConfig.PluginRequirements != nil {
		requirements, err = hclConfig.PluginRequirements.Requirements()
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get plugins requirements from HCL configuration"))
			return 1
		}
	}

	var tokenGetter client.TokenGetter

	for _, hclPlugin := range hclConfig.Plugins {
		launchOpts := []plugin.LaunchOption{}
		pluginSource := hclPlugin.Name

		if req, ok := requirements[hclPlugin.Name]; ok {
			source, err := plugin.ParseSource(req.Source, &settings.CurrentProfile.Endpoint)
			if err != nil {
				c.UI.Output(output.FormatError(err, "failed to parse plugin source %q", req.Source))
				return 1
			}

			pluginSource = source.String()

			if req.IsLocal() {
				// Handle local plugin.
				// Append the binary path to the launch options as we'll launch it directly from there.
				launchOpts = append(launchOpts, plugin.WithBinaryPath(*req.Replace))
			} else {
				// Handle remote plugin.
				c.UI.Output("Installing plugin %s...", hclPlugin.Name)

				var token *string
				// Check if the plugin source is the default endpoint.
				if source.Hostname.String() == defaultEndpointURL.Hostname() {
					// Lazy load token getter if it's not already set.
					if tokenGetter == nil {
						tokenGetter, err = settings.CurrentProfile.NewTokenGetter(c.Context)
						if err != nil {
							c.UI.Output(output.FormatError(err, "failed to get token to download plugin %s", req.Source))
							return 1
						}
					}

					// Get token to download the plugin.
					t, err := tokenGetter.Token(c.Context)
					if err != nil {
						c.UI.Output(output.FormatError(err, "failed to get token to download plugin %s", req.Source))
						return 1
					}

					token = &t
				}

				// Ensure the latest version of the plugin meets the requirements.
				version, err := pluginInstaller.EnsureLatestVersion(
					c.Context,
					source.String(),
					plugin.WithConstraints(req.Version),
					plugin.WithToken(token),
				)
				if err != nil {
					c.UI.Output(output.FormatError(err, "failed to ensure a version of plugin %q", hclPlugin.Name))
					return 1
				}

				// Install the plugin if it's not already installed.
				if err := pluginInstaller.InstallPlugin(
					c.Context,
					source.String(),
					version,
					plugin.WithToken(token),
				); err != nil {
					c.UI.Output(output.FormatError(err, "failed to install plugin %s@%s", hclPlugin.Name, version))
					return 1
				}

				launchOpts = append(launchOpts, plugin.WithVersion(version))
				c.UI.Output("Installed plugin %s@%s", hclPlugin.Name, version, terminal.WithSuccessStyle())
			}
		}

		if err := pluginManager.LaunchPlugin(c.Context, pluginSource, launchOpts...); err != nil {
			c.UI.Output(output.FormatError(err, "failed to launch plugin %q", hclPlugin.Name))
			return 1
		}
	}

	validator := validate.New(pluginManager)
	diag := validator.Validate(c.Context, hclConfig, varMap)
	if diag.HasErrors() {
		c.UI.Output(output.FormatError(diag, "pipeline template contains errors"))
		return 1
	}

	c.UI.Output("Pipeline template is valid!", terminal.WithStyle(terminal.SuccessBoldStyle))

	return 0
}

func (c *pipelineValidateCommand) Synopsis() string {
	return "Validates a pipeline template."
}

func (c *pipelineValidateCommand) Usage() string {
	return "phobos [global options] pipeline validate"
}

func (c *pipelineValidateCommand) Description() string {
	return `
  The pipeline validate command validates a pipeline template.

  HCL variables are optional when using the validate command except
  for variables used in dynamic blocks. Variables may be passed in
  via supported options or exported to the environment with
  a 'PB_VAR_' prefix.

  Variable parsing precedence:
  1. HCL variables from the environment.
  2. phobos.pbvars file from configuration's directory,
     if present.
  3. phobos.pbvars.json file from configuration's
     directory, if present.
  4. *.auto.pbvars, *.auto.pbvars.json files
     from the configuration's directory, if present.
  5. --pb-var-file option(s).
  6. --pb-var option(s).

  NOTE: If the same variable is assigned multiple values,
  the last value found will be used. A --pb-var option
  will override the values from a *.pbvars file which
  will override values from the environment (PB_VAR_).
`
}

func (c *pipelineValidateCommand) Example() string {
	return `
phobos pipeline validate \
  --filename my-pipeline-template.hcl
`
}

func (c *pipelineValidateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to directory that contains the HCL input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultPipelineFilename,
		"Name of HCL input file.",
	)
	f.Var(&c.hclVarFiles,
		"pb-var-file",
		"Path to a HCL variable file (*.pbvars or *.pbvars.json). (This flag may be repeated).",
	)
	f.Var(&c.hclVars,
		"pb-var",
		"Define a HCL variable as a key=value. To supply a string, do --pb-var 'sample=this is a value'. "+
			"Non key=value pairs will be ignored. (This flag may be repeated).",
	)

	return f
}
