// Package command contains all the logic for different commands
// within the CLI. It is the main gateway to doing operations
// against the Phobos API.
package command

import (
	"context"
	"flag"
	"fmt"
	"io"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"github.com/mitchellh/cli"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/settings"
)

// baseOptions contains the different ways to configure the behavior of BaseCommand.
type baseOptions struct {
	flags          *flag.FlagSet
	inputValidator func() error
	commandName    string
	args           []string
	withClient     bool
	withAuth       bool // Not stored in the settings.
}

// BaseOptionsFunc is an alias that allows setting baseOptions.
type BaseOptionsFunc func(*baseOptions) error

// WithFlags sets the FlagSet that needs to be parsed. Return
// values are often set as fields on the caller command's struct.
func WithFlags(flags *flag.FlagSet) BaseOptionsFunc {
	return func(o *baseOptions) error {
		o.flags = flags
		return nil
	}
}

// WithCommandName is the name of the command that called
// BaseCommand.initialize(). It should always be set as
// it allows for helpful debugger statements.
func WithCommandName(name string) BaseOptionsFunc {
	return func(o *baseOptions) error {
		o.commandName = name
		return nil
	}
}

// WithArguments sets the raw arguments that are passed into a command.
// It facilitates the parsing of flags and arguments.
func WithArguments(args []string) BaseOptionsFunc {
	return func(o *baseOptions) error {
		o.args = args
		return nil
	}
}

// WithInputValidator allows the calling command to pass in a input validator func,
// which once called, ensures proper data was passed into command. It can be used
// to make sure a flag was specified, or the value is a URL, etc.
func WithInputValidator(inputValidator func() error) BaseOptionsFunc {
	return func(o *baseOptions) error {
		o.inputValidator = inputValidator
		return nil
	}
}

// WithClient indicates that a gRPC client is needed by the command.
// Callers can set the withAuth parameter to indicate if client
// should be initialized with auth. Client should be available on the
// BaseCommand struct after initialize() has been called.
func WithClient(withAuth bool) BaseOptionsFunc {
	return func(o *baseOptions) error {
		o.withClient = true
		o.withAuth = withAuth
		return nil
	}
}

// BaseCommand contains data needed by all the CLI commands.
// It provides access to the UI, logger and other metadata
// information. Private fields are only populated after
// initialize() has been called and are entirely controllable
// by using the baseOptions above.
type BaseCommand struct {
	Context              context.Context
	Logger               hclog.Logger
	UI                   terminal.UI
	client               *client.Client
	Version              string
	DisplayTitle         string
	BinaryName           string
	CurrentProfileName   string
	DefaultHTTPEndpoint  string
	arguments            []string
	DefaultTLSSkipVerify bool
}

// initialize performs some preliminary tasks for each command. It should be
// one of the first functions called by each of the commands to parse flags,
// arguments and initialize the SDK client, if needed. The values for parsed
// flags are generally stored in the caller command's struct, and any
// arguments are available in the 'arguments' field. Use baseOptions above
// to control what happens when. Errors are already logged, so caller can
// simply check for a non-zero status code.
func (c *BaseCommand) initialize(opts ...BaseOptionsFunc) int {
	// Populate baseOptions struct with options.
	o := baseOptions{}
	for _, opt := range opts {
		if err := opt(&o); err != nil {
			c.UI.Output(output.FormatError(err, "failed to load base command options"))
			return 1
		}
	}

	c.Logger.Debug(fmt.Sprintf("Starting the '%s' with %d arguments:", o.commandName, len(o.args)))
	for ix, arg := range o.args {
		c.Logger.Debug(fmt.Sprintf("    argument %d: %s", ix, arg))
	}

	if o.flags != nil {
		// Discard any output from flags.
		o.flags.SetOutput(io.Discard)

		// Parse flags.
		if err := o.flags.Parse(o.args); err != nil {
			c.UI.Output(output.FormatError(err, "failed to parse %s options", o.commandName))
			return cli.RunResultHelp
		}

		c.arguments = o.flags.Args()
	} else {
		// There are no flags defined for the command, so default to all arguments.
		c.arguments = o.args
	}

	// Call input validator if there is one.
	if o.inputValidator != nil {
		if err := o.inputValidator(); err != nil {
			c.UI.Output(output.FormatError(err, "failed to validate %s input", o.commandName))
			return cli.RunResultHelp
		}
	}

	if o.withClient {
		curSettings, err := c.getCurrentSettings()
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get current settings"))
			return 1
		}

		client, err := curSettings.CurrentProfile.NewClient(c.Context, o.withAuth, c.Logger)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get client"))
			return 1
		}

		c.client = client
	}

	return 0
}

// getCurrentSettings returns the current settings in use for the CLI.
func (c *BaseCommand) getCurrentSettings() (*settings.Settings, error) {
	// Read the current settings.
	currentSettings, err := settings.ReadSettings(nil)
	if err != nil {
		return nil, err
	}

	// Now, we can set the current profile pointer.
	if err := currentSettings.SetCurrentProfile(c.CurrentProfileName); err != nil {
		return nil, err
	}

	c.Logger.Debug(fmt.Sprintf("settings: %#v", currentSettings))

	return currentSettings, nil
}

// Close closes any pending resources.
func (c *BaseCommand) Close() error {
	if closer, ok := c.UI.(io.Closer); ok && closer != nil {
		return closer.Close()
	}

	if c.client != nil {
		// Close the gRPC connection.
		return c.client.Close()
	}

	return nil
}
