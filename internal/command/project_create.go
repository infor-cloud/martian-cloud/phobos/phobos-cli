package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// projectCreateCommand is the structure for project create command.
type projectCreateCommand struct {
	*BaseCommand

	description string
	orgName     string
	toJSON      bool
}

var _ Command = (*projectCreateCommand)(nil)

func (c *projectCreateCommand) validate() error {
	const message = "name is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
		validation.Field(&c.orgName, validation.Required),
	)
}

// NewProjectCreateCommandFactory returns a projectCreateCommand struct.
func NewProjectCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &projectCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *projectCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("project create"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	org, err := c.client.OrganizationsClient.GetOrganizationByID(c.Context, &pb.GetOrganizationByIdRequest{
		Id: newResourcePRN("organization", c.orgName),
	})
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get organization"))
		return 1
	}

	input := &pb.CreateProjectRequest{
		Name:        c.arguments[0],
		Description: c.description,
		OrgId:       org.Metadata.Id,
	}

	c.Logger.Debug(fmt.Sprintf("project create input: %#v", input))

	createdProj, err := c.client.ProjectsClient.CreateProject(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create project"))
		return 1
	}

	return outputProject(c.UI, c.toJSON, createdProj)
}

func (c *projectCreateCommand) Synopsis() string {
	return "Create a project."
}

func (c *projectCreateCommand) Usage() string {
	return "phobos [global options] project create [options] <name>"
}

func (c *projectCreateCommand) Description() string {
	return `
  The project create command creates a new project
  with the given name and description under the specified
  parent organization.  The -json flag optionally
  allows showing the final output as formatted JSON.
`
}

func (c *projectCreateCommand) Example() string {
	return `
phobos project create \
  --json \
  --description "this is my project" \
  --org-name my-org \
  example-proj
`
}

func (c *projectCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.description,
		"description",
		"",
		"Description for the new project.",
	)
	f.StringVar(
		&c.orgName,
		"org-name",
		"",
		"Name of parent organization for the new project.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
