package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// projectUpdateCommand is the structure for project update command.
type projectUpdateCommand struct {
	*BaseCommand

	description string
	version     string
	toJSON      bool
}

var _ Command = (*projectUpdateCommand)(nil)

func (c *projectUpdateCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewProjectUpdateCommandFactory returns a projectUpdateCommand struct.
func NewProjectUpdateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &projectUpdateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *projectUpdateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("project update"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.UpdateProjectRequest{
		Id:          c.arguments[0],
		Description: &c.description,
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("project update input: %#v", input))

	updatedProj, err := c.client.ProjectsClient.UpdateProject(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update project"))
		return 1
	}

	return outputProject(c.UI, c.toJSON, updatedProj)
}

func (c *projectUpdateCommand) Synopsis() string {
	return "Update a project."
}

func (c *projectUpdateCommand) Usage() string {
	return "phobos [global options] project update [options] <id>"
}

func (c *projectUpdateCommand) Description() string {
	return `
  The project update command updates a project with
  the given description. -json flag optionally allows showing
  the final output as formatted JSON.
`
}

func (c *projectUpdateCommand) Example() string {
	return `
phobos project update \
  --description "this is an updated description" \
  prn:project:my-org/example-proj
`
}

func (c *projectUpdateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.description,
		"description",
		"",
		"Description for the new project.",
	)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be updated. "+
			"In most cases, this is not required.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
