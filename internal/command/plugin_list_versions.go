package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pluginListVersionsCommand is the structure for the plugin list-versions command.
type pluginListVersionsCommand struct {
	*BaseCommand

	cursor          *string
	sortOrder       *string
	semanticVersion *string
	limit           *int32
	shaSumsUploaded *bool
	latest          *bool
	toJSON          bool
}

var _ Command = (*pluginListVersionsCommand)(nil)

func (c *pluginListVersionsCommand) validate() error {
	const message = "plugin-id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
		validation.Field(&c.limit, validation.Min(0), validation.Max(100)),
		validation.Field(&c.sortOrder, validation.In("ASC", "DESC")),
	)
}

// NewPluginListVersionsCommandFactory returns a pluginListVersionsCommand struct.
func NewPluginListVersionsCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginListVersionsCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginListVersionsCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("plugin list-versions"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	sortOrder := pb.PluginVersionSortableField_PluginVersion_UPDATED_AT_ASC.Enum()
	if c.sortOrder != nil && strings.ToLower(*c.sortOrder) == "desc" {
		sortOrder = pb.PluginVersionSortableField_PluginVersion_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetPluginVersionsRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		PluginId:        c.arguments[0],
		SemanticVersion: c.semanticVersion,
		ShaSumsUploaded: c.shaSumsUploaded,
		Latest:          c.latest,
	}

	c.Logger.Debug(fmt.Sprintf("plugin list-versions input: %#v", input))

	result, err := c.client.PluginRegistryClient.GetPluginVersions(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get plugin versions"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "version", "protocols", "latest")

		for _, p := range result.PluginVersions {
			t.Rich([]string{
				p.Metadata.Id,
				p.SemanticVersion,
				strings.Join(p.Protocols, ", "),
				strconv.FormatBool(p.Latest),
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (*pluginListVersionsCommand) Synopsis() string {
	return "List versions for a plugin from the registry."
}

func (c *pluginListVersionsCommand) Usage() string {
	return "phobos [global options] plugin list-versions [options] <plugin-id>"
}

func (c *pluginListVersionsCommand) Description() string {
	return `
  The plugin list-versions command lists the available versions
  for a plugin. -json can be used to output the list in JSON
  format. Advanced filtering can be done using the flags.
`
}

func (c *pluginListVersionsCommand) Example() string {
	return `
phobos plugin list-versions \
  --limit 10 \
  --sort DESC \
  --json \
  prn:plugin:tomcat/gitlab
`
}

func (c *pluginListVersionsCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"limit",
		"Limit the number of results.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.Func(
		"sort",
		"Sort in this direction, ASC or DESC.",
		func(s string) error {
			c.sortOrder = &s
			return nil
		},
	)
	f.Func(
		"cursor",
		"Cursor string for manual pagination.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"version",
		"Semantic version of the plugin.",
		func(s string) error {
			c.semanticVersion = &s
			return nil
		},
	)
	f.BoolFunc(
		"has-checksums",
		"Filter by whether the sha sums are uploaded.",
		func(s string) error {
			b, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}
			c.shaSumsUploaded = &b
			return nil
		},
	)
	f.BoolFunc(
		"latest",
		"Filter by whether the version is the latest.",
		func(s string) error {
			b, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}
			c.latest = &b
			return nil
		},
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
