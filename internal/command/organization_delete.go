package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// organizationDeleteCommand is the structure for organization delete command.
type organizationDeleteCommand struct {
	*BaseCommand

	version string
}

var _ Command = (*organizationDeleteCommand)(nil)

func (c *organizationDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewOrganizationDeleteCommandFactory returns an organizationDeleteCommand struct.
func NewOrganizationDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &organizationDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *organizationDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("organization delete"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.DeleteOrganizationRequest{
		Id: c.arguments[0],
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("organization delete input: %#v", input))

	// Returned empty response is not needed.
	if _, err := c.client.OrganizationsClient.DeleteOrganization(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete organization"))
		return 1
	}

	c.UI.Output("Organization deleted successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *organizationDeleteCommand) Synopsis() string {
	return "Delete an organization."
}

func (c *organizationDeleteCommand) Usage() string {
	return "phobos [global options] organization delete [options] <id>"
}

func (c *organizationDeleteCommand) Description() string {
	return `
  The organization delete command deletes an organization
  with the given ID.
`
}

func (c *organizationDeleteCommand) Example() string {
	return `
phobos organization delete prn:organization:example-org
`
}

func (c *organizationDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be deleted. "+
			"In most cases, this is not required.",
	)

	return f
}
