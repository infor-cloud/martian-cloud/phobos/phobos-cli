package command

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	// defaultReleaseDocFilename is the default filename used to find the release JSON doc.
	defaultReleaseDocFilename = "release.json"
)

// releaseCreateComment is the structure for the release create command.
type releaseCreateCommand struct {
	*BaseCommand

	directory string
	filename  string
	toJSON    bool
}

var _ Command = (*releaseCreateCommand)(nil)

func (c *releaseCreateCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Empty),
	)
}

// NewReleaseCreateCommandFactory returns a releaseCreateCommand struct.
func NewReleaseCreateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseCreateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseCreateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release create"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	// Open the input file to make sure that succeeds.
	releaseDoc, err := os.ReadFile(filepath.Join(c.directory, c.filename)) // nosemgrep: gosec.G304-1
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to read release JSON doc"))
		return 1
	}

	var input pb.CreateReleaseRequest
	if err = protojson.Unmarshal(releaseDoc, &input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to unmarshal release JSON doc"))
		return 1
	}

	c.Logger.Debug(fmt.Sprintf("release create input: %#v", &input))

	createdRelease, err := c.client.ReleasesClient.CreateRelease(c.Context, &input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to create release"))
		return 1
	}

	return outputRelease(c.UI, c.toJSON, createdRelease)
}

func (*releaseCreateCommand) Synopsis() string {
	return "Create a release."
}

func (c *releaseCreateCommand) Usage() string {
	return "phobos [global options] release create [options]"
}

func (c *releaseCreateCommand) Description() string {
	return fmt.Sprintf(`
  The release create command creates a new release for
  the specified project. The -json flag optionally
  allows showing the final output as formatted JSON.

  ###### Schema for the release JSON input file:
%[1]sjson
{
	"$schema": "http://json-schema.org/draft-04/schema#",
	"type": "object",
	"properties": {
		"project_id": {
			"description": "The project ID or PRN",
			"type": "string"
		},
		"lifecycle_id": {
			"description": "The release lifecycle ID or PRN",
			"type": "string"
		},
		"semantic_version": {
			"description": "The semantic version of the release",
			"type": "string"
		},
		"deployment_templates": {
			"description": "The deployment templates to be used for the release",
			"type": "array",
			"minItems": 1,
			"items": {
				"type": "object",
				"properties": {
					"environment_name": {
						"description": "The name of the environment",
						"type": "string"
					},
					"pipeline_template_id": {
						"description": "The ID or PRN of the pipeline template",
						"type": "string"
					}
				},
				"required": [
					"environment_name",
					"pipeline_template_id"
				]
			}
		},
		"hcl_variables": {
			"type": "array",
			"items": {
				"type": "object",
				"properties": {
					"key": {
						"description": "The key of the variable",
						"type": "string"
					},
					"value": {
						"description": "The value of the variable",
						"type": "string"
					}
				},
				"required": [
					"key",
					"value"
				]
			}
		},
		"variable_set_revision": {
			"description": "The variable set revision ID or latest",
			"type": "string"
		},
		"due_date": {
			"description": "The due date of the release",
			"type": "string"
		},
		"user_participant_ids": {
			"description": "The user participant IDs or PRNs",
			"type": "array",
			"items": {
				"type": "string"
			}
		},
		"team_participant_ids": {
			"description": "The team participant IDs or PRNs",
			"type": "array",
			"items": {
				"type": "string"
			}
		},
		"notes": {
			"description": "The notes for the release",
			"type": "string"
		}
	},
	"required": [
		"project_id",
		"lifecycle_id",
		"semantic_version",
		"deployment_templates",
		"hcl_variables",
		"user_participant_ids",
		"team_participant_ids",
		"notes"
	]
}
%[1]s
`, "```")
}

func (c *releaseCreateCommand) Example() string {
	return `
cat <<-EOF > release.json
{
    "project_id": "prn:project:example-org/example-project",
    "lifecycle_id": "prn:release_lifecycle:example-org/standard",
    "semantic_version": "1.0.1",
    "deployment_templates": [
        {
            "environment_name": "dev",
            "pipeline_template_id": "prn:pipeline_template:example-org/example-project/deploy/1.0.0"
        },
        {
            "environment_name": "prod",
            "pipeline_template_id": "prn:pipeline_template:example-org/example-project/deploy/1.0.0"
        }
    ],
    "hcl_variables": [
        {
            "key": "region",
            "value": "us-east-1"
        }
    ],
    "variable_set_revision": "latest",
    "due_date": "2025-01-01T12:00:00.00Z",
    "user_participant_ids": [
        "prn:user:john.smith"
    ],
    "team_participant_ids": [],
    "notes": "This is an example release"
}
EOF

phobos release create \
  --filename release.json
`
}

func (c *releaseCreateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.directory,
		"directory",
		defaultDirectory,
		"Path to the directory that contains the release JSON input file.",
	)
	f.StringVar(
		&c.filename,
		"filename",
		defaultReleaseDocFilename,
		"Name of the JSON input file.",
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	return f
}
