package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseLifecycleDeleteCommand is the structure for release lifecycle delete command.
type releaseLifecycleDeleteCommand struct {
	*BaseCommand

	version string
}

var _ Command = (*releaseLifecycleDeleteCommand)(nil)

func (c *releaseLifecycleDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseLifecycleDeleteCommandFactory returns a releaseLifecycleDeleteCommand struct.
func NewReleaseLifecycleDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseLifecycleDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseLifecycleDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release-lifecycle delete"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.DeleteReleaseLifecycleRequest{
		Id: c.arguments[0],
	}

	if c.version != "" {
		input.Version = &c.version
	}

	c.Logger.Debug(fmt.Sprintf("release lifecycle delete input: %#v", input))

	// Returned empty response is not needed.
	if _, err := c.client.ReleaseLifecyclesClient.DeleteReleaseLifecycle(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete release lifecycle"))
		return 1
	}

	c.UI.Output("ReleaseLifecycle deleted successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *releaseLifecycleDeleteCommand) Synopsis() string {
	return "Delete a release lifecycle."
}

func (c *releaseLifecycleDeleteCommand) Usage() string {
	return "phobos [global options] release-lifecycle delete [options] <id>"
}

func (c *releaseLifecycleDeleteCommand) Description() string {
	return `
  The release-lifecycle delete command deletes a release lifecycle
  with the given ID.
`
}

func (c *releaseLifecycleDeleteCommand) Example() string {
	return `
phobos release-lifecycle delete \
  prn:release_lifecycle:my-org/my-project/example-release-lifecycle
`
}

func (c *releaseLifecycleDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.StringVar(
		&c.version,
		"version",
		"",
		"Metadata version of the resource to be deleted. "+
			"In most cases, this is not required.",
	)

	return f
}
