package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseRemoveParticipantCommand is the structure for the release remove-participant command.
type releaseRemoveParticipantCommand struct {
	*BaseCommand

	userID *string
	teamID *string
	toJSON bool
}

var _ Command = (*releaseRemoveParticipantCommand)(nil)

func (c *releaseRemoveParticipantCommand) validate() error {
	const message = "release-id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseRemoveParticipantCommandFactory returns a releaseRemoveParticipantCommand struct.
func NewReleaseRemoveParticipantCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseRemoveParticipantCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseRemoveParticipantCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release remove-participant"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.RemoveParticipantFromReleaseRequest{
		ReleaseId: c.arguments[0],
		UserId:    c.userID,
		TeamId:    c.teamID,
	}

	c.Logger.Debug(fmt.Sprintf("release remove-participant input: %#v", input))

	release, err := c.client.ReleasesClient.RemoveParticipantFromRelease(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to remove participant from release"))
		return 1
	}

	return outputRelease(c.UI, c.toJSON, release)
}

func (*releaseRemoveParticipantCommand) Synopsis() string {
	return "Remove a participant from a release."
}

func (c *releaseRemoveParticipantCommand) Usage() string {
	return "phobos [global options] release remove-participant [options] <release-id>"
}

func (c *releaseRemoveParticipantCommand) Description() string {
	return `
  The release remove-participant command removes one or more participants
  from a release. The -json flag optionally allows showing the final
  output as formatted JSON.
`
}

func (c *releaseRemoveParticipantCommand) Example() string {
	return `
phobos release remove-participant \
  --user-id prn:user:random.person \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseRemoveParticipantCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	f.Func(
		"user-id",
		"ID of the user participant to remove from the release.",
		func(s string) error {
			c.userID = &s
			return nil
		},
	)
	f.Func(
		"team-id",
		"ID of the team participant to remove from the release.",
		func(s string) error {
			c.teamID = &s
			return nil
		},
	)
	return f
}
