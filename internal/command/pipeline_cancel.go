package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// pipelineCancelCommand is the structure for pipeline cancel command.
type pipelineCancelCommand struct {
	*BaseCommand
}

var _ Command = (*pipelineCancelCommand)(nil)

func (c *pipelineCancelCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)), // Require only 1 argument.
	)
}

// NewPipelineCancelCommandFactory returns a pipelineCancelCommand struct.
func NewPipelineCancelCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pipelineCancelCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pipelineCancelCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithCommandName("pipeline cancel"),
		WithInputValidator(c.validate),
		WithClient(true),
	); code != 0 {
		return code
	}

	input := &pb.CancelPipelineRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("pipeline cancel input: %#v", input))

	_, err := c.client.PipelinesClient.CancelPipeline(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to cancel pipeline"))
		return 1
	}

	c.UI.Output("Pipeline canceled successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (c *pipelineCancelCommand) Synopsis() string {
	return "Cancel a pipeline."
}

func (c *pipelineCancelCommand) Usage() string {
	return "phobos [global options] pipeline cancel <id>"
}

func (c *pipelineCancelCommand) Description() string {
	return `
  The pipeline cancel command cancels a pipeline by id.
`
}

func (c *pipelineCancelCommand) Example() string {
	return `
phobos pipeline cancel \
  prn:pipeline:my-org/my-project/MM3DEOLGGVSTILLEGJSTOLJUG4ZTQLJYMYZWMLLFHA3TEZLFGFTDEYZUGJPVASI
`
}

func (c *pipelineCancelCommand) Flags() *flag.FlagSet {
	return nil
}
