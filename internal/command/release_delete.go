package command

import (
	"flag"
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
)

// releaseDeleteCommand is the structure for the release delete command.
type releaseDeleteCommand struct {
	*BaseCommand

	version *string
}

var _ Command = (*releaseDeleteCommand)(nil)

func (c *releaseDeleteCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.version, is.Int, validation.When(c.version != nil)),
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseDeleteCommandFactory returns a releaseDeleteCommand struct.
func NewReleaseDeleteCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseDeleteCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseDeleteCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release delete"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	input := &pb.DeleteReleaseRequest{
		Id:      c.arguments[0],
		Version: c.version,
	}

	c.Logger.Debug(fmt.Sprintf("release delete input: %#v", input))

	if _, err := c.client.ReleasesClient.DeleteRelease(c.Context, input); err != nil {
		c.UI.Output(output.FormatError(err, "failed to delete release"))
		return 1
	}

	c.UI.Output("Release deleted successfully!", terminal.WithStyle(terminal.SuccessBoldStyle))
	return 0
}

func (*releaseDeleteCommand) Synopsis() string {
	return "Delete a release."
}

func (c *releaseDeleteCommand) Usage() string {
	return "phobos [global options] release delete [options] <id>"
}

func (c *releaseDeleteCommand) Description() string {
	return `
  The release delete command deletes a release.
`
}

func (c *releaseDeleteCommand) Example() string {
	return `
phobos release delete \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseDeleteCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func(
		"version",
		"Metadata version of the resource to be deleted. In most cases, this is not required.",
		func(s string) error {
			c.version = &s
			return nil
		},
	)

	return f
}
