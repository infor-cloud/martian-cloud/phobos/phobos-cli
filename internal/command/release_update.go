package command

import (
	"flag"
	"fmt"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// releaseUpdateCommand is the structure for the release update command.
type releaseUpdateCommand struct {
	*BaseCommand

	notes   *string
	dueDate *string
	toJSON  bool
}

var _ Command = (*releaseUpdateCommand)(nil)

func (c *releaseUpdateCommand) validate() error {
	const message = "id is required"
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Required.Error(message), validation.Length(1, 1).Error(message)),
	)
}

// NewReleaseUpdateCommandFactory returns a releaseUpdateCommand struct.
func NewReleaseUpdateCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &releaseUpdateCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *releaseUpdateCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("release update"),
		WithClient(true),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	getReleaseInput := &pb.GetReleaseByIdRequest{
		Id: c.arguments[0],
	}

	c.Logger.Debug(fmt.Sprintf("release get input: %#v", getReleaseInput))

	// Get the release so we can keep the release due date intact if it's not being updated.
	release, err := c.client.ReleasesClient.GetReleaseByID(c.Context, getReleaseInput)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get release"))
		return 1
	}

	input := &pb.UpdateReleaseRequest{
		Id:      release.Metadata.Id,
		Notes:   c.notes,
		DueDate: release.DueDate,
	}

	if c.dueDate != nil && *c.dueDate != "none" {
		// 2006-01-02T15:04:05Z07:00
		parsed, pErr := time.Parse(time.RFC3339, *c.dueDate)
		if pErr != nil {
			c.UI.Output(output.FormatError(pErr, "failed to parse due date as RFC3339"))
			return 1
		}

		input.DueDate = timestamppb.New(parsed.UTC())
	}

	if c.dueDate != nil && *c.dueDate == "none" {
		// Remove the due date.
		input.DueDate = nil
	}

	c.Logger.Debug(fmt.Sprintf("release update input: %#v", input))

	updatedRelease, err := c.client.ReleasesClient.UpdateRelease(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to update release"))
		return 1
	}

	return outputRelease(c.UI, c.toJSON, updatedRelease)
}

func (*releaseUpdateCommand) Synopsis() string {
	return "Update a release."
}

func (c *releaseUpdateCommand) Usage() string {
	return "phobos [global options] release update [options] <id>"
}

func (c *releaseUpdateCommand) Description() string {
	return `
  The release update command updates a release. The -json
  flag optionally allows showing the final output as
  formatted JSON.
`
}

func (c *releaseUpdateCommand) Example() string {
	return `
phobos release update \
  --due-date none \
  --notes "this is a note" \
  prn:release:my-org/my-project/0.0.17
`
}

func (c *releaseUpdateCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.Func("notes",
		"Notes associated with the release.",
		func(s string) error {
			c.notes = &s
			return nil
		},
	)
	f.Func("due-date",
		"Due date for the release. Format must be '2006-01-02T15:04:05Z07:00'. "+
			"As a special case, supplying 'none' will remove the due date.",
		func(s string) error {
			c.dueDate = &s
			return nil
		},
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)
	return f
}
