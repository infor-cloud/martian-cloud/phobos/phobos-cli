package command

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"strings"
	"text/tabwriter"

	"github.com/kr/text"
	"github.com/mitchellh/go-glint"
)

const (
	// defaultDirectory is the default directory to use for commands that
	// require a directory. This is "." by default.
	defaultDirectory = "."
)

// arrayFlags contains values from duplicates flags in one slice.
// Allows passing in multiple variables.
type arrayFlags []string

// String returns the string representation of the slice.
func (a *arrayFlags) String() string {
	return ""
}

// Set adds a value to the slice.
func (a *arrayFlags) Set(value string) error {
	*a = append(*a, value)
	return nil
}

// objectToJSON marshals object and returns the result as a string.
func objectToJSON(object any) (string, error) {
	buf, err := json.MarshalIndent(object, "", "    ")
	if err != nil {
		return "", err
	}

	return string(buf), nil
}

// getUsageString returns the flag usage as a string.
// This should be called in the Help() function of any
// command that defines a flag.FlagSet.
func getUsageString(set *flag.FlagSet) string {
	var buf bytes.Buffer
	document := glint.New()
	document.SetRenderer(&glint.TerminalRenderer{
		Output: &buf,
		Rows:   10,
		Cols:   80,
	})
	document.Append(glint.Text("Command options:"))

	// Visit each of the flags and build the options output.
	var optionsBuf bytes.Buffer
	writer := tabwriter.NewWriter(&optionsBuf, 0, 80, 0, ' ', 0)
	set.VisitAll(func(f *flag.Flag) {
		var defValue string
		if f.DefValue != "" {
			defValue = fmt.Sprintf("(default %s)", f.DefValue)
		}

		// Wrap text after 70 characters (figured out experimentally).
		wrapped := text.Wrap(f.Usage, 70)
		lines := strings.Split(wrapped, "\n")
		for i, line := range lines {
			// Add some padding before each line to make them look
			// like they're on the same column.
			lines[i] = strings.Repeat(" ", 6) + line
		}

		fmt.Fprintf(writer, "-%s %s\n\t%s\n", f.Name, defValue, strings.Join(lines, "\n"))
	})

	// Flush the writer, so everything is written to the buffer.
	writer.Flush()

	document.Append(glint.Layout(
		glint.Text(strings.TrimSpace(optionsBuf.String())), // Remove extra space.
	).PaddingLeft(2)) // Add some padding to left.

	document.RenderFrame()
	return buf.String()
}

// newResourcePRN returns a new PRN string for the given resource and
// arguments. This is a helper function for creating PRNs.
func newResourcePRN(resource string, a ...string) string {
	return fmt.Sprintf("prn:%s:%s", resource, strings.Join(a, "/"))
}

// resourcePathFromPRN returns the resource path from the PRN.
func resourcePathFromPRN(prn string) string {
	index := strings.LastIndex(prn, ":")
	return prn[index+1:]
}
