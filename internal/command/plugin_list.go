package command

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"github.com/aws/smithy-go/ptr"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/output"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/plugins"
)

// pluginListCommand is the struct for the plugin list command.
type pluginListCommand struct {
	*BaseCommand

	search           *string
	cursor           *string
	organizationName *string
	sortOrder        *string
	limit            *int32
	builtInOnly      bool
	toJSON           bool
}

var _ Command = (*pluginListCommand)(nil)

func (c *pluginListCommand) validate() error {
	return validation.ValidateStruct(c,
		validation.Field(&c.arguments, validation.Empty),
		validation.Field(&c.limit, validation.Min(0), validation.Max(100), validation.When(!c.builtInOnly)),
		validation.Field(&c.sortOrder, validation.In("ASC", "DESC"), validation.When(!c.builtInOnly)),
	)
}

// NewPluginListCommandFactory returns a pluginListCommand struct.
func NewPluginListCommandFactory(baseCommand *BaseCommand) func() (Command, error) {
	return func() (Command, error) {
		return &pluginListCommand{
			BaseCommand: baseCommand,
		}, nil
	}
}

func (c *pluginListCommand) Run(args []string) int {
	if code := c.initialize(
		WithArguments(args),
		WithFlags(c.Flags()),
		WithCommandName("plugin list"),
		WithInputValidator(c.validate),
	); code != 0 {
		return code
	}

	if c.builtInOnly {
		pluginList := plugins.ListPlugins()
		if c.toJSON {
			buf, err := objectToJSON(pluginList)
			if err != nil {
				c.UI.Output(output.FormatError(err, "failed to get JSON output"))
				return 1
			}
			c.UI.Output(string(buf))
		} else {
			t := terminal.NewTable("name", "description")
			for name, meta := range pluginList {
				t.Rich([]string{
					name,
					meta.Description,
				}, nil)
			}
			c.UI.Table(t)
		}

		return 0
	}

	// Get the current settings since we need an authenticated client.
	currentSettings, err := c.getCurrentSettings()
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get current settings"))
		return 1
	}

	c.client, err = currentSettings.CurrentProfile.NewClient(c.Context, true, c.Logger)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get client"))
		return 1
	}

	sortOrder := pb.PluginSortableField_Plugin_UPDATED_AT_ASC.Enum()
	if c.sortOrder != nil && strings.ToLower(*c.sortOrder) == "desc" {
		sortOrder = pb.PluginSortableField_Plugin_UPDATED_AT_DESC.Enum()
	}

	if c.limit == nil {
		c.limit = ptr.Int32(100)
	}

	input := &pb.GetPluginsRequest{
		Sort: sortOrder,
		PaginationOptions: &pb.PaginationOptions{
			First: c.limit,
			After: c.cursor,
		},
		Search: c.search,
	}

	if c.organizationName != nil {
		orgPRN := newResourcePRN("organization", *c.organizationName)
		input.OrganizationId = &orgPRN
	}

	c.Logger.Debug(fmt.Sprintf("plugin list input: %#v", input))

	result, err := c.client.PluginRegistryClient.GetPlugins(c.Context, input)
	if err != nil {
		c.UI.Output(output.FormatError(err, "failed to get a list of plugins"))
		return 1
	}

	if c.toJSON {
		buf, err := objectToJSON(result)
		if err != nil {
			c.UI.Output(output.FormatError(err, "failed to get JSON output"))
			return 1
		}
		c.UI.Output(string(buf))
	} else {
		t := terminal.NewTable("id", "name", "organization", "private")

		for _, p := range result.Plugins {
			t.Rich([]string{
				p.Metadata.Id,
				p.Name,
				p.OrganizationId,
				strconv.FormatBool(p.Private),
			}, nil)
		}

		c.UI.Table(t)
		namedValues := []terminal.NamedValue{
			{Name: "Total count", Value: result.GetPageInfo().TotalCount},
			{Name: "Has Next Page", Value: result.GetPageInfo().HasNextPage},
		}
		if result.GetPageInfo().EndCursor != nil {
			// Show the next cursor _ONLY_ if there is a next page.
			namedValues = append(namedValues, terminal.NamedValue{
				Name:  "Next cursor",
				Value: result.GetPageInfo().GetEndCursor(),
			})
		}

		c.UI.NamedValues(namedValues)
	}

	return 0
}

func (c *pluginListCommand) Synopsis() string {
	return "List available plugins."
}

func (c *pluginListCommand) Usage() string {
	return "phobos [global options] plugin list [options]"
}

func (c *pluginListCommand) Description() string {
	return `
  The plugin list command lists the available plugins.
  -json can be used to output the list in JSON format.
  This command can either be used to list built-in
  plugins or plugins from the plugin registry.
`
}

func (c *pluginListCommand) Example() string {
	return `
phobos plugin list \
  --limit 10 \
  --sort DESC \
  --org-name "tomcat" \
  --json
`
}

func (c *pluginListCommand) Flags() *flag.FlagSet {
	f := flag.NewFlagSet("Command options", flag.ContinueOnError)
	f.BoolVar(
		&c.builtInOnly,
		"built-in",
		false,
		"List only built-in plugins.",
	)
	f.Func(
		"limit",
		"Limit the number of results. Defaults to 100. Not applicable for built-in plugins.",
		func(s string) error {
			i, err := strconv.ParseInt(s, 10, 32)
			if err != nil {
				return err
			}
			c.limit = ptr.Int32(int32(i))
			return nil
		},
	)
	f.Func(
		"sort-order",
		"Sort in this direction, ASC or DESC. Not applicable for built-in plugins.",
		func(s string) error {
			c.sortOrder = &s
			return nil
		},
	)
	f.Func(
		"cursor",
		"Cursor string for manual pagination. Not applicable for built-in plugins.",
		func(s string) error {
			c.cursor = &s
			return nil
		},
	)
	f.Func(
		"org-name",
		"Organization name to filter by. Not applicable for built-in plugins.",
		func(s string) error {
			c.organizationName = &s
			return nil
		},
	)
	f.Func(
		"search",
		"Filter to only plugins with this name prefix. Not applicable for built-in plugins.",
		func(s string) error {
			c.search = &s
			return nil
		},
	)
	f.BoolVar(
		&c.toJSON,
		"json",
		false,
		"Show final output as JSON.",
	)

	return f
}
