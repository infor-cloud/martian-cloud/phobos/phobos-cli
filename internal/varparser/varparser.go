// Package varparser contains the logic for parsing
// HCL and environment variables that Phobos
// API supports. It supports parsing variables
// passed in via flags and from files.
package varparser

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclparse"
	"github.com/zclconf/go-cty/cty"
	ctyjson "github.com/zclconf/go-cty/cty/json"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
)

// Constants required for this module.
const (
	pbvarsExtension          = ".pbvars"
	pbvarsJSONExtension      = ".pbvars.json"
	autoPbVarsExtension      = ".auto" + pbvarsExtension
	autoPbVarsJSONExtension  = ".auto" + pbvarsJSONExtension
	phobosPbVarsFilename     = "phobos" + pbvarsExtension
	phobosPbVarsJSONFilename = "phobos" + pbvarsJSONExtension
	exportedHCLVarPrefix     = "PB_VAR_"
)

// ParseHCLVariablesInput defines the input for ProcessHCLVariables.
type ParseHCLVariablesInput struct {
	HCLVarFilePaths []string
	HCLVariables    []string
}

// ParseEnvironmentVariablesInput defines the input for ProcessEnvironmentVariables.
type ParseEnvironmentVariablesInput struct {
	EnvVarFilePaths []string
	EnvVariables    []string
}

// Variable represents a parsed HCL or environment variable.
type Variable struct {
	Value    string
	Key      string
	Category pb.VariableCategory
}

// VariableParser implements functionalities needed to parse variables.
type VariableParser struct {
	variableMap                map[string]Variable
	configDirectory            *string
	withHCLVarsFromEnvironment bool
}

// NewVariableParser returns a new VariableProcessor.
func NewVariableParser(configDirectory *string, withHCLVarsFromEnvironment bool) *VariableParser {
	return &VariableParser{
		configDirectory:            configDirectory,
		withHCLVarsFromEnvironment: withHCLVarsFromEnvironment,
	}
}

// ParseHCLVariables dispatches the functions to parse HCL
// variables and returns a unique slice of parsed Variables.
//
// Parsing precedence:
// 1. HCL variables from the environment.
// 2. phobos.pbvars file, if present.
// 3. phobos.pbvars.json file, if present.
// 4. *.auto.pbvars.* files, if present.
// 5. --pb-var-file option(s).
// 6. --pb-var option(s).
func (v *VariableParser) ParseHCLVariables(input *ParseHCLVariablesInput) ([]Variable, error) {
	v.variableMap = map[string]Variable{} // Initialize the variable map.

	// Parse exported hcl variables if required.
	if v.withHCLVarsFromEnvironment {
		exportedVariables := []string{}
		for _, e := range os.Environ() {
			if strings.HasPrefix(e, exportedHCLVarPrefix) {
				exportedVariables = append(exportedVariables, e[len(exportedHCLVarPrefix):])
			}
		}
		err := v.processStringVariables(exportedVariables, pb.VariableCategory_HCL)
		if err != nil {
			return nil, err
		}
	}

	if err := v.processPhobosPbvarsFiles(); err != nil {
		return nil, err
	}

	if err := v.processAutoPbvarsFiles(); err != nil {
		return nil, err
	}

	if err := v.processPbVarsFile(input.HCLVarFilePaths); err != nil {
		return nil, err
	}

	err := v.processStringVariables(input.HCLVariables, pb.VariableCategory_HCL)
	if err != nil {
		return nil, err
	}

	variables := []Variable{}
	for _, v := range v.variableMap {
		variables = append(variables, v)
	}

	return variables, nil
}

// ParseEnvironmentVariables dispatches functions to parse environment
// variables and returns a unique slice of parsed Variables.
//
// Parsing precedence:
// 1. --env-var-file option(s).
// 2. --env-var option(s).
func (v *VariableParser) ParseEnvironmentVariables(input *ParseEnvironmentVariablesInput) ([]Variable, error) {
	v.variableMap = map[string]Variable{} // Initialize the variable map.

	if err := v.processEnvVarsFile(input.EnvVarFilePaths); err != nil {
		return nil, err
	}

	err := v.processStringVariables(input.EnvVariables, pb.VariableCategory_ENVIRONMENT)
	if err != nil {
		return nil, err
	}

	variables := []Variable{}
	for _, v := range v.variableMap {
		variables = append(variables, v)
	}

	return variables, nil
}

// processStringVariables iterates through the variables slice and splits variables using "=".
func (v *VariableParser) processStringVariables(variables []string, category pb.VariableCategory) error {
	for _, pair := range variables {
		s := strings.SplitN(pair, "=", 2)

		if len(s) != 2 {
			// Not a key value pair, skip.
			continue
		}

		key := strings.TrimSpace(s[0])
		val := strings.TrimSpace(s[1])

		v.variableMap[key] = Variable{
			Key:      key,
			Value:    val,
			Category: category,
		}
	}

	return nil
}

// processPbVarsFile parses a .pbvars or .pbvars.json file.
func (v *VariableParser) processPbVarsFile(filePaths []string) error {
	for _, path := range filePaths {
		parser := hclparse.NewParser()

		var (
			parsedFile *hcl.File
			diags      hcl.Diagnostics
		)

		// Call the respective parser based on the file extension.
		if strings.HasSuffix(path, pbvarsExtension) {
			parsedFile, diags = parser.ParseHCLFile(path)
			if diags.HasErrors() {
				return fmt.Errorf("%s", diags.Error())
			}
		} else if strings.HasSuffix(path, pbvarsJSONExtension) {
			parsedFile, diags = parser.ParseJSONFile(path)
			if diags.HasErrors() {
				return fmt.Errorf("%s", diags.Error())
			}
		} else {
			return fmt.Errorf("file extension must only be either %q or %q", pbvarsExtension, pbvarsJSONExtension)
		}

		// Get only the attributes.
		attributes, diags := parsedFile.Body.JustAttributes()
		if diags.HasErrors() {
			return fmt.Errorf("%s", diags.Error())
		}

		// Get the values for each attribute and create run variables.
		for key, attr := range attributes {
			value, diags := attr.Expr.Value(nil)
			if diags.HasErrors() {
				return fmt.Errorf("%s", diags.Error())
			}

			var val string
			if value.Type() == cty.String {
				// Type is a string so it can be returned as is.
				val = value.AsString()
			} else {
				// Type is not a string so we need to marshal it to JSON.
				bytes, err := ctyjson.Marshal(value, value.Type())
				if err != nil {
					return fmt.Errorf("%s", diags.Error())
				}
				val = string(json.RawMessage(bytes))
			}

			v.variableMap[key] = Variable{
				Key:      key,
				Category: pb.VariableCategory_HCL,
				Value:    val,
			}
		}
	}

	return nil
}

// processEnvVarsFile reads a environment variables file
// and calls processVariables to store Variables in result map.
func (v *VariableParser) processEnvVarsFile(filePaths []string) error {
	for _, path := range filePaths {
		cleanedPath := filepath.Clean(path)
		file, err := os.Open(cleanedPath)
		if err != nil {
			return err
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)

		// Read file line by line into lines slice.
		var lines []string
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}

		if err = v.processStringVariables(lines, pb.VariableCategory_ENVIRONMENT); err != nil {
			return err
		}
	}

	return nil
}

// processAutoPbvarsFiles lists all the files in the configuration's directory
// and processes the variables from the *.auto.pbvars or *.auto.pbvars.json
func (v *VariableParser) processAutoPbvarsFiles() error {
	if v.configDirectory == nil {
		// Nothing to process.
		return nil
	}

	files, err := os.ReadDir(*v.configDirectory)
	if err != nil {
		return err
	}

	// Find all the files with the "*.auto.pbvars.*" extensions.
	allFiles := []string{}
	for _, file := range files {
		if !file.IsDir() {
			if strings.HasSuffix(file.Name(), autoPbVarsExtension) ||
				strings.HasSuffix(file.Name(), autoPbVarsJSONExtension) {
				pathToFile := filepath.Join(*v.configDirectory, file.Name())
				allFiles = append(allFiles, pathToFile)
			}
		}
	}

	return v.processPbVarsFile(allFiles)
}

// processHCLTfvarsFiles strictly looks for "phobos.pbvars" and
// "phobos.pbvars.json" files in the configuration's directory. Calls
// processTfVarsFile to process the file(s) that exist.
func (v *VariableParser) processPhobosPbvarsFiles() error {
	if v.configDirectory == nil {
		// Nothing to process.
		return nil
	}

	phobosPbVarsFilepath := filepath.Join(*v.configDirectory, phobosPbVarsFilename)
	phobosPbVarsJSONFilepath := filepath.Join(*v.configDirectory, phobosPbVarsJSONFilename)

	// Determine which file exists and therefore we have to process.
	filesToProcess := []string{}
	for _, path := range []string{phobosPbVarsFilepath, phobosPbVarsJSONFilepath} {
		if _, err := os.Stat(path); err == nil {
			filesToProcess = append(filesToProcess, path)
		}
	}

	return v.processPbVarsFile(filesToProcess)
}
