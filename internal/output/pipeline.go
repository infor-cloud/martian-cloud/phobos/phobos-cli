package output

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/client"
	pb "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-api/pkg/protos/gen"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	indentationMap = map[string]string{
		"pipeline":        "",
		"stage":           "  ",
		"task":            "    ",
		"nested pipeline": "    ",
		"action":          "      ",
	}
)

type node struct {
	step     terminal.Step
	nodeType string
	name     string
	status   pb.PipelineNodeStatus
}

func newNode(nodeType string, name string, status pb.PipelineNodeStatus, sg terminal.StepGroup) *node {
	a := &node{
		nodeType: nodeType,
		status:   status,
		name:     name,
	}
	a.step = sg.Add(a.String())
	if status == pb.PipelineNodeStatus_CREATED || status == pb.PipelineNodeStatus_READY {
		a.step.Status("")
	}
	return a
}

func (n *node) String() string {
	return fmt.Sprintf("%s\"%s\" %s", indentationMap[n.nodeType], n.nodeType, n.name)
}

func (n *node) SetStatus(status pb.PipelineNodeStatus) {
	oldStatus := n.status
	n.status = status

	if n.status != oldStatus {
		n.step.Update(n.String())

		switch n.status {
		case pb.PipelineNodeStatus_SKIPPED:
			n.step.Status(terminal.StatusWarn)
		case pb.PipelineNodeStatus_FAILED:
			n.step.Status(terminal.StatusError)
		case pb.PipelineNodeStatus_SUCCEEDED:
			n.step.Status(terminal.StatusOK)
			n.step.Done()
		}
	}
}

// PipelineRenderer displays the pipeline status in realtime
type PipelineRenderer struct {
	ui     terminal.UI
	client *client.Client
}

// NewPipelineRenderer returns a new PipelineRenderer
func NewPipelineRenderer(ui terminal.UI, client *client.Client) *PipelineRenderer {
	return &PipelineRenderer{ui: ui, client: client}
}

// Render renders the pipeline status in realtime
func (r *PipelineRenderer) Render(ctx context.Context, pipelineID string) error {
	// Get the pipeline
	pipeline, err := r.client.PipelinesClient.GetPipelineByID(ctx, &pb.GetPipelineByIdRequest{
		Id: pipelineID,
	})
	if err != nil {
		return err
	}

	// Create terminal step group to display pipeline status
	sg := r.ui.StepGroup()
	nodes := map[string]*node{}

	nodes[pipeline.Metadata.Id] = newNode("pipeline", pipeline.Metadata.Id, pipeline.Status, sg)

	for _, stage := range pipeline.Stages {
		nodes[stage.Path] = newNode("stage", stage.Name, stage.Status, sg)

		for _, task := range stage.Tasks {
			nodes[task.Path] = newNode("task", task.Name, task.Status, sg)

			for _, action := range task.Actions {
				nodes[action.Path] = newNode("action", action.Name, action.Status, sg)
			}
		}

		for _, np := range stage.NestedPipelines {
			nodes[np.Path] = newNode("nested pipeline", np.Name, np.Status, sg)
		}
	}

	return r.render(ctx, pipeline, nodes)
}

func (r *PipelineRenderer) render(ctx context.Context, pipeline *pb.Pipeline, nodes map[string]*node) error {
	lastSeenVersion := pipeline.Metadata.Id

	for {
		if ctx.Err() != nil {
			// Return once parent context is canceled.
			return ctx.Err()
		}

		// Create a context we can use to control the subscription.
		streamCtx, streamCancel := context.WithCancel(ctx)
		defer streamCancel()

		// Subscribe to the pipeline streaming events
		stream, err := r.client.PipelinesClient.GetPipelineEventsStreaming(streamCtx, &pb.GetPipelineEventsStreamingRequest{
			PipelineId: pipeline.Metadata.Id,
			// Pass in last seen version to ensure that we don't miss any events
			LastSeenVersion: &lastSeenVersion,
		})
		if err != nil {
			if status.Code(err) == codes.Canceled {
				// Retry incase child context was canceled.
				continue
			}

			return err
		}

		// Render pipeline status until the pipeline has completed
		for {
			// Run code in a goroutine to call cancel() for the context
			// passed to GetPipelineEventsStreaming after the timeout expires.
			// In order to prevent this from happening when a response
			// is received, a separate context is used to cancel
			// this goroutine once a response is received.
			timeoutCtx, timeoutCancel := context.WithCancel(ctx)
			go func() {
				select {
				case <-timeoutCtx.Done():
					return
				case <-time.After(time.Minute):
					streamCancel()
					return
				}
			}()

			resp, err := stream.Recv()

			// We got the response, so cancel the timeout goroutine.
			timeoutCancel()

			if err != nil {
				if status.Code(err) == codes.Canceled {
					// Break out of inner for loop to retry incase
					// child context was canceled.
					break
				}

				if err == io.EOF {
					// Stream closed.
					return nil
				}

				// Return other errors as is.
				return err
			}

			lastSeenVersion = resp.Pipeline.Metadata.Version

			for _, stage := range resp.Pipeline.Stages {
				for _, task := range stage.Tasks {
					for _, action := range task.Actions {
						node := nodes[action.Path]
						node.SetStatus(action.Status)
					}
					node := nodes[task.Path]
					node.SetStatus(task.Status)
				}
				for _, np := range stage.NestedPipelines {
					node := nodes[np.Path]
					node.SetStatus(np.Status)
				}
				node := nodes[stage.Path]
				node.SetStatus(stage.Status)
			}

			node := nodes[pipeline.Metadata.Id]
			node.SetStatus(resp.Pipeline.Status)

			// Return once pipeline has finished.
			switch resp.Pipeline.Status {
			case pb.PipelineNodeStatus_SUCCEEDED,
				pb.PipelineNodeStatus_FAILED,
				pb.PipelineNodeStatus_CANCELED:
				return nil
			}
		}
	}
}
