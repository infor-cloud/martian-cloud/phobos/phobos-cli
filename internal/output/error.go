// Package output is responsible for formatting the
// final output / errors the user sees in the CLI.
package output

import (
	"bytes"
	"fmt"
	"strings"

	"github.com/kr/text"
	"github.com/mitchellh/go-glint"
	"google.golang.org/grpc/status"
)

const (
	bar = "│ "
)

// FormatError outputs the error with proper formatting for CLI.
// Produces:
// |
// | Error: formatted message here...
// |
// | Code: gRPC code here (if one).
// |
// | Error here...
// |
func FormatError(err error, format string, a ...any) string {
	var buf bytes.Buffer

	document := glint.New()
	document.SetRenderer(
		&glint.TerminalRenderer{
			Output: &buf, // Write everything to buffer.
			Rows:   10,
			Cols:   120, // Wrap long strings.
		},
	)
	document.Append(glint.Text(""))
	document.Append(glint.Style(
		glint.Text(bar),
		glint.Color("red"),
	))
	document.Append(glint.Layout(
		glint.Style(
			glint.Text(bar),
			glint.Color("red"),
		),
		glint.Style(
			glint.Text("Error:"),
			glint.Color("red"),
		),
		glint.Text(" "),
		glint.Text(fmt.Sprintf(format, a...)), // Formatted message.
	).Row())
	document.Append(glint.Style(
		glint.Text(bar),
		glint.Color("red"),
	))

	if err != nil {
		// Fallback to just the error message for non-gRPC errors.
		message := err.Error()

		// Attempt to extract gRPC code out of the error message, if any.
		if s, ok := status.FromError(err); ok {
			message = s.Message() // Use the gRPC error message, when available.
			document.Append(glint.Layout(
				glint.Style(
					glint.Text(bar),
					glint.Color("red"),
				),
				glint.Text("Code:"),
				glint.Text(" "),
				glint.Text(s.Code().String()),
			).Row())
			document.Append(
				glint.Style(
					glint.Text(bar),
					glint.Color("red"),
				),
			)
		}

		// Wrap long error messages and format them properly.
		wrapped := text.Wrap(message, 120)
		lines := strings.Split(wrapped, "\n")
		for _, line := range lines {
			document.Append(glint.Layout(
				glint.Style(
					glint.Text(bar),
					glint.Color("red"),
				),
				glint.Text(line),
			).Row())
		}

		document.Append(
			glint.Style(
				glint.Text(bar),
				glint.Color("red"),
			),
		)
	}

	document.Append(glint.Text(""))

	// Render to buffer.
	document.RenderFrame()

	return buf.String()
}
