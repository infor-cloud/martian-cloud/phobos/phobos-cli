# Phobos CLI

Phobos CLI is a command-line interface to the Phobos API.

- **Organizations**: Create, update, delete organizations.

## Security

If you've discovered a security vulnerability in Phobos CLI, please let us know by creating a **confidential** issue in this project.

## Statement of support

Please submit any bugs or feature requests for Phobos. Of course, MR's are even better. :)

## License

Phobos CLI is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
