// Package main contains the necessary functions for
// building the help menu and configuring the CLI
// library with all subcommand routes.
package main

import (
	"bytes"
	"context"
	"flag"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"github.com/mitchellh/cli"
	"github.com/mitchellh/go-glint"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/command"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-cli/internal/settings"
	"golang.org/x/text/cases"
	"golang.org/x/text/language"
)

const (
	// Logger level environment variable
	logLevelEnvVar = "PHOBOS_CLI_LOG"
)

var (
	// Version is passed in via ldflags at build time
	Version = "1.0.0"

	// DefaultHTTPEndpoint is passed in via ldflags at build time.
	DefaultHTTPEndpoint string

	// DefaultTLSSkipVerify is passed in via ldflags at build time.
	// Indicates if client should skip verifying the server's
	// certificate chain and domain name.
	DefaultTLSSkipVerify bool
)

func main() {
	os.Exit(realMain())
}

// Facilitate testing the main function by wrapping it.
// Now, a test can call realMain without having the os.Exit call getting in the way.
func realMain() int {
	var (
		// binaryName is the name of the binary.
		binaryName = filepath.Base(os.Args[0])
		// displayTitle is the name of the binary in title case.
		displayTitle = cases.Title(language.English, cases.Compact).String(binaryName)
		// rawArgs are the arguments passed to the binary.
		rawArgs = os.Args[1:]
		// profileName is the name of the profile to use.
		profileName string
	)

	// Create a global flagSet.
	f := flag.NewFlagSet("global options", flag.ContinueOnError)
	f.SetOutput(io.Discard)
	f.StringVar(
		&profileName,
		"p",
		settings.DefaultProfileName,
		"Profile name from config file.",
	)
	// Values are never used since CLI framework can handle them,
	// these are simply meant to facilitate the help output for
	// available global flags.
	_ = f.String("v", "", "Show the version information.")
	_ = f.String("h", "", "Show this usage message.")

	// Ignore errors since CLI framework will handle them.
	_ = f.Parse(rawArgs)

	// Log the startup.
	log := hclog.New(&hclog.LoggerOptions{
		Name:        binaryName,
		Level:       hclog.LevelFromString(os.Getenv(logLevelEnvVar)),
		Output:      os.Stdout,
		Color:       hclog.AutoColor,
		DisableTime: true,

		IndependentLevels: true,
	})
	hclog.SetDefault(log)

	log.Debug("",
		"version", Version,
		"binary_name", binaryName,
		"display_title", displayTitle,
		"arguments", rawArgs,
		"profile_name", profileName,
	)

	// For any variation of "-h" or "-help", simply use "-h".
	// Since help option can be used for any command, we must
	// handle it the same anywhere.
	for ix, arg := range rawArgs {
		if arg == "--h" || arg == "--help" || arg == "-help" {
			rawArgs[ix] = "-h"
		}
	}

	// Only replace "--version" and "--v" at the global level i.e. the first argument.
	// Allows using the same argument in commands and subcommands.
	if len(rawArgs) > 0 && (rawArgs[0] == "--version" || rawArgs[0] == "-version" || rawArgs[0] == "--v") {
		rawArgs[0] = "-v"
	}

	// Get the CLI context.
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Prepare the command metadata struct.
	baseCommand := &command.BaseCommand{
		Context:              ctx,
		BinaryName:           binaryName,
		DisplayTitle:         displayTitle,
		Version:              Version,
		Logger:               log,
		UI:                   terminal.ConsoleUI(ctx),
		CurrentProfileName:   profileName,
		DefaultHTTPEndpoint:  DefaultHTTPEndpoint,
		DefaultTLSSkipVerify: DefaultTLSSkipVerify,
	}

	// Defer closing the base command, so the UI output could finish rendering.
	defer baseCommand.Close()

	commandArgs := rawArgs // No global flag.
	if f.NFlag() > 0 {
		// A global option was set, so pass remaining arguments to commands,
		// since it will automatically be handled.
		commandArgs = f.Args()
	}

	availableCommands, err := commands(baseCommand)
	if err != nil {
		log.Error(err.Error())
		return 1
	}

	c := cli.CLI{
		Name:       binaryName,
		Version:    Version,
		Args:       commandArgs,
		Commands:   availableCommands,
		HelpFunc:   helpFunc(cli.BasicHelpFunc(binaryName), f),
		HelpWriter: os.Stdout,
	}

	// Run the CLI.
	exitStatus, err := c.Run()
	if err != nil {
		log.Error(err.Error())
		return 1
	}

	return exitStatus
}

// commands returns all the available commands.
func commands(baseCommand *command.BaseCommand) (map[string]cli.CommandFactory, error) {

	// The map of all commands except documentation.
	commandMap := map[string]command.Factory{
		"fmt":                        command.NewFmtCommandFactory(baseCommand),
		"sso":                        command.NewHelpCommandFactory(getHelpText("sso")),
		"sso login":                  command.NewLoginCommandFactory(baseCommand),
		"configure":                  command.NewConfigureCommandFactory(baseCommand),
		"configure list":             command.NewConfigureListCommandFactory(baseCommand),
		"configure delete":           command.NewConfigureDeleteCommandFactory(baseCommand),
		"organization":               command.NewHelpCommandFactory(getHelpText("organization")),
		"organization get":           command.NewOrganizationGetCommandFactory(baseCommand),
		"organization list":          command.NewOrganizationListCommandFactory(baseCommand),
		"organization create":        command.NewOrganizationCreateCommandFactory(baseCommand),
		"organization update":        command.NewOrganizationUpdateCommandFactory(baseCommand),
		"organization delete":        command.NewOrganizationDeleteCommandFactory(baseCommand),
		"project":                    command.NewHelpCommandFactory(getHelpText("project")),
		"project get":                command.NewProjectGetCommandFactory(baseCommand),
		"project list":               command.NewProjectListCommandFactory(baseCommand),
		"project create":             command.NewProjectCreateCommandFactory(baseCommand),
		"project update":             command.NewProjectUpdateCommandFactory(baseCommand),
		"project delete":             command.NewProjectDeleteCommandFactory(baseCommand),
		"pipeline":                   command.NewHelpCommandFactory(getHelpText("pipeline")),
		"pipeline get":               command.NewPipelineGetCommandFactory(baseCommand),
		"pipeline list":              command.NewPipelineListCommandFactory(baseCommand),
		"pipeline create":            command.NewPipelineCreateCommandFactory(baseCommand),
		"pipeline cancel":            command.NewPipelineCancelCommandFactory(baseCommand),
		"pipeline validate":          command.NewPipelineValidateCommandFactory(baseCommand),
		"plugin":                     command.NewHelpCommandFactory(getHelpText("plugin")),
		"plugin install":             command.NewPluginInstallCommandFactory(baseCommand),
		"plugin launch":              command.NewPluginLaunchCommandFactory(baseCommand),
		"plugin list":                command.NewPluginListCommandFactory(baseCommand),
		"plugin get":                 command.NewPluginGetCommandFactory(baseCommand),
		"plugin create":              command.NewPluginCreateCommandFactory(baseCommand),
		"plugin update":              command.NewPluginUpdateCommandFactory(baseCommand),
		"plugin delete":              command.NewPluginDeleteCommandFactory(baseCommand),
		"plugin upload-version":      command.NewPluginUploadVersionCommandFactory(baseCommand),
		"plugin get-version":         command.NewPluginGetVersionCommandFactory(baseCommand),
		"plugin get-platform":        command.NewPluginGetPlatformCommandFactory(baseCommand),
		"plugin delete-version":      command.NewPluginDeleteVersionCommandFactory(baseCommand),
		"plugin delete-platform":     command.NewPluginDeletePlatformCommandFactory(baseCommand),
		"plugin list-versions":       command.NewPluginListVersionsCommandFactory(baseCommand),
		"plugin list-platforms":      command.NewPluginListPlatformsCommandFactory(baseCommand),
		"release":                    command.NewHelpCommandFactory(getHelpText("release")),
		"release list":               command.NewReleaseListCommand(baseCommand),
		"release get":                command.NewReleaseGetCommandFactory(baseCommand),
		"release create":             command.NewReleaseCreateCommandFactory(baseCommand),
		"release update":             command.NewReleaseUpdateCommandFactory(baseCommand),
		"release delete":             command.NewReleaseDeleteCommandFactory(baseCommand),
		"release add-participant":    command.NewReleaseAddParticipantCommandFactory(baseCommand),
		"release remove-participant": command.NewReleaseRemoveParticipantCommandFactory(baseCommand),
		"release update-deployment":  command.NewReleaseUpdateDeploymentCommandFactory(baseCommand),
		"release-lifecycle":          command.NewHelpCommandFactory(getHelpText("release-lifecycle")),
		"release-lifecycle get":      command.NewReleaseLifecycleGetCommandFactory(baseCommand),
		"release-lifecycle list":     command.NewReleaseLifecycleListCommandFactory(baseCommand),
		"release-lifecycle create":   command.NewReleaseLifecycleCreateCommandFactory(baseCommand),
		"release-lifecycle update":   command.NewReleaseLifecycleUpdateCommandFactory(baseCommand),
		"release-lifecycle delete":   command.NewReleaseLifecycleDeleteCommandFactory(baseCommand),
		"release-lifecycle validate": command.NewReleaseLifecycleValidateCommandFactory(baseCommand),
		"pipeline-template":          command.NewHelpCommandFactory(getHelpText("pipeline-template")),
		"pipeline-template get":      command.NewPipelineTemplateGetCommandFactory(baseCommand),
		"pipeline-template list":     command.NewPipelineTemplateListCommandFactory(baseCommand),
		"pipeline-template create":   command.NewPipelineTemplateCreateCommandFactory(baseCommand),
		"pipeline-template delete":   command.NewPipelineTemplateDeleteCommandFactory(baseCommand),
		"version":                    command.NewVersionCommandFactory(baseCommand),
	}

	// Add the documentation commands.
	commandMap["documentation"] = command.NewHelpCommandFactory(getHelpText("documentation"))
	commandMap["documentation generate"] = command.NewDocumentationGenerateCommandFactory(baseCommand, commandMap)

	// Convert CommandFactory to cli.CommandFactory.
	returnMap := map[string]cli.CommandFactory{}
	for name, helpCommandFactory := range commandMap {
		helpCommand, err := helpCommandFactory()
		if err != nil {
			return nil, err
		}

		returnMap[name] = func() (cli.Command, error) {
			return command.NewWrapper(helpCommand), nil
		}
	}

	return returnMap, nil
}

// helpFunc adds global options to the default help function.
func helpFunc(h cli.HelpFunc, f *flag.FlagSet) cli.HelpFunc {
	return func(commands map[string]cli.CommandFactory) string {
		var headingBuf bytes.Buffer
		document := glint.New()
		document.SetRenderer(&glint.TerminalRenderer{
			Output: &headingBuf, // Write everything to buffer.
			Rows:   10,
			Cols:   180,
		})
		document.Append(glint.Layout(
			glint.Style(
				glint.Text("Welcome to Phobos!"),
				glint.Bold(),
				glint.ColorRGB(186, 104, 200), // Same as UI.
			),
			glint.Text(" — "),
			glint.Style(
				glint.Text("A deployment orchestration and release management tool."),
				glint.Italic(),
			),
		).Row())
		document.Append(glint.Layout(
			glint.Style(
				glint.Text("Documentation:"),
				glint.Bold(),
			),
			glint.Text(" "),
			glint.Text("https://phobos.martian-cloud.io"),
		).Row())
		document.Append(glint.Layout(
			glint.Style(
				glint.Text("Version:"),
				glint.Color("green"),
				glint.Bold(),
			),
			glint.Text(" "),
			glint.Text(Version),
		).Row(), glint.Text(""), glint.Text("")) // Make them part of the same row and add newlines.

		// Render a single frame to buffer.
		document.RenderFrame()

		// Build global flag usage.
		var usageBuf bytes.Buffer
		usageBuf.Write([]byte("\n"))
		globalFlags := f
		globalFlags.SetOutput(&usageBuf)
		globalFlags.Usage()

		return strings.TrimSpace(headingBuf.String() + h(commands) + usageBuf.String())
	}
}

// getHelpText returns the helpText for command.
func getHelpText(commandName string) (string, string) {
	return helpText[commandName][0], helpText[commandName][1]
}

// This should be used for all parent commands that appear on the main page
// i.e., commands that are generally placeholders for subcommands.
var helpText = map[string][2]string{
	"sso": {
		"Log in to the OAuth2 provider and return an authentication token.", // Synopsis text.
		`
The sso command authenticates the CLI with the OAuth2 provider,
and allows making authenticated calls to Phobos backend.
`,
	},
	"organization": {
		"Perform operations on organizations.",
		`
The organization commands do operations on organizations.
Organizations are containers that house other Phobos
resources. Subcommands provide full CRUD support.
`,
	},
	"project": {
		"Perform operations on projects.",
		`
The project commands do operations on projects.
Projects are also containers that house other Phobos
resources. Subcommands provide full CRUD support.
`,
	},
	"pipeline": {
		"Perform operations on pipelines.",
		`
The pipeline commands do operations on pipelines.
Pipelines execute sequences of prescribed operations.
Subcommands provide create, list, get, and cancel support.
`,
	},
	"plugin": {
		"Interact with plugins.",
		`
The plugin commands do operations on plugins.
Plugins are used to extend the functionality of Phobos.
Subcommands provide support for listing, launching and
managing plugins via the plugin registry.
`,
	},
	"release": {
		"Create, manage releases and deployments.",
		`
The release commands perform operations on project releases and deployments.
Subcommands provide create, update, list, get functionality and more.
`,
	},
	"release-lifecycle": {
		"Perform operations on release lifecycles.",
		`
The release-lifecycle commands do operations on release lifecycles.
Subcommands provide create, validate, list, get, update and delete support.
`,
	},
	"pipeline-template": {
		"Perform operations on pipeline templates.",
		`
The pipeline-template commands do operations on pipeline templates.
A pipeline template is a reusable pipeline definition.
Subcommands provide create, list, get, and delete support.
`,
	},
	"documentation": {
		"Perform command documentation operations.",
		`
The documentation command(s) perform operations on the documentation.
`,
	},
}
